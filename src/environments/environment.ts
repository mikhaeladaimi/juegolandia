// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const pre = true;
export const baseUrl = pre ? 'http://192.168.2.53:8080' : 'http://192.168.2.109:8097';

export const environment = {
  production: false,
  reCaptchaSiteKey: "6LcBdroUAAAAAAOkeZzO0swa_AFkLcQIH1Ip-u0z",
  api: {
    public:  baseUrl + '/JuegolandiaBack/v1/public/',
    app: baseUrl + '/JuegolandiaBack/v1/rest/app/',
    auth: baseUrl + '/JuegolandiaBack/v1/auth/app/',
  },
  firebase: {
    apiKey: "AIzaSyAMHPSIMMC8QSEHj2z2iYwgY2iYfMBJcgI",
    authDomain: "juegolandia-uoc.firebaseapp.com",
    databaseURL: "https://juegolandia-uoc.firebaseio.com",
    projectId: "juegolandia-uoc",
    storageBucket: "juegolandia-uoc.appspot.com",
    messagingSenderId: "243234874940",
    appId: "1:243234874940:web:dd693f697931eed4350539"
  },
  googleCode: "código",
  stripeKey: "pk_test_QiT0RlYGh5Nb3teaj6W1t8o700Gs37pWPk",
};
