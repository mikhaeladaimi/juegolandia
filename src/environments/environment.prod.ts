export const environment = {
  production: true,
  reCaptchaSiteKey: "project-key",
  api: {
    public: '',
    app: '',
    auth: '',
  },
  firebase: {
    apiKey: "AIzaSyAMHPSIMMC8QSEHj2z2iYwgY2iYfMBJcgI",
    authDomain: "juegolandia-uoc.firebaseapp.com",
    databaseURL: "https://juegolandia-uoc.firebaseio.com",
    projectId: "juegolandia-uoc",
    storageBucket: "juegolandia-uoc.appspot.com",
    messagingSenderId: "243234874940",
    appId: "1:243234874940:web:dd693f697931eed4350539"
  },
  googleCode: "código",
  stripeKey: "pk_test_QiT0RlYGh5Nb3teaj6W1t8o700Gs37pWPk"
};
