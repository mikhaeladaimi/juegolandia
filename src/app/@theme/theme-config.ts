import {LegalSection} from "../pages/legal/LegalSection";
import {ObjectPositionType} from "../models/enum/ObjectPositionType";
import {HorizontalAlignmentType} from "../models/enum/HorizontalAlignmentType";
import {VerticalAlignmentType} from "../models/enum/VerticalAlignmentType";

export const THEME_CONFIG = {
  hasTopBar: false,
  backgroundHeader: "https://picsum.photos/1500/500",
  menu: {
    logoPosition: ObjectPositionType.LEFT,
    isSticky: true,
    isBoxed: true,
    displayCart: true,
    displayUser: true
  },
  footer: {
    displayCopyright: true
  },
  shopConfig: {
    sidebar: HorizontalAlignmentType.NONE,
    sidebarConfig : {
      displayPriceFilter: true,
      displayProductFeatured: true,
      bannerList: false,
      displaySocialMedia: true,
      displaySearchBar: true,
      displayCategoriesFilter: true,
      displayAttributesFilter: true
    },
    isEcommerce: true,
    displayPrice: true,
    displayCart: true,
    displayStripe: true,
    displayPriceOnList: false,
    title: "Tu tienda de Juegos de Mesa",
    displayControlsOnDetail: true,
    displaySidebarOnDetail: true,
    showDisplayTitle: true,
    couponActivated: true
  },
  social: [
    {
      link: "https://www.youtube.com"
    },
    {
      link: "https://www.facebook.com/"
    },
    {
      link: "https://www.twitter.com"
    },
    {
      link: "https://www.instagram.com"
    }
  ],
  cookieConfig: { // NgcCookieConsentConfig https://tinesoft.github.io/ngx-cookieconsent/home
    cookie: {
      domain: "localhost"
    },
    position: "bottom",
    theme: "classic",
    palette: {
      popup: {
        "background": "#000000"
      },
      button: {
        "background": "#ffffff"
      }
    },
    type: "info",
    content: {
      message: "Utilizamos cookies propias y de terceros para mejorar nuestros servicios, " +
        "personalizar y analizar sus hábitos de navegación y mostrarle publicidad relacionada con sus preferencias.",
      dismiss: "Aceptar",
      link: "Saber más",
      href: "/legal/" + LegalSection.COOKIES,
      policy: "Política de cookies"
    }
  },
  blog: {
    blogUrl: "https://blog.juegolandia.com/",
  },
  contactConfig: {
    contactData: [
      {
        icon: "fa-phone",
        roundIcon: "",
        link: "tel:689573618",
        title: "689573618",
        target: ""
      },
      {
        icon: "fa-envelope",
        roundIcon: "",
        link: "mailto:info@juegolandia.com",
        title: "info@juegolandia.com",
        target: ""
      },
      {
        icon: "fa-hourglass",
        roundIcon: "",
        link: "",
        title: "9:00-14:00 / 17:00-20:30",
        target: ""
      }
    ],
    contactInfo: "Será un texto que puede mostrarse en la sidebar, a veces incluye horarios o información importante.",
    title: "Contacta con nosotros",
    subtitle: "¡Queremos saber de ti!",
    map: {
      mapAlign: VerticalAlignmentType.TOP,
      mapLink: "https://maps.google.com/maps?q=Universidad+Oberta+de+Catalunya&t=&z=13&ie=UTF8&iwloc=&output=embed"
    },
    form: {
      title: "Contacta con nosotros",
      description: "Complete el formulario y le responderemos con la mayor brevedad posible.",
      active: true,
      displayPhone: false,
      textAlign: HorizontalAlignmentType.CENTER
    },
    sidebar: HorizontalAlignmentType.LEFT,
    displaySocialBar: true
  },
  bankAccount: {
    account: "1210	0418	40	1234567891",
    owner: "Mikhael Adaimi",
    baseConcept: "Juegolandia"
  }
};
