import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {FooterComponent} from './components/footer/footer.component';
import {HeaderComponent} from './components/header/header.component';
import {MenuComponent} from './components/menu/menu.component';
import {LayoutComponent} from './components/layout/layout.component';
import {MatProgressSpinnerModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {environment} from '../../environments/environment';
import {TopbarComponent} from "./components/topbar/topbar.component";
import {SocialComponent} from "./components/social/social.component";
import {ShoppingCartComponent} from "./components/shopping-cart/shopping-cart.component";
import {CartCacheService} from "../shared/cache/cart-cache.service";
import {SearchProductComponent} from "../components/search-product/search-product.component";

import {
  CalculateTotalPipe,
  CapitalizePipe,
  HorizontalAlignmentPipe,
  ImageUrlPipe,
  LinkPipe,
  NumberWithCommasPipe,
  ObjectPositionMatchPipe,
  PaymentTypePipe,
  PluralPipe,
  PricePipe,
  ReadMorePipe,
  RoundPipe,
  TimingPipe,
  TruncatePipe,
  ValidationPipe,
} from './pipes';

const BASE_MODULES = [
  CommonModule,
  FormsModule,
  RouterModule,
  ReactiveFormsModule,
  NgbModule,
  MatProgressSpinnerModule,
  LoggerModule];

const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  ValidationPipe,
  ImageUrlPipe,
  PricePipe,
  ReadMorePipe,
  TruncatePipe,
  PaymentTypePipe,
  CalculateTotalPipe,
  HorizontalAlignmentPipe,
  ObjectPositionMatchPipe,
  LinkPipe,
];

const TH_MODULES = [];

const THEME_PROVIDERS = [
  CartCacheService,
  ...LoggerModule.forRoot({
    serverLoggingUrl: environment.api.public + 'log',
    level: !environment.production ? NgxLoggerLevel.DEBUG : NgxLoggerLevel.OFF,
    serverLogLevel: environment.production ? NgxLoggerLevel.ERROR : NgxLoggerLevel.OFF,
  }).providers,
  ...RouterModule.forRoot([]).providers
];

const COMPONENTS = [
  FooterComponent, HeaderComponent, MenuComponent, LayoutComponent, TopbarComponent, SocialComponent, ShoppingCartComponent, SearchProductComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...TH_MODULES],
  exports: [...BASE_MODULES, ...COMPONENTS, ...PIPES],
  declarations: [...COMPONENTS, ...PIPES],
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeModule,
      providers: [...THEME_PROVIDERS],
    };
  }
}
