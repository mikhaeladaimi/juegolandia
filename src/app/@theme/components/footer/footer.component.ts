import {Component} from '@angular/core';
import {version} from '../../../../environments/version';
import {FOOTER_COLUMNS, FOOTER_COPYRIGHT_LINKS} from './footer-data';
import {THEME_CONFIG} from '../../theme-config';
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  columns = FOOTER_COLUMNS;
  copyrightLinks = FOOTER_COPYRIGHT_LINKS;
  displayCopyright: boolean;
  version = version.number;

  // region routes
  legalRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.LEGAL);

  // endregion

  constructor() {
    this.displayCopyright = THEME_CONFIG.footer.displayCopyright;
  }

}
