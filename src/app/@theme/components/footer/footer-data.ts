import {SlugsUtils} from "../../../shared/utils/slugs.utils";

export const FOOTER_COLUMNS = [
  {
    title: "Juegolandia",
    links: [
      {
        text: 'Quiénes somos',
        route: '/' + SlugsUtils.COMPANY
      },
      {
        text: 'Nuestros productos',
        route: '/' + SlugsUtils.SHOP
      },
      {
        text: 'Contacto',
        route: '/' + SlugsUtils.CONTACT
      },
    ]
  },
  {
    title: "Los mejores juegos",
    links: [
      {
        text: 'Cartas',
        route: '/' + SlugsUtils.SHOP
      },
      {
        text: 'Wargames',
        route: '/' + SlugsUtils.SHOP
      },
    ]
  },
  {
    title: "¿Dónde estamos?",
    links: [
      {
        icon: 'fa fa-envelope',
        text: 'info@juegolandia.com',
        route: 'mailto:info@juegolandia.com'
      },
      {
        text: 'Calle Los Ovalle, Nº 32 <br> 37185 Salamanca',
        route: 'https://goo.gl/maps/KNEpNZwRpegrCn7E6',
        target: '_blank'
      },
    ]
  },
];

export const FOOTER_COPYRIGHT_LINKS = [
  {
    text: 'Aviso legal',
    route: SlugsUtils.NOTICE
  },
  {
    text: 'Política de privacidad',
    route: SlugsUtils.PRIVACY
  }
];


