import {Component, EventEmitter, Input, Output} from '@angular/core';
import {THEME_CONFIG} from "../../theme-config";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Input() displayLogo: boolean = false;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() subtitleSmall: string;
  @Input() background: any = THEME_CONFIG.backgroundHeader;
  @Input() hasParallaxEffect: boolean;

  @Output() onImageClickEvent: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  onImageClick() {
    this.background && this.onImageClickEvent.emit(this.background);
  }
}
