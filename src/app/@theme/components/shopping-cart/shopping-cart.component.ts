import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Rx";
import {CartCacheService} from "../../../shared/cache/cart-cache.service";
import {ViewUtils} from "../../../shared/utils/view.utils";
import {NavigationEnd, Router} from "@angular/router";
import {OrderItem} from "../../../models/order/OrderItem";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  totalPrice: string;
  orderItems: OrderItem[];
  viewUtils = ViewUtils;
  atShop: boolean;

  // region routes
  shopRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.SHOP);
  cartDetailRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.CART_DETAIL);

  // endregion

  constructor(private cartUtils: CartCacheService, private router: Router) {
  }

  ngOnInit() {
    if (!this.subscription) {
      this.subscription = this.cartUtils.cartObservable.subscribe(() => {
        this.retrieveCartInfo();
      });
    }
    this.checkAtShop();
    this.router.events.subscribe(event => event instanceof NavigationEnd && this.checkAtShop());
  }

  checkAtShop() {
    this.atShop = this.router.url == this.shopRoute;
  }

  retrieveCartInfo() {
    this.orderItems = this.cartUtils.getCartData();
    this.totalPrice = this.viewUtils.getPrice(this.cartUtils.getTotal());
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }
}
