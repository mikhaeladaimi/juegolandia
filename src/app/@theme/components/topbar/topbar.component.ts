import {Component, OnInit} from '@angular/core';
import {GeneralLink} from "../../../models/link/GeneralLink";
import {TOP_BAR_ITEMS} from "./topbar-items";
import {IconsType} from "../../../models/enum/IconsType";
import {HorizontalAlignmentType} from "../../../models/enum/HorizontalAlignmentType";

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  linksItems = [];
  contactLinksItems = [];
  alignment = HorizontalAlignmentType;
  alignmentType: HorizontalAlignmentType;
  iconType = IconsType.SIMPLE;

  constructor() {
  }

  ngOnInit() {
    this.getTopbarItems();
    this.alignmentType = TOP_BAR_ITEMS.alignment;
  }

  getTopbarItems() {

    for (const item of TOP_BAR_ITEMS.links) {
      this.linksItems.push(new GeneralLink().deserialize(item));
    }

    for (const item of TOP_BAR_ITEMS.contactLinks) {
      this.contactLinksItems.push(new GeneralLink().deserialize(item));
    }
  }
}
