import {HorizontalAlignmentType} from "../../../models/enum/HorizontalAlignmentType";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

export const TOP_BAR_ITEMS = {
  links: [{
    icon: "",
    squareIcon: "",
    link: SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME),
    title: "> Contact Us",
  },
    {
      icon: "",
      squareIcon: "",
      link: SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME),
      title: "> Sign In",
    },
    {
      icon: "",
      squareIcon: "",
      link: SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME),
      title: "> Add More",
    }],
  contactLinks: [{
    icon: 'fa fa-envelope',
    squareIcon: "",
    link: 'mailto:nombre@mail.com',
    title: "nombre@mail.com",
  },
    {
      icon: 'fa fa-phone',
      squareIcon: "",
      link: 'tel:923102030',
      title: "923 10 20 30",
    }],
  alignment: HorizontalAlignmentType.CENTER
};

