import {Component, OnInit} from '@angular/core';
import {MENU_ITEMS} from './menu-items';
import {THEME_CONFIG} from "../../theme-config";
import {MenuConfig} from "../../../models/menu/MenuConfig";
import {UserService} from "../../../shared/services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  menu_items = [];
  config: MenuConfig = new MenuConfig();
  openSearch: boolean = false;

  // region routes
  shopRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.SHOP);
  myProfileRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.MY_PROFILE);
  accessRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.ACCESS);
  homeRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME);

  // endregion

  constructor(private router: Router, public userService: UserService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.config.deserialize(THEME_CONFIG.menu);
    (this.config.isLogoCentered) ? this.generateLists() : this.menu_items.push(MENU_ITEMS);
  }

  generateLists() {
    const rightSide = MENU_ITEMS;
    const leftSide = MENU_ITEMS;
    const half = Math.ceil(MENU_ITEMS.length / 2);
    leftSide.splice(0, half);
    rightSide.splice(half, MENU_ITEMS.length - half);
    this.menu_items.push(leftSide);
    this.menu_items.push(rightSide);
  }

  onSearchEnter(name) {
    this.openSearch = false;
    if (this.router.url.toString().toLowerCase().includes('catalogo')) {
      this.router.navigate(
        [],
        {
          relativeTo: this.route,
          queryParams: {nombre: name},
          queryParamsHandling: "merge",
        });
    } else {
      this.router.navigate([SlugsUtils.getAbsoluteRoute(SlugsUtils.SHOP)], {queryParams: {nombre: name}});
    }
  }

  onShowChange() {
    this.openSearch = !this.openSearch;
  }

}
