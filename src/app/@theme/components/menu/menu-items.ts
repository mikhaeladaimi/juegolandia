import {SlugsUtils} from "../../../shared/utils/slugs.utils";

export const MENU_ITEMS = [
  {
    title: 'Principal',
    link: SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME),
    target: '_self'
  },
  {
    title: 'Sobre nosotros',
    icon: '',
    link: SlugsUtils.getAbsoluteRoute(SlugsUtils.COMPANY),
    target: '_self'
  },
  {
    title: 'Contacto',
    icon: '',
    link: SlugsUtils.getAbsoluteRoute(SlugsUtils.CONTACT),
    target: '_self'
  },
];
