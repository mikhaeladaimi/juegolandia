import {Component, Input, OnInit} from "@angular/core";
import {IconsType} from "../../../models/enum/IconsType";
import {GeneralLink} from "../../../models/link/GeneralLink";
import {SOCIAL_CONFIG} from "./social-config";

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})

export class SocialComponent implements OnInit {

  @Input() iconsType: IconsType;

  socialItems = [];

  constructor() {

  }

  ngOnInit(): void {
    this.getSocialItems();
  }

  getSocialItems() {
    for (const item of SOCIAL_CONFIG) {
      this.socialItems.push(new GeneralLink().deserialize(item));
    }
  }

  getIconsClass(item): any {
    switch (this.iconsType) {
      case IconsType.SIMPLE: return item.icon;
      case IconsType.SQUARE: return item.squareIcon;
      case IconsType.ROUNDED: return item.icon;
    }
  }
}
