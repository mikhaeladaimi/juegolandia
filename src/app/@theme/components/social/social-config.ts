export const SOCIAL_CONFIG = [
  {
    link: "https://www.youtube.com",
    icon: 'fa fa-youtube-play',
    squareIcon: 'fa fa-youtube-square'
  },
  {
    link: "https://www.facebook.com/",
    icon: 'fa fa-facebook',
    squareIcon: 'fa fa-facebook-square'
  },
  {
    link: "https://www.twitter.com",
    icon: 'fa fa-twitter',
    squareIcon: 'fa fa-twitter-square'
  },
  {
    link: "https://www.instagram.com",
    icon: 'fa fa-instagram',
    squareIcon: 'fa fa-instagram'
  }
];
