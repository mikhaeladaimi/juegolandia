import { Component } from '@angular/core';
import { THEME_CONFIG } from '../../theme-config';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {

  hasTopBar = THEME_CONFIG.hasTopBar;
  constructor() { }

}
