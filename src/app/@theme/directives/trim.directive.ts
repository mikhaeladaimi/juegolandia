import {Directive, EventEmitter, HostListener, Output} from "@angular/core";

@Directive({
  selector: 'input[appTrim],textarea[appTrim]'
})
export class TrimDirective {

  @Output() ngModelChange = new EventEmitter();

  private trimValue(el, value) {
    el.value = value.trim().replace(/\s{2,}/g, " ");
    this.ngModelChange.emit(el.value);
  }

  @HostListener('blur', ['$event.target', '$event.target.value'])
  onBlur(el: any, value: string): void {
    this.trimValue(el, value);
  }
}
