import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'price'})
export class PricePipe implements PipeTransform {

  /**
   *
   * @param input
   * @return price on Eur with € added.
   */
  transform(input: any): any {
    let result = 0;
    if (!isNaN(+input)) {
      result = input / 100;
    }
    return result.toFixed(2) + " €";
  }

}
