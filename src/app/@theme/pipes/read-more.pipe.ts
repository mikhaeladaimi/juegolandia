import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'readMore'})
export class ReadMorePipe implements PipeTransform {

  transform(input: string): string {

    if (input != null) {
      if (input.length < 5) return input;
      const descriptionPlain = input.slice(0, -4);
      return (descriptionPlain.length > 250) ? descriptionPlain.slice(0, 250) + "..." : descriptionPlain + "...";
    } else {
      return "";
    }
  }
}
