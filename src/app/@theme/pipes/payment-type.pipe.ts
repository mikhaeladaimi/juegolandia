import {Pipe, PipeTransform} from '@angular/core';
import {PaymentType} from "../../models/enum/PaymentType";

@Pipe({ name: 'paymentType' , pure: true})
export class PaymentTypePipe implements PipeTransform {

  /**
   * Validation errors of forms
   */

  transform(paymentType: PaymentType): string {
    switch (paymentType) {
      case PaymentType.STRIPE:
        return "Tarjeta";
      case PaymentType.TPV:
        return "Tarjeta";
      case PaymentType.TRANSFER:
        return "Transferencia";
    }
  }

}
