import {Pipe, PipeTransform} from '@angular/core';
import {ObjectPositionType} from "../../models/enum/ObjectPositionType";

@Pipe({name: 'ngxObjectPositionMatch'})
export class ObjectPositionMatchPipe implements PipeTransform {

  transform(input: ObjectPositionType, values: string[]): boolean {
    let found = false;
    if (input && values && values.length > 0) {
      values.filter(value => {
        if (input.toLowerCase() == value.toLowerCase())
          found = true;
      });
    }
    return found;
  }
}
