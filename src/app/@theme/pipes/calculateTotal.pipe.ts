import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'ngxCalculateTotal'})
export class CalculateTotalPipe implements PipeTransform {

  transform(input: any): string {
    let total = 0;
    
    for (const orderLine of input) {
      total += orderLine.quantity;
    }

    if (total > 1) {
      return total + " productos";
    } else {
      return total + " producto";
    }
  }
}
