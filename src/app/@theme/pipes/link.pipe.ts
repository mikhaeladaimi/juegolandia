import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'ngxLink', pure: true})
export class LinkPipe implements PipeTransform {


  /**
   * Validation link
   */

  transform(link: string): string {
    return link.includes('http') || link.startsWith('mailto:') || link.startsWith('tel') ? link : '//' + link;
  }
}
