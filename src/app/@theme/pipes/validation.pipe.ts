import {ValidationErrors} from "@angular/forms";

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'validation' , pure: true})
export class ValidationPipe implements PipeTransform {


  /**
   * Validation errors of forms
   */
  private static REQUIRED: string = 'required';
  private static PATTERN: string = 'pattern';
  private static MINLENGTH: string = 'minlength';
  private static MAXLENGTH: string = 'maxlength';
  private static REQUIRED_LENGTH: string = 'requiredLength';

  transform(errors: ValidationErrors): string {
    let error: string = "Este campo tiene errores";
    if (ValidationPipe.isDefined(errors, ValidationPipe.REQUIRED)) {
      error = "Este campo es obligatorio.";
    }
    if (ValidationPipe.isDefined(errors, ValidationPipe.PATTERN)) {
      error = "Este campo no es válido.";
    }
    if (ValidationPipe.isDefined(errors, ValidationPipe.MINLENGTH)) {
      error = "Este campo no contiene la longitud mínima necesaria (" +
        ValidationPipe.getErrorValue(errors, ValidationPipe.MINLENGTH, ValidationPipe.REQUIRED_LENGTH) + ").";
    }
    if (ValidationPipe.isDefined(errors, ValidationPipe.MAXLENGTH)) {
      error = "Este campo excede la longitud máxima permitida (" +
        ValidationPipe.getErrorValue(errors, ValidationPipe.MAXLENGTH, ValidationPipe.REQUIRED_LENGTH) + ").";
    }
    return error;
  }

  static isDefined(errors: ValidationErrors, error: string): boolean {
    return errors[error] != undefined;
  }

  static getErrorValue(errors: ValidationErrors, error: string, value: string): any {
    return errors[error][value];
  }
}
