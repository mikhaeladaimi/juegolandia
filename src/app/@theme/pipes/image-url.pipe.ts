import {Pipe, PipeTransform} from '@angular/core';
import {ImageApp} from "../../models/image/ImageApp";
import {ImageUtils} from "../../shared/utils/image.utils";

@Pipe({name: 'imageUrl', pure: true})
export class ImageUrlPipe implements PipeTransform {

  /**
   * Image url pipe
   */

  transform(image: ImageApp | string | string[], defaultImg?: string): string {
    defaultImg = defaultImg ? defaultImg : ImageUtils.DEFAULT_IMAGE;
    if (Array.isArray(image)) {
      if (image.length > 0) {
        image = image[0];
      } else {
        return defaultImg;
      }
    }
    if (image instanceof ImageApp)
      return image && image.url ? image.url + "?" + image.signature : defaultImg;
    else
      return image ? image : defaultImg;
  }

}
