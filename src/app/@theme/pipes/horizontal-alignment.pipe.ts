import {Pipe, PipeTransform} from '@angular/core';
import {HorizontalAlignmentType} from "../../models/enum/HorizontalAlignmentType";

@Pipe({name: 'ngxHorizontalAlignment'})
export class HorizontalAlignmentPipe implements PipeTransform {

  transform(input: string): string {
    switch (input) {
      case HorizontalAlignmentType.CENTER:
        return 'align-center';
      case HorizontalAlignmentType.RIGHT:
        return 'align-right';
      case HorizontalAlignmentType.LEFT:
        return 'align-left';
      case HorizontalAlignmentType.NONE:
      default:
        return '';
    }
  }
}
