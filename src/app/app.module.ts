/*** GENERAL IMPORTS */
import {APP_INITIALIZER, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing.module';
import {environment} from "../environments/environment";
/*** SERVICIOS **** */
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {ErrorInterceptor} from "./shared/interceptors/error.interceptor";
/*** THEME*/
import {ThemeModule} from './@theme/theme.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {THEME_CONFIG} from "./@theme/theme-config";
import {ModalModule} from './components/modal/modal.module';
/** LIBRARIES ***/
import {AngularFireModule} from "@angular/fire";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {MDBBootstrapModule} from "angular-bootstrap-md";
import {NgcCookieConsentConfig, NgcCookieConsentModule} from "ngx-cookieconsent";
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from './shared/shared.module';

import {StripeCardModule} from "./components/modal-stripe-card/stripe-card.module";
import {AuthGuard} from "./shared/guards/authguard.service";
import {InitService} from "./shared/services/init.service";
import {AccessGuard} from "./shared/guards/accessguard.service";
import {MAT_DATE_LOCALE} from "@angular/material";
import {AngularFirestoreModule} from "@angular/fire/firestore";

@NgModule({
  declarations: [AppComponent],
  imports: [
    // Basic
    AppRoutingModule,
    BrowserModule.withServerTransition({appId: 'walle'}),
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    SharedModule,

    // Libraries
    ModalModule,
    StripeCardModule,
    NgcCookieConsentModule.forRoot(THEME_CONFIG.cookieConfig as NgcCookieConsentConfig),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,

  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [
    AuthGuard, AccessGuard,
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [InitService],
      multi: true
    },
    {provide: MAT_DATE_LOCALE, useValue: 'es'}
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}

export function startupServiceFactory(initService: InitService): Function {
  return () => initService.startUp();
}
