import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {CallToActionComponent} from "./call-to-action.component";


@NgModule({
  imports: [
    ThemeModule,
  ],

  declarations: [
    CallToActionComponent
  ],

  exports: [
    CallToActionComponent
  ],

})
export class CallToActionModule {
}
