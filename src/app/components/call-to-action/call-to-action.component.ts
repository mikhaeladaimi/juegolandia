import {Component, Input} from "@angular/core";
import {InfoObject} from "../../models/info-object/InfoObject";
import {ObjectPositionType} from "../../models/enum/ObjectPositionType";

@Component({
  selector: "app-call-to-action",
  templateUrl: "call-to-action.component.html",
  styleUrls: ["call-to-action.component.scss"]
})

export class CallToActionComponent {

  @Input() object: InfoObject;
  @Input() background: any;
  @Input() parallaxEffect: boolean;
  @Input() textColor: string;
  @Input() buttonPosition: string;
  @Input() textAlignment: ObjectPositionType;

  alignment = ObjectPositionType;

  constructor() {

  }
}
