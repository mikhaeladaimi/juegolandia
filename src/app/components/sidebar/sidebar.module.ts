import {NgModule} from '@angular/core';
import {SidebarComponent} from './sidebar.component';
import {ThemeModule} from '../../@theme/theme.module';
import {RangeSliderModule} from "../range-slider/range-slider.module";
import {ProductListModule} from "../product-list/product-list.module";
import {FilterListModule} from "../filter-list/filter-list.module";
import {FilterSelectModule} from "../filter-select/filter-select.module";
import {CallToActionModule} from "../call-to-action/call-to-action.module";

@NgModule({
  imports: [ThemeModule,
    RangeSliderModule,
    FilterListModule,
    ProductListModule,
    CallToActionModule,
    FilterSelectModule],

  declarations: [
    SidebarComponent
  ],
  exports: [
    SidebarComponent]
})
export class SidebarModule {
}
