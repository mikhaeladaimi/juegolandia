import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PriceFilter} from "../../models/filter/PriceFilter";
import {ListFilter} from "../../models/filter/ListFilter";
import {IconsType} from "../../models/enum/IconsType";
import {SortedObject} from "../../models/sort/SortedObject";
import {FeaturedProductList} from "../../models/product/FeaturedProductList";

@Component({
  selector: 'app-sidebar',
  templateUrl: 'sidebar.component.html',
  styleUrls: ['sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() productName: string;
  @Input() priceFilter: PriceFilter;
  @Input() listSimpleFilter: ListFilter;
  @Input() dropdownFilter: ListFilter;
  @Input() productFeatured: FeaturedProductList;
  @Input() bannerList: SortedObject;
  @Input() displaySocialMedia: boolean;
  @Input() displaySearchBar: boolean;

  @Output() getMaxValue: EventEmitter<number> = new EventEmitter();
  @Output() getMinValue: EventEmitter<number> = new EventEmitter();
  @Output() filterPriceProduct: EventEmitter<PriceFilter> = new EventEmitter();
  @Output() itemsSelectedListFilter: EventEmitter<any[]> = new EventEmitter();
  @Output() itemsSelectedSelectFilter: EventEmitter<any[]> = new EventEmitter();
  @Output() onClearFilters: EventEmitter<boolean> = new EventEmitter();
  @Output() onSearchProduct: EventEmitter<string> = new EventEmitter();

  iconType = IconsType.SIMPLE;
  minSelected = 0;
  maxSelected;

  constructor() {
  }

  ngOnInit(): void {
    if (this.priceFilter) {
      this.minSelected = this.priceFilter.minValue;
      this.maxSelected = this.priceFilter.maxValue;
    }
  }

  setMinValue(event) {
    this.minSelected = event;
    this.priceFilter.minSelected = event;
    this.getMinValue.emit(event);
  }

  setMaxValue(event) {
    this.maxSelected = event;
    this.priceFilter.maxSelected = event;
    this.getMaxValue.emit(event);
  }

  filterPriceProducts() {
    this.filterPriceProduct.emit(this.priceFilter);
  }

  filterItemsListFilterSelected(event) {
    this.itemsSelectedListFilter.emit(event);
  }

  filterItemsSelectFilterSelected(event) {
    this.itemsSelectedSelectFilter.emit(event);
  }

  clearFilters() {
    this.minSelected = (this.priceFilter && this.priceFilter.minValue) ? this.priceFilter.minValue : null;
    this.maxSelected = (this.priceFilter && this.priceFilter.maxValue) ? this.priceFilter.maxValue : null;
    this.onClearFilters.emit(true);
  }

  searchProduct(name) {
    this.onSearchProduct.emit(name);
  }

}
