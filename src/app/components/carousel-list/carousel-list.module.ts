import {NgModule} from '@angular/core';
import {CarouselListComponent} from './carousel-list.component';
import {ThemeModule} from '../../@theme/theme.module';
import { NguCarouselModule} from "@ngu/carousel";

@NgModule({
  imports: [ThemeModule,
    NguCarouselModule],

  declarations: [
    CarouselListComponent
  ],
  exports: [
    CarouselListComponent]
})
export class CarouselListModule {
}
