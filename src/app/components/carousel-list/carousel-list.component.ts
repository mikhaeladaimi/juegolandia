import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GeneralLink} from "../../models/link/GeneralLink";
import {CarouselItem} from "../../models/carousel/CarouselItem";
import {ImageUtils} from "../../shared/utils/image.utils";

@Component({
  selector: 'app-carousel-list',
  templateUrl: 'carousel-list.component.html',
  styleUrls: ['carousel-list.component.scss']
})
export class CarouselListComponent implements OnInit {

  @Input() displayPrice: boolean;
  @Input() button: GeneralLink;
  @Input() items: CarouselItem[] = [];
  @Input() itemsToDisplay: number;
  @Input("xsItems") xsItems = 1;
  @Input("smItems") smItems = 2;
  @Input("mdItems") mdItems = 3;
  @Input("lgItems") lgItems = 3;
  @Input("defaultImg") defaultImg = ImageUtils.DEFAULT_IMAGE;


  @Output() itemClick: EventEmitter<number> = new EventEmitter();

  carousel: any = {};


  onItemClick(event) {
    this.itemClick.emit(event);
  }

  ngOnInit(): void {
    this.carousel = {
      grid: {xs: this.xsItems, sm: this.smItems, md: this.mdItems, lg: this.lgItems, all: 0},
      slide: 1,
      speed: 200,
      interval: {
        timing: 5000,
        initialDelay: 1000
      },
      point: {
        visible: false
      },
      load: 2,
      touch: true,
      loop: true,
      custom: 'object'
    };
  }


  setDefaultPic(item) {
    item.image = this.defaultImg;
  }


}
