import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AttributeValue} from "../../models/attribute/AttributeValue";
import {FilterUtils} from "../../shared/utils/filter.utils";

@Component({
  selector: 'app-filter-select',
  templateUrl: 'filter-select.component.html',
  styleUrls: ['filter-select.component.scss']
})
export class FilterSelectComponent implements OnInit {

  @Input() items: any[];
  @Input() listSelected: any[];
  @Input() title: string;

  @Output() onItemSelected: EventEmitter<any[]> = new EventEmitter();
  @Output() onFilteredList: EventEmitter<any[]> = new EventEmitter();

  selectedSubItems: Map<number, any> = new Map();
  selectedValue = [];

  constructor() {
  }

  ngOnInit(): void {
    FilterUtils.mergeArrayList(this.items, this.listSelected);
    this.getSelectedValues();
  }

  onItemChange(item, event) {
    this.selectedSubItems.set(item.id, event);
    this.emitFilteredAttributes();
  }

  private getSelectedValues() {
    let found = false;
    let valueToShow;
    if (this.items && this.items.length > 0) {
      this.items.filter(value => {
        value.values.filter(subValue => {
          if (subValue.isSelected) {
            found = true;
            valueToShow = subValue.value;
            this.selectedSubItems.set(value.id, subValue);
          }
        });
        if (found)
          this.selectedValue.push(valueToShow);
        else
          this.selectedValue.push("");
        found = false;
        valueToShow = undefined;
      });
    }
  }

  private emitFilteredAttributes() {
    const selectedItems = [];
    this.selectedSubItems.forEach((value: any, key: number) => {
      selectedItems.push(new AttributeValue().deserialize({id: key, value: value.value}));
    });
    this.onItemSelected.emit(selectedItems);
  }

}
