import {NgModule} from '@angular/core';
import {FilterSelectComponent} from './filter-select.component';
import {ThemeModule} from '../../@theme/theme.module';
import {MatSelectModule} from "@angular/material";

@NgModule({
  imports: [ThemeModule,
    MatSelectModule],

  declarations: [
    FilterSelectComponent
  ],
  exports: [
    FilterSelectComponent]
})
export class FilterSelectModule {
}
