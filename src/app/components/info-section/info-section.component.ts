import {Component, Input, OnInit} from "@angular/core";
import {DataSection} from "../../models/data-section/DataSection";
import {Media} from "../../models/media/Media";
import {DomSanitizer} from "@angular/platform-browser";
import {HorizontalAlignmentType} from "../../models/enum/HorizontalAlignmentType";

@Component({
  selector: 'app-info-section',
  templateUrl: 'info-section.component.html',
  styleUrls: ['info-section.component.scss']
})

export class InfoSectionComponent implements OnInit {

  @Input() data: DataSection;
  @Input() media: Media;

  alignment = HorizontalAlignmentType;

  constructor(private sanitizer: DomSanitizer) {

  }

  ngOnInit(): void {
    if (this.media.align == null) this.media.align = HorizontalAlignmentType.RIGHT;
  }

  getURL() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.media.video.url);
  }
}
