import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {InfoSectionComponent} from "./info-section.component";

@NgModule({
  imports: [ThemeModule],

  declarations: [
    InfoSectionComponent
  ],
  exports: [
    InfoSectionComponent
    ],
})

export class InfoSectionModule {
}
