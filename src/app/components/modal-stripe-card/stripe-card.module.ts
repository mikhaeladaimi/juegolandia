import { NgModule } from '@angular/core';
import {ThemeModule} from "../../@theme/theme.module";
import {StripeCardComponent} from "./stripe-card.component";
import {MatProgressSpinnerModule} from "@angular/material";

@NgModule({
  imports: [
    ThemeModule,
    MatProgressSpinnerModule
  ],

  declarations: [
    StripeCardComponent,
  ],
  entryComponents: [StripeCardComponent]
})
export class StripeCardModule { }
