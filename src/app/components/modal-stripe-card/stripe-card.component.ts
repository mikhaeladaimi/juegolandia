import {AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {User} from "../../models/user/User";
import {NGXLogger} from "ngx-logger";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {environment} from "../../../environments/environment";

declare var Stripe: any;

@Component({
  selector: 'app-stripe-card',
  templateUrl: './stripe-card.component.html',
  styleUrls: ['./stripe-card.component.scss'],
})
export class StripeCardComponent implements OnInit, AfterViewInit, OnDestroy {

  private stripe;
  private cardElement;


  @Input() user: User = new User();
  @Input() price: string;
  @Output() onErrorFound: EventEmitter<any> = new EventEmitter();
  @Output() onTokenFound: EventEmitter<any> = new EventEmitter();

  cardHandler = this.onChange.bind(this);


  error;
  loading = false;

  constructor(private activeModal: NgbActiveModal, private logger: NGXLogger, private cd: ChangeDetectorRef) {

  }

  ngOnInit(): void {
    if (!this.user.name) this.user.name = "Cliente";
    this.stripe = new Stripe(environment.stripeKey);
    const elements = this.stripe.elements();
    this.cardElement = elements.create('card');
    this.cardElement.mount('#card-element');
  }

  ngAfterViewInit(): void {
    this.cardElement.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.cardElement.removeEventListener('change', this.cardHandler);
    this.cardElement.destroy();
  }

  onChange({error}) {
   this.error = error ? error.message : null;
    this.cd.detectChanges();
  }

  onStripeError(error: Error) {
    this.logger.debug("Error found", error);
    this.onErrorFound.emit(error);
  }

  resolveToken(paymentMethod) {
    if (paymentMethod != undefined) {
      this.onTokenFound.emit(paymentMethod);
      this.activeModal.close(paymentMethod);
    }
  }

  createToken() {
    if (!this.error) {
      this.loading = true;
      this.stripe.createPaymentMethod('card', this.cardElement, {
        billing_details: {name: this.user.name}
      })
        .then(response => this.resolveToken(response.paymentMethod))
        .catch(error => this.onStripeError(error))
        .then(() => this.loading = false);
    }
  }

  dismissModal() {
    this.activeModal.dismiss();
  }
}
