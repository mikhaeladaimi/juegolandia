import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../models/product/Product";
import {ImageUtils} from "../../shared/utils/image.utils";
import {OrderItem} from "../../models/order/OrderItem";
import {Attribute} from "../../models/attribute/Attribute";
import {AttributeValue} from "../../models/attribute/AttributeValue";
import {CategoriesService} from "../../shared/services/categories.service";

@Component({
  selector: 'app-product-detail-view',
  templateUrl: './product-detail-view.component.html',
  styleUrls: ['./product-detail-view.component.scss']
})

export class ProductDetailViewComponent implements OnInit {

  @Input() product: Product;
  @Input() displayPrice: boolean;
  @Input() displayCart: boolean;

  @Output() shoppingCartClick: EventEmitter<OrderItem> = new EventEmitter();
  @Output() onAttributeChanges: EventEmitter<AttributeValue[]> = new EventEmitter();

  _attributes: Attribute[];
  _selectedAttributeValues: AttributeValue[];

  viewAttributes: Attribute[];
  quantity: number = 1;
  images: String[] = [];
  category;

  constructor(private categoryService: CategoriesService) {
  }

  ngOnInit(): void {
    if (!this.product.images || (this.product.images && this.product.images.length < 1)) {
      this.images.push(ImageUtils.DEFAULT_PRODUCT_IMAGE);
    } else {
      for (const image of this.product.images) {
        this.images.push(image);
      }
    }
    this.getCategoryData();
  }

  private getCategoryData() {
    this.categoryService.getCategory(this.product.category)
      .then(solve => this.category = solve);
  }

  addQuantity() {
    this.quantity++;
  }

  removeQuantity() {
    if (this.quantity > 1) {
      this.quantity--;
    }
  }

  addProductToCart() {
    this.shoppingCartClick.emit(new OrderItem().deserialize({
      product: this.product,
      quantity: +this.quantity
    }));
  }

}
