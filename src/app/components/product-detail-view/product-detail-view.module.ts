import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {ProductDetailViewComponent} from "./product-detail-view.component";
import {SlideshowModule} from "ng-simple-slideshow";
import {MatChipsModule} from "@angular/material";


@NgModule({
  imports: [ThemeModule, SlideshowModule, MatChipsModule],

  declarations: [
    ProductDetailViewComponent
  ],
  exports: [ProductDetailViewComponent]
})
export class ProductDetailViewModule {
}
