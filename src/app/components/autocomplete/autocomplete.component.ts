import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {FormControl} from '@angular/forms';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {Property} from '../../models/property/Property';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss'],
})

export class AutocompleteComponent {
  @Input() set list(list: any[]) {
    this.instantiateFilteredList(list);
    this.localList = list;
  }

  @Input() set selected(selected: any) {
    this.localSelected = selected;
    if (this.disabled) {
      this.formCtrl = new FormControl({value: {name: this.placeholder}, disabled: true});
    } else {
      this.formCtrl = new FormControl({value: {name: this.localSelected && this.localSelected.name ? this.localSelected.name : ''}, disabled: false});
    }
  }

  @Input() set error(error: any) {
    this.localError = error;
  }

  @Input() placeholder = "Introduce una localidad...";
  @Input() disabled: boolean;

  @Input() emitProperty = true;

  @Output() onInputValueChange: EventEmitter<any> = new EventEmitter();
  @Output() onSelectionChange: EventEmitter<any> = new EventEmitter();

  formCtrl: FormControl = new FormControl();
  filteredList: Observable<any[]>;

  localList;
  localSelected;
  localError;

  constructor() {
  }

  addElement() {
    this.onSelectionChange.emit(this.formCtrl.value);
  }

  keyUp(event) {
    event.preventDefault();
    if (event.keyCode != 37 && event.keyCode != 38 && event.keyCode != 39 && event.keyCode != 40 && event.keyCode != 13)
      this.onInputValueChange.emit(this.formCtrl.value);
  }

  displayFn(locality?: Property<any>): String | undefined {
    return locality != null ? locality.name : null;
  }

  private instantiateFilteredList(list) {
    if (list) {
      if (!this.formCtrl || (this.formCtrl.disabled)) this.formCtrl = new FormControl({
        value: this.localSelected && this.localSelected.name ? this.localSelected.name : '',
        disabled: false
      });
      this.disabled = false;
      this.filteredList = this.formCtrl.valueChanges
        .pipe(
          startWith<string | Property<any>>(''),
          map(value => typeof value == 'string' ? value : value.name),
          map(name => name ? this.filterList(name) : list.slice()));
    } else {
      this.formCtrl = new FormControl({value: {name: this.localSelected && this.localSelected.name ? this.localSelected.name : this.placeholder}, disabled: true});
      this.disabled = true;
    }
  }

  private filterList(name: String): Property<any>[] {
    return this.localList.filter(item =>
      item.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

}
