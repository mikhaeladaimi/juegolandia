import {MatAutocompleteModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {NgModule} from '@angular/core';

import {ThemeModule} from '../../@theme/theme.module';

import {AutocompleteComponent} from './autocomplete.component';

@NgModule({
  imports: [
    ThemeModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
  ],

  declarations: [AutocompleteComponent],
  exports: [AutocompleteComponent],
})
export class AutocompleteModule {
}
