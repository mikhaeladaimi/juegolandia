import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../models/product/Product";

@Component({
  selector: 'app-featured-product-view',
  templateUrl: 'featured-product-view.component.html',
  styleUrls: ['featured-product-view.component.scss']
})
export class FeaturedProductViewComponent implements OnInit {

  @Input() displayPrice: boolean;
  @Input() displayCart: boolean;
  @Input() product: Product;
  @Input() displayEffects: boolean;

  @Output() productClick: EventEmitter<number> = new EventEmitter();
  @Output() shoppingCartClick: EventEmitter<number> = new EventEmitter();

  image: String;

  ngOnInit(): void {
    this.image = this.getImage();
  }

  onProductClick(event) {
    this.productClick.emit(event.productId);
  }

  getImage() {
    return (this.product.images && this.product.images.length > 0) ? this.product.images[0] : null;
  }
}
