import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {FeaturedProductViewComponent} from "./featured-product-view.component";


@NgModule({
  imports: [ThemeModule],

  declarations: [
    FeaturedProductViewComponent
  ],
  exports: [FeaturedProductViewComponent]
})
export class FeaturedProductViewModule {
}
