import {NgModule} from '@angular/core';
import {RangeSliderComponent} from './range-slider.component';
import {ThemeModule} from '../../@theme/theme.module';
import {Ng5SliderModule} from "ng5-slider";

@NgModule({
  imports: [ThemeModule,
    Ng5SliderModule],

  declarations: [
    RangeSliderComponent
  ],
  exports: [
    RangeSliderComponent]
})
export class RangeSliderModule {
}
