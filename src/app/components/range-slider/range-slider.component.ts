import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Options} from "ng5-slider";

@Component({
  selector: 'app-range-slider',
  templateUrl: 'range-slider.component.html',
  styleUrls: ['range-slider.component.scss']
})
export class RangeSliderComponent {

  @Input() minValue: number;
  @Input() maxValue: number;
  @Input() options: Options;

  @Output() onMinValueSelected: EventEmitter<number> = new EventEmitter();
  @Output() onMaxValueSelected: EventEmitter<number> = new EventEmitter();

  constructor() {
  }

  onChange(event) {
    this.onMinValueSelected.emit(event.value);
    this.onMaxValueSelected.emit(event.highValue);
  }

}
