import {Component, Input} from '@angular/core';
import {ModalUtils} from "../../shared/utils/modal.utils";
import {NGXLogger} from "ngx-logger";
import {ReCaptchaV3Service} from "ng-recaptcha";
import {CaptchaService} from "../../shared/services/captcha.service";
import {ErrorResponseUtils} from "../../shared/utils/errorResponse.utils";
import {Contact} from "../../models/contact/Contact";
import {SlugsUtils} from "../../shared/utils/slugs.utils";
import {ContactService} from "../../shared/services/contact.service";

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
})
export class ContactFormComponent {

  @Input() formConfig: any;

  contact: Contact = new Contact();
  loading: boolean = false;

  // region routes
  privacyRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.PRIVACY);
  noticeRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.NOTICE);

  // endregion

  constructor(private contactService: ContactService, private modalUtils: ModalUtils, private logger: NGXLogger,
              private recaptchaV3Service: ReCaptchaV3Service, private captchaService: CaptchaService) {
  }

  createContact() {
    this.loading = true;
    this.recaptchaV3Service.execute('homepage')
      .subscribe((token) => this.validateCaptcha(token));
  }

  private validateCaptcha(token) {
    this.captchaService.validateCaptcha(token)
      .then(response => this.sendContact())
      .catch(() => {
        this.onError();
      });
  }

  private sendContact() {
    this.contactService.postContactForm(this.contact)
      .then(() => {
        this.modalUtils.openModal("¡Conseguido!", "El mensaje se ha enviado con éxito. Muchas gracias por contactar con nosotros.");
      })
      .catch(error => this.onError(error))
      .then(() => {
        this.contact = new Contact();
        this.loading = false;
      });
  }

  private onError(error?) {
    let message = error && ErrorResponseUtils.getStringError(error);
    if (!message) message = "Se ha producido un error";
    this.logger.debug("Error encontrado al enviar el email de contacto: ", message);
    this.loading = false;
    this.modalUtils.openErrorModal("Hubo un error al intentar enviar tu mensaje. Por favor inténtalo de nuevo más tarde");
  }
}
