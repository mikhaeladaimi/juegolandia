import {NgModule} from '@angular/core';
import {ContactFormComponent} from "./contact-form.component";
import {ThemeModule} from "../../@theme/theme.module";
import {RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module} from "ng-recaptcha";
import {environment} from "../../../environments/environment";
import {TrimDirective} from "../../@theme/directives/trim.directive";

@NgModule({
  imports: [
    ThemeModule, RecaptchaV3Module
  ],
  providers: [
    {provide: RECAPTCHA_V3_SITE_KEY, useValue: environment.reCaptchaSiteKey},
  ],
  declarations: [
    ContactFormComponent, TrimDirective,
  ],
  exports: [
    ContactFormComponent
  ]
})
export class ContactFormModule {
}
