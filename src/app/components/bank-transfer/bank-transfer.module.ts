import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import { BankTransferComponent } from './bank-transfer.component';


@NgModule({
  imports: [
    ThemeModule
  ],

  declarations: [
    BankTransferComponent
  ],

  exports: [
    BankTransferComponent
  ],

})
export class BankTransferModule {
}
