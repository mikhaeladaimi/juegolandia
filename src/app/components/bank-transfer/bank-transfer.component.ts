import { Component, Input, OnInit } from "@angular/core";
import { Order } from "../../models/order/Order";
import { THEME_CONFIG } from "../../@theme/theme-config";
import { ModalUtils } from "../../shared/utils/modal.utils";

@Component({
  selector: "app-bank-transfer",
  templateUrl: "bank-transfer.component.html",
  styleUrls: ["bank-transfer.component.scss"]
})

export class BankTransferComponent implements OnInit {

  @Input() order: Order;
  concept: string;
  account: string;
  owner: string;
  missingData: boolean = false;
  functionHandler: any;

  constructor(private modalUtils: ModalUtils) {
  }

  ngOnInit(): void {
    if (THEME_CONFIG.bankAccount && this.order && this.order.id) {
      this.account = THEME_CONFIG.bankAccount.account;
      this.owner = THEME_CONFIG.bankAccount.owner;
      this.concept = THEME_CONFIG.bankAccount.baseConcept;
    } else {
      this.missingData = true;
    }
  }

  copyToClipboard() {
    this.functionHandler = this.copyPlainText.bind(this);
    document.addEventListener('copy', this.functionHandler, true);
    document.execCommand('copy');
  }

  copyPlainText(e: ClipboardEvent) {
    e.clipboardData.setData('text/plain', (this.account));
    e.preventDefault();
    document.removeEventListener('copy', this.functionHandler, true);
    this.modalUtils.openModal("¡Copiado!", "Ya tienes la cuenta bancaria en el portapapeles");
  }
}

