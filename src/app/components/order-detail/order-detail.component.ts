import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Order} from "../../models/order/Order";
import {OrderStatusType} from "../../models/enum/OrderStatusType";

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})

export class OrderDetailComponent implements OnInit {

  @Input() order: Order;
  @Output() onPrint: EventEmitter<any> = new EventEmitter();

  isBankTransfer;

  orderCancelled = OrderStatusType.CANCELLED;

  ngOnInit(): void {
    this.isBankTransfer = (this.order && !this.order.isCreditCard);
  }

  printOrder() {
    this.onPrint.emit();
  }
}
