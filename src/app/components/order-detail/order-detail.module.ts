import {LOCALE_ID, NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {OrderDetailComponent} from "./order-detail.component";
import localeEs from '@angular/common/locales/es';
import {registerLocaleData} from "@angular/common";
import {BankTransferModule} from "../bank-transfer/bank-transfer.module";

registerLocaleData(localeEs, 'es');

@NgModule({
  imports: [
    ThemeModule,
    BankTransferModule
  ],
  providers: [{provide: LOCALE_ID, useValue: 'es'}],
  declarations: [
    OrderDetailComponent
  ],
  exports: [OrderDetailComponent]
})
export class OrderDetailModule {
}
