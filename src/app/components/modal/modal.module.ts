import {NgModule} from '@angular/core';
import {ModalComponent} from './modal.component';
import {ThemeModule} from '../../@theme/theme.module';

@NgModule({
  imports: [ThemeModule],

  declarations: [
    ModalComponent
  ],
  entryComponents: [ModalComponent]
})
export class ModalModule {
}
