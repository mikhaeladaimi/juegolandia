import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Component} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: 'modal.component.html',
  styleUrls: ['modal.component.scss']
})
export class ModalComponent {

  modalHeader: string;
  modalContent = "";

  cancel = false;
  swipe = false;

  acceptText: string = "Aceptar";
  cancelText: string = "Cancelar";
  loading: boolean = false;

  constructor(private activeModal: NgbActiveModal) {
  }

  confirmModal() {
    this.activeModal.close();
  }

  dismissModal() {
    this.activeModal.dismiss();
  }
}
