import {Component, Input} from '@angular/core';
import {ImageUtils} from "../../../shared/utils/image.utils";

@Component({
  selector: 'app-featured-post',
  templateUrl: './featured-post.component.html',
  styleUrls: ['./featured-post.component.scss']
})
export class FeaturedPostComponent {

  @Input() post;
  @Input() defaultImg = ImageUtils.DEFAULT_IMAGE;

  constructor() {
  }

  navigateToPost(url) {
    window.open(url, '_blank');
  }
}
