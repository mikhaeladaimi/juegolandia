import {NgModule} from '@angular/core';
import {ThemeModule} from '../../../@theme/theme.module';
import {FeaturedPostComponent} from "./featured-post.component";


@NgModule({
  imports: [
    ThemeModule
  ],

  declarations: [
    FeaturedPostComponent
  ],

  exports: [
   FeaturedPostComponent
  ],

})
export class FeaturedPostModule {
}
