import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {ImageUtils} from "../../shared/utils/image.utils";
import {FeedService} from "../../shared/services/feed.service";
import {FeedItem} from "../../models/feed/FeedItem";


@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  @Input() defaultImg = ImageUtils.DEFAULT_BLOG_IMAGE;
  @Input() url = FeedService.BLOG_URL;
  @Input() showPagination = true;
  @Input() showFeaturedPost = false;
  @Input() portraitMode = false;
  @Input() maxItems = 6;
  @Input() col = "col-xl-4 col-lg-6";
  @Input() categoriesExclude;
  @Input() categories;
  @Output() onLoading: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onError: EventEmitter<boolean> = new EventEmitter<boolean>();

  items;
  itemsToDisplay: FeedItem[] = [];
  page = 1;
  loading = true;
  maxPage;

  featuredPost;

  constructor(private feedService: FeedService) {
    this.items = [];
  }

  ngOnInit(): void {
    if (this.categoriesExclude && !isNaN(this.categoriesExclude)) this.url = this.url + "&categories_exclude=" + this.categoriesExclude;
    if (this.categories) this.url = this.url + "&categories=" + this.categories;
    this.getContentFromPage();
  }

  getContentFromPage() {
    this.onLoading.emit(true);
    this.feedService.getFeedContent(this.url + "&page=" + this.page).then(data => {
      this.items = data.posts;
      this.maxPage = data.maxPage;
      this.itemsToDisplay = (this.showFeaturedPost) ? this.saveFeaturedPost() : this.addPosts(data.posts);
      if (this.itemsToDisplay.length > this.maxItems) this.itemsToDisplay = this.itemsToDisplay.slice(1, this.maxItems + 1);
      this.loading = false;
      this.onLoading.emit(false);
      if (this.items.length < 1) this.onError.next();
    }).catch(error => this.onError.next(error));
  }

  saveFeaturedPost() {
    if (!this.featuredPost) this.featuredPost = this.items[0];
    return this.items.slice(1, this.maxItems + 1);
  }

  addPosts(posts) {
    return this.itemsToDisplay.concat(posts);
  }

  navigateToPost(url) {
    window.open(url, '_blank');
  }

  nextPage() {
    if (this.page != this.maxPage) {
      this.page++;
      this.getContentFromPage();
    }
  }

}
