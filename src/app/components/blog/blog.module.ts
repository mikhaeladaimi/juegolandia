import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {BlogComponent} from "./blog.component";
import {FeaturedPostModule} from "./featured-post/featured-post.module";


@NgModule({
  imports: [
    ThemeModule, FeaturedPostModule
  ],

  declarations: [
    BlogComponent,
  ],

  exports: [
   BlogComponent
  ],

})
export class BlogModule {
}
