import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TabsData} from "../../models/tabs-data/TabsData";

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent {

  @Input() tabsData: TabsData[];

  @Output() onTabClick: EventEmitter<boolean> = new EventEmitter();

  onTabItemClick(tabItem: TabsData) {
    if (tabItem.hasOnClick)
      this.onTabClick.emit(true);
  }

}
