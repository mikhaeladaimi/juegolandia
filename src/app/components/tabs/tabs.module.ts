import {NgModule} from '@angular/core';
import {TabsComponent} from './tabs.component';
import {ThemeModule} from '../../@theme/theme.module';
import {MatTabsModule} from "@angular/material";

@NgModule({
  imports: [
    ThemeModule,
    MatTabsModule
  ],

  declarations: [
    TabsComponent
  ],

  exports: [
    TabsComponent
  ]
})
export class TabsModule {
}

