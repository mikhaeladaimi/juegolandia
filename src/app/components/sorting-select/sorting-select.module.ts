import { NgModule } from '@angular/core';
import {ThemeModule} from "../../@theme/theme.module";
import {SortingSelectComponent} from "./sorting-select.component";
import {MatFormFieldModule, MatSelectModule} from "@angular/material";

@NgModule({
  imports: [
    ThemeModule,
    MatFormFieldModule,
    MatSelectModule
  ],

  declarations: [
    SortingSelectComponent,
  ],
  exports: [
    SortingSelectComponent
  ]
})
export class SortingSelectModule { }
