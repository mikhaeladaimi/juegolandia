import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-sorting-select',
  templateUrl: './sorting-select.component.html',
  styleUrls: ['./sorting-select.component.scss'],
})
export class SortingSelectComponent {

  @Input() options: any[];

  @Output() onOptionChanged: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  optionChanged(option) {
    this.onOptionChanged.emit(option);
  }
}
