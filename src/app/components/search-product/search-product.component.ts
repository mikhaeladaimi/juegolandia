import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Product} from "../../models/product/Product";

@Component({
  selector: 'app-search-product',
  templateUrl: 'search-product.component.html',
  styleUrls: ['search-product.component.scss']
})
export class SearchProductComponent {

  @Input() products: Product[];
  @Input() productName: string;

  @Output() onSearch: EventEmitter<string> = new EventEmitter();

  constructor() {

  }

  emitSearch() {
    this.onSearch.emit(this.productName);
  }
}
