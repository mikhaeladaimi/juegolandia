import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {SearchProductComponent} from "./search-product.component";

@NgModule({
  imports: [ThemeModule],

  exports: [
    SearchProductComponent
  ]
})
export class SearchProductModule {
}
