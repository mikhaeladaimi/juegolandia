import {NgModule} from '@angular/core';
import {MapComponent} from "./map.component";
import {ThemeModule} from "../../@theme/theme.module";

@NgModule({
  imports: [
    ThemeModule
  ],

  declarations: [
    MapComponent,
  ],
  exports: [
    MapComponent
  ]
})
export class MapModule {
}
