import {DomSanitizer} from "@angular/platform-browser";
import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-map-frame',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @Input() src: string;
  @Input() width: string = "100%";
  @Input() height: string = "25em";
  srcBypassed;

  constructor(public sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.srcBypassed = this.sanitizer.bypassSecurityTrustResourceUrl(this.src);
  }
}
