import {NgModule} from '@angular/core';
import {FilterListComponent} from './filter-list.component';
import {ThemeModule} from '../../@theme/theme.module';

@NgModule({
  imports: [ThemeModule],

  declarations: [
    FilterListComponent
  ],
  exports: [
    FilterListComponent]
})
export class FilterListModule {
}
