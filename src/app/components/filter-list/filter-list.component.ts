import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FilterUtils} from "../../shared/utils/filter.utils";

@Component({
  selector: 'app-filter-list',
  templateUrl: 'filter-list.component.html',
  styleUrls: ['filter-list.component.scss']
})
export class FilterListComponent {

  @Input() items: any[];
  @Input() title: string;

  @Input() set listSelected(list: any[]) {
    FilterListComponent.clearList(this.items);
    this.items = FilterUtils.mergeArray(this.items, list);
  }

  @Output() onItemsSelected: EventEmitter<any[]> = new EventEmitter();

  constructor() {
  }

  onItemClick(clickedItem, clickedSubItem, event) {
    event.stopPropagation();
    this.filterSelectedItems(clickedItem, clickedSubItem);
    this.onItemsSelected.emit(FilterUtils.getSelectedElements(this.items));
  }

  private static clearList(list) {
    list.filter(item => {
      item.isSelected = false;
      if (item.children) {
        item.children.filter(child => {
          child.isSelected = false;
        });
      }
    });
  }

  private filterSelectedItems(clickedItem, clickedSubItem) {
    const list = this.items.filter(item => item.id == clickedItem.id);

    if (list.length > 0) {
      const item = list[0];
      if (clickedSubItem) {
        item.children.filter(subItem => {
          if (clickedSubItem.slug == subItem.slug) {
            (subItem.isSelected) ? FilterListComponent.clearList(this.items) : this.selectItem(subItem);
          }
        });

      } else {
        (item.isSelected) ? FilterListComponent.clearList(this.items) : this.selectItem(item);
      }
    }

  }

  private selectItem(item) {
    FilterListComponent.clearList(this.items);
    item.isSelected = true;
  }

}
