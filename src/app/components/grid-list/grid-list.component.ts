import {Component, Input, OnInit} from '@angular/core';
import {Grid} from "../../models/grid/Grid";

@Component({
  selector: 'app-grid-list',
  templateUrl: 'grid-list.component.html',
  styleUrls: ['grid-list.component.scss']
})

export class GridListComponent implements OnInit {

  @Input() gridList: Grid[];
  @Input() itemsToDisplay: number;
  @Input() gridClass: string;

  gridListToDisplay: Grid[] = [];
  limitItems = 0;

  constructor() {
  }

  ngOnInit(): void {
    if (this.gridList)
      this.initializeGridToDisplay();
  }

  addElementsToDisplay() {
    this.limitItems = this.getGridListToDisplayLength() + this.itemsToDisplay;

    for (let i = 0; i < this.gridList.length && (this.getGridListToDisplayLength() < this.limitItems); i++) {
      if (i >= (this.limitItems - this.itemsToDisplay))
        this.gridListToDisplay.push(this.gridList[i]);
    }
  }

  private initializeGridToDisplay() {
    for (let i = 0; i < this.gridList.length && (this.getGridListToDisplayLength() < this.itemsToDisplay); i++) {
      this.gridListToDisplay.push(this.gridList[i]);
    }
  }

  private getGridListToDisplayLength() {
    return this.gridListToDisplay.length ? this.gridListToDisplay.length : 0;
  }

}
