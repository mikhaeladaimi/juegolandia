import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {GridListComponent} from "./grid-list.component";
import {GridItemComponent} from "./grid-item/grid-item.component";

@NgModule({
  imports: [ThemeModule],

  declarations: [
    GridListComponent,
    GridItemComponent
  ],
  exports: [GridListComponent]
})
export class GridListModule {
}
