import {Component, Input} from '@angular/core';
import {ImageUtils} from "../../../shared/utils/image.utils";

@Component({
  selector: 'app-grid-item',
  templateUrl: 'grid-item.component.html',
  styleUrls: ['grid-item.component.scss']
})
export class GridItemComponent {

  @Input() title: string;
  @Input() image: string;
  @Input() link: string;
  @Input() target: string;
  @Input() query: any;

  constructor() {

  }

  setDefaultPic() {
    this.image = ImageUtils.DEFAULT_IMAGE;
  }
}
