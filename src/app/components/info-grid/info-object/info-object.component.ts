import {Component, Input} from '@angular/core';
import {InfoObject} from "../../../models/info-object/InfoObject";
import {HorizontalAlignmentType} from "../../../models/enum/HorizontalAlignmentType";
import {ObjectPositionType} from "../../../models/enum/ObjectPositionType";

@Component({
  selector: 'app-info-object',
  templateUrl: 'info-object.component.html',
  styleUrls: ['info-object.component.scss']
})
export class InfoObjectComponent {

  @Input() iconPosition: ObjectPositionType;
  @Input() infoObject: InfoObject;
  @Input() numberOfColumns: string;
  @Input() textAlignment: HorizontalAlignmentType;
  @Input() backgroundItem: string;

}
