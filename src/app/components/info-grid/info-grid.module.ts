import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {InfoObjectComponent} from "./info-object/info-object.component";
import {InfoGridComponent} from "./info-grid.component";

@NgModule({
  imports: [ThemeModule],

  declarations: [
    InfoGridComponent,
    InfoObjectComponent
  ],
  exports: [InfoGridComponent]
})
export class InfoGridModule {
}
