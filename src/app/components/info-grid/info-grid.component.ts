import {Component, Input} from '@angular/core';
import {InfoObject} from "../../models/info-object/InfoObject";
import {HorizontalAlignmentType} from "../../models/enum/HorizontalAlignmentType";
import {ObjectPositionType} from "../../models/enum/ObjectPositionType";

@Component({
  selector: 'app-info-grid',
  templateUrl: 'info-grid.component.html',
  styleUrls: ['info-grid.component.scss']
})
export class InfoGridComponent {

  @Input() iconPosition: ObjectPositionType;
  @Input() infoObjects: InfoObject[];
  @Input() columnClass: string;
  @Input() textAlignment: HorizontalAlignmentType;
  @Input() backgroundItem: string;

  constructor() {
  }

}
