import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Slide} from "../../models/slide/Slide";
import {HorizontalAlignmentType} from "../../models/enum/HorizontalAlignmentType";
import {VerticalAlignmentType} from "../../models/enum/VerticalAlignmentType";

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss']
})

export class SlideComponent {

  @Input() height: string;
  @Input() slideObjects: Slide[];
  @Input() interval: number;
  @Input() displayArrows: boolean;
  @Input() displayPoints: boolean;
  @Input() animation: any;

  @Output() onImageClickEvent: EventEmitter<any> = new EventEmitter();

  horizontalAlignment = HorizontalAlignmentType;
  verticalAlignment = VerticalAlignmentType;

  constructor() {
  }

  onImageClick(slide: Slide) {
    this.onImageClickEvent.emit(slide.background.url);
  }

}
