
import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import {SlideComponent} from "./slide.component";
import {NgbCarouselModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  imports: [ThemeModule,
    NgbCarouselModule],

  declarations: [
    SlideComponent
  ],
  exports: [SlideComponent]
})
export class SlideModule { }
