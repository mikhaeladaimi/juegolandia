import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Product} from "../../models/product/Product";

@Component({
  selector: 'app-product-list',
  templateUrl: 'product-list.component.html',
  styleUrls: ['product-list.component.scss']
})
export class ProductListComponent {

  @Input() displayPrice: boolean;
  @Input() displayCart: boolean;
  @Input() displayPriceOnList: boolean;
  @Input() products: Product[];
  @Input() loading: boolean;

  @Output() shoppingCartClick: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  onShoppingCartClick(event) {
    this.shoppingCartClick.emit(event);
  }

}
