import {NgModule} from '@angular/core';
import {ProductListComponent} from './product-list.component';
import {ThemeModule} from '../../@theme/theme.module';
import {ProductViewComponent} from "./product-view/product-view.component";
import {MatPaginatorIntl, MatPaginatorModule} from "@angular/material";
import {MatPaginatorIntlEsp} from "../../shared/utils/mat.paginator.int.esp";

@NgModule({
  imports: [ThemeModule,
    MatPaginatorModule],

  providers: [
    {provide: MatPaginatorIntl, useClass: MatPaginatorIntlEsp},
  ],

  declarations: [
    ProductListComponent, ProductViewComponent
  ],
  exports: [
    ProductListComponent,
    ProductViewComponent]
})
export class ProductListModule {
}
