import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Product} from "../../../models/product/Product";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-product-view',
  templateUrl: 'product-view.component.html',
  styleUrls: ['product-view.component.scss']
})
export class ProductViewComponent {

  @Input() displayPrice: boolean;
  @Input() displayPriceOnList: boolean;
  @Input() displayCart: boolean;
  @Input() displayEffects: boolean;

  @Input("product") product: Product;

  @Output() shoppingCartClick: EventEmitter<string> = new EventEmitter();

  allowPurchase: boolean = true;

  // region routes
  productDetailRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.PRODUCT_DETAIL);

  // endregion

  constructor() {
  }

  addProductToCart(event) {
    event.preventDefault();
    event.stopPropagation();
    this.shoppingCartClick.emit(this.product.slug);
  }

}
