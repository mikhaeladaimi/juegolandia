import {NgModule} from '@angular/core';

import {ThemeModule} from '../../@theme/theme.module';
import {FaqsComponent} from "./faqs.component";
import {MatExpansionModule, MatIconModule} from "@angular/material";


@NgModule({
  imports: [
    ThemeModule,
    MatExpansionModule,
    MatIconModule
  ],

  declarations: [
    FaqsComponent
  ],

  exports: [
    FaqsComponent
  ],

})
export class FaqsModule {
}
