import {Component, Input, OnInit} from "@angular/core";
import {Question} from "../../models/question/Question";
import {ExpandableView} from "../../models/expandable-view/ExpandableView";

@Component({
  selector: "app-faqs",
  templateUrl: "faqs.component.html",
  styleUrls: ["faqs.component.scss"]
})

export class FaqsComponent implements OnInit {

  @Input() questions: Question[];
  @Input() openedIcon: string;
  @Input() closedIcon: string;

  expandableViewList: ExpandableView<Question>[];

  constructor() {
  }

  ngOnInit(): void {
    if (this.questions && this.questions.length > 0) this.setExpandableViewData();
  }

  private setExpandableViewData() {
    this.expandableViewList = [];
    for (const question of this.questions) {
      this.expandableViewList.push(new ExpandableView<Question>(question, false));
    }
  }

}

