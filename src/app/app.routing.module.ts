import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './@theme/components/layout/layout.component';
import {AuthGuard} from "./shared/guards/authguard.service";
import {AccessGuard} from "./shared/guards/accessguard.service";
import {SlugsUtils} from "./shared/utils/slugs.utils";

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: SlugsUtils.HOME,
        loadChildren: './pages/home/home.module#HomeModule'
      },
      {
        path: 'sample',
        loadChildren: './pages/sample/sample.module#SampleModule'
      },
      {
        path: SlugsUtils.SHOP,
        loadChildren: './pages/shop/shop.module#ShopModule'
      },
      {
        path: SlugsUtils.CONTACT,
        loadChildren: './pages/contact/contact.module#ContactModule'
      },
      {
        path: SlugsUtils.COMPANY,
        loadChildren: './pages/company/company.module#CompanyModule'
      },
      {
        path: SlugsUtils.LEGAL + '/:section',
        loadChildren: './pages/legal/legal.module#LegalModule'
      },
      {
        path: SlugsUtils.ACCESS,
        loadChildren: './pages/access/access.module#AccessModule',
        canActivate: [AccessGuard],
      },
      {
        path: SlugsUtils.MY_PROFILE,
        loadChildren: './pages/my-profile/my-profile.module#MyProfileModule',
        canActivate: [AuthGuard],
      },
      {
        path: SlugsUtils.ORDER + '/:id',
        loadChildren: './pages/order/order.module#OrderModule'
      },
      {
        path: SlugsUtils.CART_DETAIL,
        loadChildren: './pages/shop/cart-detail/cart-detail.module#CartDetailModule'
      },
      {
        path: SlugsUtils.CHECKOUT,
        loadChildren: './pages/checkout/checkout.module#CheckoutModule'
      },
      {
        path: SlugsUtils.ERROR_404,
        loadChildren: './pages/error-404-page/error-404-page.module#Error404PageModule',
      },
      {path: '**', redirectTo: SlugsUtils.ERROR_404, pathMatch: 'full'}
    ],
  },
  {path: '**', redirectTo: SlugsUtils.ERROR_404, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {initialNavigation: 'enabled'})],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
