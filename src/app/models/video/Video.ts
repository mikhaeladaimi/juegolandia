import {Deserializable} from "../Deserializable";

export class Video implements Deserializable {

  constructor(public title?: string,
              public url?: string,
              public description?: string,
              public startAlone?: any) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Video {
    return new Video().deserialize(this);
  }
}
