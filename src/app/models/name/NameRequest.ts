import {Deserializable} from "../Deserializable";

export class NameRequest implements Deserializable {

  constructor(public name?: string) {
  }

  public deserialize(input) {
    this.name = input.name;
    return this;
  }
}
