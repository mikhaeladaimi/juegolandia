import {Deserializable} from "../Deserializable";

export class Grid implements Deserializable {

  constructor(public title?: string,
              public image?: string,
              public link?: string,
              public query?: any,
              public target?: string,
              public description?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    if (input.name) this.title = input.name;
    if (input.image && input.image.url) this.image = input.image.url;
    return this;
  }

  clone(): Grid {
    return new Grid().deserialize(this);
  }
}
