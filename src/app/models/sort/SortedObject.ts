import {Deserializable} from "../Deserializable";

export class SortedObject implements Deserializable {

  constructor(public position?: number,
              public object?: any) {
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }

  clone(): SortedObject {
    return new SortedObject().deserialize(this);
  }
}
