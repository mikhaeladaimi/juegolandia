import {Deserializable} from "../Deserializable";
import {SortRequest} from "./SortRequest";

export class SortListRequest implements Deserializable {

  constructor(public order?: SortRequest[]) {
  }

  deserialize(input: any): this {
    this.order = input.order;
    return this;
  }
}
