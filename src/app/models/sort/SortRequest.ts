import {Deserializable} from "../Deserializable";

export class SortRequest implements Deserializable {

  constructor(public direction?: string,
              public field?: string) {
  }

  deserialize(input: any): this {
    this.direction = input.direction;
    this.field = input.field;
    return this;
  }
}
