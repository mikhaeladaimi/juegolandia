import {Deserializable} from "../Deserializable";
import {ObjectPositionType} from "../enum/ObjectPositionType";

export class MenuConfig implements Deserializable {

  isLogoCentered: boolean = false;
  isLogoTop: boolean = false;
  isLogoBottom: boolean = false;
  isLogoRight: boolean = false;
  isLogoLeft: boolean = false;

  constructor(public logoPosition?: ObjectPositionType,
              public isSticky?: boolean,
              public isBoxed?: boolean,
              public displayCart?: boolean,
              public displayUser?: boolean) {
  }

  deserialize(input) {
    Object.assign(this, input);
    switch (this.logoPosition) {
      case ObjectPositionType.TOP:
        this.isLogoTop = true;
        break;
      case ObjectPositionType.RIGHT:
        this.isLogoRight = true;
        break;
      case ObjectPositionType.DOWN:
        this.isLogoBottom = true;
        break;
      case ObjectPositionType.LEFT:
        this.isLogoLeft = true;
        break;
      case ObjectPositionType.CENTER:
        this.isLogoCentered = true;
    }
    return this;
  }

  clone(): MenuConfig {
    return new MenuConfig().deserialize(this);
  }
}
