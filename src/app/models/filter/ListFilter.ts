import {Deserializable} from "../Deserializable";

export class ListFilter implements Deserializable {

  constructor(public title?: string,
              public items?: any[],
              public selectedItems?: any[],
              public order?: number) {
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }

  clone(): ListFilter {
    return new ListFilter().deserialize(this);
  }
}
