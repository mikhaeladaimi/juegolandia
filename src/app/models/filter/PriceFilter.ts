import {Options} from "ng5-slider";
import {Deserializable} from "../Deserializable";

/**
 * Price filter
 */
export class PriceFilter implements Deserializable {

  constructor(public minValue?: number,
              public maxValue?: number,
              public options?: Options,
              public order?: number,
              public minSelected?: number,
              public maxSelected?: number) {
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }

  clone(): PriceFilter {
    return new PriceFilter().deserialize(this);
  }
}
