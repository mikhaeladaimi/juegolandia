import {GeneralLink} from "../link/GeneralLink";
import {Deserializable} from "../Deserializable";
import {HorizontalAlignmentType} from "../enum/HorizontalAlignmentType";

export class DataSection implements Deserializable {

  constructor(public title?: string,
              public description?: string,
              public button?: GeneralLink,
              public textAlign?: HorizontalAlignmentType) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): DataSection {
    return new DataSection().deserialize(this);
  }
}
