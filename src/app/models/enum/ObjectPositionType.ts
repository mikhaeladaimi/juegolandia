export enum ObjectPositionType {
  LEFT = 'left', CENTER = 'center', RIGHT = 'right', TOP = 'top', DOWN = 'down', NONE = 'none'
}
