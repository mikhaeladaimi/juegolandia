export enum SeverityType {
  FATAL = "FATAL",
  NONE = "NONE",
  INFO = "INFO"
}
