export enum VerticalAlignmentType {
  TOP = "top", CENTER = "center", BOTTOM = "bottom", NONE = "none"
}
