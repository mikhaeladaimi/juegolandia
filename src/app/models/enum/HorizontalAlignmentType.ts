export enum HorizontalAlignmentType {
  LEFT = "left", CENTER = "center", RIGHT = "right", NONE = "none"
}
