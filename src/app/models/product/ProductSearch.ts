import {Deserializable} from "../Deserializable";
import {Pageable} from "../pagination/Pageable";
import {AttributeValue} from "../attribute/AttributeValue";

export class ProductSearch extends Pageable implements Deserializable {

  attributes: AttributeValue[] = [];
  categorySlugs: string[] = [];
  minPrice: number = 0;
  pageSize: number = 9;
  currentPage: number = 0;

  constructor(attributes?: AttributeValue[],
              categorySlugs?: string[],
              public name?: string,
              public date?: string,
              public maxPrice?: number,
              minPrice?: number,
              public ordination?: string,
              pageSize?: number,
              currentPage?: number) {
    super();
    this.attributes = attributes;
    this.categorySlugs = categorySlugs;
    this.minPrice = minPrice;
    this.pageSize = pageSize;
    this.currentPage = currentPage;
  }

  public deserialize(input) {
    Object.assign(this, input);
    this.currentPage = input.pageNumber + 1;
    return this;
  }

  clone(): ProductSearch {
    return new ProductSearch().deserialize(this);
  }
}
