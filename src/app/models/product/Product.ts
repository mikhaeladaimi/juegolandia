import {Deserializable} from "../Deserializable";

export class Product implements Deserializable {

  constructor(
              public id?: number,
              public images?: String[],
              public category?: any,
              public shortDescription?: string,
              public longDescription?: string,
              public name?: string,
              public price?: number,
              public stock?: boolean,
              public featured?: boolean,
              public additionalInformation?: string,
              public slug?: string,
              public quantity?: number) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Product {
    return new Product().deserialize(this);
  }
}
