import {Deserializable} from "../Deserializable";
import {Pageable} from "../pagination/Pageable";
import {SortListRequest} from "../sort/SortListRequest";
import {AttributeValueRequest} from "../attribute/AttributeValueRequest";

export class ProductSearchRequest extends Pageable implements Deserializable {

  attributes: AttributeValueRequest[] = [];
  categorySlugs: string[] = [];
  minPrice: number = 0;
  pageSize: number = 9;
  currentPage: number = 0;

  constructor(attributes?: AttributeValueRequest[],
              categorySlugs?: string[],
              minPrice?: number,
              pageSize?: number,
              currentPage?: number,
              public categoryIds?: number[],
              public categoryName?: string,
              public offset?: number,
              public name?: string,
              public maxPrice?: number,
              public ordination?: string,
              public featured?: boolean,
              public pageNumber?: number,
              public paged?: boolean,
              public stock?: boolean,
              public unpaged?: boolean,
              public excludedIds?: number[],
              public id?: number,
              public price?: number,
              public refference?: string,
              public sortRequest?: SortListRequest) {
    super();
    this.attributes = attributes;
    this.categorySlugs = categorySlugs;
    this.minPrice = minPrice;
    this.pageSize = pageSize;
    this.currentPage = currentPage;
  }

  public deserialize(input) {
    this.attributes = input.attributes;
    this.categorySlugs = input.categorySlugs;
    this.minPrice = input.minPrice;
    this.pageSize = input.pageSize;
    this.categoryIds = input.categoryIds;
    this.categoryName = input.categoryName;
    this.offset = input.offset;
    this.name = input.name;
    this.maxPrice = input.maxPrice;
    this.ordination = input.ordination;
    this.featured = input.featured;
    this.pageNumber = input.pageNumber;
    this.paged = input.paged;
    this.stock = input.stock;
    this.unpaged = input.unpaged;
    this.excludedIds = input.excludedIds;
    this.id = input.id;
    this.price = input.price;
    this.refference = input.refference;
    this.sortRequest = input.sortRequest;
    this.currentPage = input.currentPage;
    return this;
  }
}
