import {Product} from "./Product";
import {Deserializable} from "../Deserializable";

/**
 * Product featured
 */
export class FeaturedProductList implements Deserializable {

  constructor(public title?: string,
              public productList?: Product[],
              public order?: number) {
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }

  clone(): FeaturedProductList {
    return new FeaturedProductList().deserialize(this);
  }
}
