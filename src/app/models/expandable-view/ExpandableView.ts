import {Deserializable} from "../Deserializable";

export class ExpandableView<T> implements Deserializable {

  constructor(public object?: T,
              public panelOpenState?: boolean) {
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }

  clone(): ExpandableView<T> {
    return new ExpandableView<T>().deserialize(this);
  }
}
