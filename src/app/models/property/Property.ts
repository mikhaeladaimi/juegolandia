import {Deserializable} from "../Deserializable";

export class Property<T> implements Deserializable {

  constructor(public id?: String,
              public name?: String,
              public imageUrl?,
              public originalItem?: T) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Property<T> {
    return new Property<T>().deserialize(this);
  }
}

