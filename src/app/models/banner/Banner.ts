import {InfoObject} from "../info-object/InfoObject";
import {Deserializable} from "../Deserializable";

export class Banner implements Deserializable {

  constructor(public objectList?: InfoObject[],
              public background?: any) {
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }

  clone(): Banner {
    return new Banner().deserialize(this);
  }
}
