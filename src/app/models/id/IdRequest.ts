import {Deserializable} from "../Deserializable";

export class IdRequest implements Deserializable {

  constructor(public id?: number) {
  }

  deserialize(input: any): this {
    this.id = input.id;
    return this;
  }
}
