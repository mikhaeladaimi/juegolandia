import {GeneralLink} from "../link/GeneralLink";
import {Deserializable} from "../Deserializable";

export class InfoObject implements Deserializable {

  constructor(public title?: string,
              public subtitle?: string,
              public description?: string,
              public button?: GeneralLink,
              public link?: GeneralLink,
              public image?: string,
              public icon?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    if (input.imageUrl)
      this.image = input.imageUrl;
    if (input.name)
      this.title = input.name;
    if (input.button)
      this.button = new GeneralLink().deserialize(input.button);
    if (input.link)
      this.link = new GeneralLink().deserialize(input.link);
    return this;
  }

  clone(): InfoObject {
    return new InfoObject().deserialize(this);
  }
}
