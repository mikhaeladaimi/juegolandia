import {Video} from "../video/Video";
import {Deserializable} from "../Deserializable";
import {HorizontalAlignmentType} from "../enum/HorizontalAlignmentType";

export class Media implements Deserializable {

  constructor(public image?: any,
              public video?: Video,
              public align?: HorizontalAlignmentType) {
  }

  deserialize(input) {
    Object.assign(this, input);
    if (input.video)
      this.video = new Video().deserialize(input.video);
    return this;
  }

  clone(): Media {
    return new Media().deserialize(this);
  }
}
