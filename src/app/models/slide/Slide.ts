import {Deserializable} from "../Deserializable";
import {GeneralLink} from "../link/GeneralLink";
import {VerticalAlignmentType} from "../enum/VerticalAlignmentType";
import {HorizontalAlignmentType} from "../enum/HorizontalAlignmentType";
import {ImageApp} from "../image/ImageApp";

export class Slide implements Deserializable {

  background: any;
  backgroundColor: string;
  title: string;
  subtitle: string;
  description: string;
  textHAlign: HorizontalAlignmentType;
  textVAlign: VerticalAlignmentType;
  buttons: GeneralLink[];

  constructor() {
  }

  deserialize(input) {
    Object.assign(this, input);
    if (input.textHAlign)
      this.textHAlign = input.textHAlign.toString().toLowerCase();
    else
      this.textHAlign = HorizontalAlignmentType.LEFT;
    if (input.textVAlign)
      this.textVAlign = input.textVAlign.toString().toLowerCase();
    else
      this.textVAlign = VerticalAlignmentType.TOP;
    if (input.buttonTitle) {
      this.buttons = [new GeneralLink().deserialize({
        title: input.buttonTitle,
        link: input.buttonLink,
        target: input.buttonTarget
      })];
    }

    if (input.image) this.background = new ImageApp().deserialize(input.image);
    this.backgroundColor = input.backgroundColor;
    return this;
  }
}
