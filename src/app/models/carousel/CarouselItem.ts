import {Deserializable} from "../Deserializable";
import {ImageApp} from "../image/ImageApp";
import {Category} from "../category/Category";

export class CarouselItem implements Deserializable {

  constructor(public title?: string,
              public image?: ImageApp,
              public price?: number,
              public isNew?: boolean,
              public categories?: Category[]) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): CarouselItem {
    return new CarouselItem().deserialize(this);
  }
}
