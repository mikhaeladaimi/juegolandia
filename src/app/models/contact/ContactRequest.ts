import {Deserializable} from "../Deserializable";

export class ContactRequest implements Deserializable {

  constructor(public id?: number,
              public name?: string,
              public email?: string,
              public phone?: string,
              public subject?: string,
              public text?: string,
              public privacy?: boolean) {
  }

  deserialize(input) {
    this.id = input.id;
    this.name = input.name;
    this.email = input.email;
    this.phone = input.phone;
    this.subject = input.subject;
    this.text = input.text;
    this.privacy = input.privacy;
    return this;
  }

}
