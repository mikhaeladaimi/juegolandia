import {Deserializable} from "../Deserializable";

export class Contact implements Deserializable {

  constructor(public id?: number,
              public name?: string,
              public email?: string,
              public phone?: string,
              public subject?: string,
              public text?: string,
              public privacy?: boolean) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Contact {
    return new Contact().deserialize(this);
  }
}
