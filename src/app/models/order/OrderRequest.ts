import {Deserializable} from "../Deserializable";
import {OrderItemRequest} from "./OrderItemRequest";
import {AddressRequest} from "../address/AddressRequest";

export class OrderRequest implements Deserializable {
  constructor(public billingAddress?: AddressRequest,
              public couponCode?: string,
              public deliveryAddress?: AddressRequest,
              public email?: string,
              public name?: string,
              public observations?: string,
              public paymentType?: string,
              public phone?: string,
              public preorderId?: number,
              public products?: OrderItemRequest[],
              public stripeToken?: string) {
  }

  deserialize(input: any): this {
    this.billingAddress = new AddressRequest().deserialize(input.billingAddress);
    this.couponCode = input.couponCode;
    this.deliveryAddress = new AddressRequest().deserialize(input.deliveryAddress);
    this.email = input.email;
    this.name = input.name;
    this.observations = input.observations;
    this.paymentType = input.paymentType;
    this.phone = input.phone;
    this.preorderId = input.preorderId;
    if (input.products)
      this.products = input.products.map(product => new OrderItemRequest().deserialize(product));
    this.stripeToken = input.stripeToken;
    return this;
  }

}
