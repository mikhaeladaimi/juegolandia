import {Deserializable} from "../Deserializable";

export class OrderItemRequest implements Deserializable {

  constructor(public attributesIds?: number[],
              public position?: number,
              public productId?: number,
              public quantity?: number) {
  }

  deserialize(input: any): this {
    this.attributesIds = input.attributesIds;
    this.position = input.position;
    this.productId = input.productId;
    this.quantity = input.quantity;
    return this;
  }
}
