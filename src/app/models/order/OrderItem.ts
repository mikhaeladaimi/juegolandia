import {Product} from "../product/Product";
import {Deserializable} from "../Deserializable";

export class OrderItem implements Deserializable {

  constructor(public product?: Product,
              public quantity?: number,
              public position?: number) {
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    this.product = new Product().deserialize(input.product);
    return this;
  }

  clone(): OrderItem {
    return new OrderItem().deserialize(this);
  }
}
