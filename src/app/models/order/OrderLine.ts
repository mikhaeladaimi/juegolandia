import {Deserializable} from "../Deserializable";

export class OrderLine implements Deserializable {

  productId: number;
  description: string;
  imageUrl: string;
  name: string;
  price: number;
  quantity: number;
  unitPrice: number;
  images: any[];

  constructor() {
  }

  deserialize(input: any): this {
    if (!input.product) {
      Object.assign(this, input);
    } else {
      this.name = input.product.name;
      this.unitPrice = input.product.price;
      if (input.product.images && input.product.images.length > 0) {
          this.imageUrl = input.product.images[0];
      }
      this.quantity = (input.quantity) ? input.quantity : 1;
      this.price = this.unitPrice * this.quantity;
    }
    return this;
  }
}
