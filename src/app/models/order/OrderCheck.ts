import {Deserializable} from "../Deserializable";

export class OrderCheck implements Deserializable {

  constructor(public couponCode?,
              public discount?,
              public deliveryPrice?,
              public id?,
              public ivaPrice?,
              public productsPrice?,
              public totalPrice?) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): OrderCheck {
    return new OrderCheck().deserialize(this);
  }
}
