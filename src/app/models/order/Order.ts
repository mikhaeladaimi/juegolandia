import {Deserializable} from "../Deserializable";
import {OrderLine} from "./OrderLine";
import {PriceLine} from "../price/PriceLine";
import {Address} from "../address/Address";
import {Product} from "../product/Product";
import {OrderStatusType} from "../enum/OrderStatusType";

export class Order implements Deserializable {

  constructor(public id?: number,
              public totalPrice?: number,
              public orderStatus?: OrderStatusType,
              public isCreditCard?: boolean,
              public email?: string,
              public observations?: string,
              public name?: string,
              public billingAddress?: Address,
              public deliveryAddress?: Address,
              public deliveryMethod?: string,
              public creationDate?: Date | string,
              public priceLines?: PriceLine[],
              public orderLines?: OrderLine[],
              public phone?: number | string,
              public preorderId?: number,
              public products?: Product,
              public stripeToken?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    this.creationDate = new Date(Date.parse(input.creationDate));
    this.billingAddress = new Address().deserialize(input.billingAddress);
    this.deliveryAddress = new Address().deserialize(input.deliveryAddress);
    if (input.priceLines)
      this.priceLines = input.priceLines.map(item => new PriceLine().deserialize(item));
    if (input.orderLines)
      this.orderLines = input.orderLines.map(item => new OrderLine().deserialize(item));
    if (input.orderItems)
      this.products = input.orderItems.map(item => new Product().deserialize(item));
    else if (input.products)
      this.products = input.products.map(item => new Product().deserialize({product: item}));
    return this;
  }

  clone(): Order {
    return new Order().deserialize(this);
  }
}
