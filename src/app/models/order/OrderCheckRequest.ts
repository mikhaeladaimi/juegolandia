import {Deserializable} from "../Deserializable";
import {OrderItemRequest} from "./OrderItemRequest";
import {AddressRequest} from "../address/AddressRequest";

export class OrderCheckRequest implements Deserializable {

  constructor(public couponCode?: string,
              public deliveryAddress?: AddressRequest,
              public products?: OrderItemRequest[]) {
  }

  public deserialize(input) {
    this.couponCode = input.couponCode;
    this.deliveryAddress = input.deliveryAddress;
    this.products = input.products;
    return this;
  }
}
