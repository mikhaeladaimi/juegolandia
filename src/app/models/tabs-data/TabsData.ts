import {Deserializable} from "../Deserializable";

/**
 * Tabs data
 */
export class TabsData implements Deserializable {

  hasOnClick: boolean = false;

  constructor(public title?: String,
              public content?: String,
              public emptyView?: String,
              hasOnClick?: boolean) {
    this.hasOnClick = hasOnClick;
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }

  clone(): TabsData {
    return new TabsData().deserialize(this);
  }
}
