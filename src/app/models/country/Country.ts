import {Deserializable} from "../Deserializable";

export class Country implements Deserializable {

  constructor(public id?: number,
              public name?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Country {
    return new Country().deserialize(this);
  }
}
