import {Deserializable} from "../Deserializable";

export class LimitPrice implements Deserializable {

  constructor(public min?: number,
              public max?: number) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): LimitPrice {
    return new LimitPrice().deserialize(this);
  }
}
