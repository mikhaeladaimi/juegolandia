import {Deserializable} from "../Deserializable";

export class PriceLine implements Deserializable {

  constructor(public price?: number,
              public concept?: string) {
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }

  clone(): PriceLine {
    return new PriceLine().deserialize(this);
  }
}

