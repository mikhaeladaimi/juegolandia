import {Deserializable} from "../Deserializable";

export class DeliveryPrice implements Deserializable {

  price = 500;
  freeDeliveryThreshold = 10000;

  constructor() {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): DeliveryPrice {
    return new DeliveryPrice().deserialize(this);
  }
}
