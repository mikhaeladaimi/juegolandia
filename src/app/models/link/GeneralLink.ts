import {Deserializable} from "../Deserializable";

export class GeneralLink implements Deserializable {

  constructor(public icon?: string,
              public squareIcon?: string,
              public link?: string,
              public title?: string,
              public target?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): GeneralLink {
    return new GeneralLink().deserialize(this);
  }
}
