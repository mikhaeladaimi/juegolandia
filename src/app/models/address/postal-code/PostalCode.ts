import {Deserializable} from "../../Deserializable";

export class PostalCode implements Deserializable {

  constructor(public id?: number,
              public postalCode?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): PostalCode {
    return new PostalCode().deserialize(this);
  }
}
