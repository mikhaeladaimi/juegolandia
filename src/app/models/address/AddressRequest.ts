import {Deserializable} from "../Deserializable";

export class AddressRequest implements Deserializable {

  constructor(public address?: string,
              public addressDetail?: string,
              public postalCode?: number,
              public locality?: string,
              public province?: string,
              public country?: string,
              public name?: string,
              public surname?: string,
              public dni?: string,
              public company?: string,
              public phone?: string) {
  }

  deserialize(input) {
    this.address = input.address;
    this.addressDetail = input.addressDetail;
    this.postalCode = input.postalCode;
    this.locality = input.locality;
    this.province = input.province;
    this.country = input.country;
    this.name = input.name;
    this.surname = input.surname;
    this.dni = input.dni;
    this.company = input.company;
    this.phone = input.phone;
    return this;
  }
}


