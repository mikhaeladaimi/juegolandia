import {Deserializable} from "../../Deserializable";

export class Province implements Deserializable {

  constructor(public id?: number,
              public name?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Province {
    return new Province().deserialize(this);
  }
}
