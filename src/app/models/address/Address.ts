import {Deserializable} from "../Deserializable";

export class Address implements Deserializable {

  constructor(public country?: string,
              public address?: string,
              public postalCode?: number,
              public locality?: string,
              public province?: string,
              public dni?: string) {
    this.country = "España";
  }

  deserialize(input) {
    Object.assign(this, input);
    this.country = "España";
    return this;
  }

  clone(): Address {
    return new Address().deserialize(this);
  }
}
