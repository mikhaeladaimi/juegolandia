import {Deserializable} from "../../Deserializable";

export class Locality implements Deserializable {

  constructor(public id?: number,
              public name?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Locality {
    return new Locality().deserialize(this);
  }
}
