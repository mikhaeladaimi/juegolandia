import {Deserializable} from "../Deserializable";

export class FileApp implements Deserializable {

  constructor(public id?: number,
              public displayName?: string,
              public fileName?: string,
              public filePath?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): FileApp {
    return new FileApp().deserialize(this);
  }
}
