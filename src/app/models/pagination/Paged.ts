import {Deserializable} from "../Deserializable";

export class Paged<T extends Deserializable> implements Deserializable {

  constructor(public content?: T[],
              public pageNumber?: number,
              public offset?: number,
              public size?: number,
              public totalElements?: number) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Paged<T> {
    return new Paged<T>().deserialize(this);
  }
}
