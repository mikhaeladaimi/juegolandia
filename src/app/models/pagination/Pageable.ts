import {Deserializable} from "../Deserializable";

export class Pageable implements Deserializable {

  constructor(public offset?: number,
              public pageNumber?: number,
              public pageSize?: number) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Pageable {
    return new Pageable().deserialize(this);
  }
}
