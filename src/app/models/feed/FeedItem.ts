import {Deserializable} from "../Deserializable";
import {ImageApp} from "../image/ImageApp";

export class FeedItem implements Deserializable {

  constructor(public title?: string,
              public link?: string,
              public pubDate?,
              public description?: string,
              public content?,
              public image?: ImageApp) {
  }

  deserialize(input) {
    if (input.title && input.title.rendered) this.title = input.title.rendered;
    this.link = input.link;
    this.pubDate = input.date;
    if (input.excerpt && input.excerpt.rendered) this.description = input.excerpt.rendered.replace(/<[^>]*>/g, '');
    if (input.content && input.content.rendered) this.content = input.content.rendered.replace(/<[^>]*>/g, '');

    const embedded = input['_embedded'];
    if (embedded) {
      this.image = new ImageApp().deserialize(FeedItem.saveEmbeddedValue(embedded, "wp:featuredmedia"));
    }
    return this;
  }

  static saveEmbeddedValue(embedded, value) {
    return (embedded[value] && embedded[value].length > 0) ? embedded[value][0] : {};
  }

  clone(): FeedItem {
    return new FeedItem().deserialize(this);
  }
}
