import {Deserializable} from "../Deserializable";
import {FeedItem} from "./FeedItem";

export class Feed implements Deserializable {

  maxItems: number;
  maxPage: number;
  posts: FeedItem[] = [];

  constructor() {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Feed {
    return new Feed().deserialize(this);
  }
}
