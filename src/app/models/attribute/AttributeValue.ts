import {Deserializable} from "../Deserializable";

export class AttributeValue implements Deserializable {

  constructor(public id?: number,
              public name?: string,
              public value?: string,
              public attributeId?: number,
              public enabled?: boolean,
              public isSelected?: boolean) {
  }

  public deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): AttributeValue {
    return new AttributeValue().deserialize(this);
  }
}
