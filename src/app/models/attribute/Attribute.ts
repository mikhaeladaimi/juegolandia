import {AttributeValue} from "./AttributeValue";
import {Deserializable} from "../Deserializable";

export class Attribute implements Deserializable {

  constructor(public id?: number,
              public name?: string,
              public values?: AttributeValue[]) {
  }

  public deserialize(input) {
    Object.assign(this, input);
    this.mapAttributes(input);
    return this;
  }

  clone(): Attribute {
    return new Attribute().deserialize(this);
  }

  private mapAttributes(input) {
    if (input.values)  this.values = input.values.map(attribute => new AttributeValue().deserialize(attribute));
    this.values.forEach(
      value => {
      if (!value.attributeId) value.attributeId = this.id;
    });
  }
}
