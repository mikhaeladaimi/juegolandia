import {Deserializable} from "../Deserializable";
import {AttributesUtils} from "../../shared/utils/attributes.utils";

export class AttributeRequest implements Deserializable {

  constructor(public attributes?: number[],
              public id?: number) {
  }

  deserialize(input: any): this {
    this.attributes = AttributesUtils.transformToAttributeIds(input.attributes);
    this.id = input.id;
    return this;
  }


}
