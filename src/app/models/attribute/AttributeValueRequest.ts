import {Deserializable} from "../Deserializable";

export class AttributeValueRequest implements Deserializable {

  constructor(public id?: number,
              public value?: string) {
  }

  deserialize(input) {
    this.id = input.id;
    this.value = input.value;
    return this;
  }
}
