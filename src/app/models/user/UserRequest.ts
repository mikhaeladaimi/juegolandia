import {Deserializable} from "../Deserializable";

export class UserRequest implements Deserializable {

  constructor(public name?: string) {
  }

  deserialize(input: any): this {
    this.name = input.name;
    return this;
  }
}
