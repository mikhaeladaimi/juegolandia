import {Deserializable} from "../Deserializable";
import {Address} from "../address/Address";

export class User implements Deserializable {

  constructor(public id?: string,
              public email?: string,
              public name?: string,
              public address?: Address,
              public phone?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    if (!this.address) this.address = new Address();
    return this;
  }

  clone(): User {
    return new User().deserialize(this);
  }
}
