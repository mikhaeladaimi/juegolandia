import {Deserializable} from "../Deserializable";

export class ImageApp implements Deserializable {

  constructor(public id?,
              public signature?: string,
              public url?: string,
              public alt?: string,
              public thumbnailUrl?: string) {
  }

  deserialize(input) {
    if (input && input.source_url) this.url = input.source_url;
    if (input && input.alt_text) this.alt = input.alt_text;
    Object.assign(this, input);
    return this;
  }

  clone(): ImageApp {
    return new ImageApp().deserialize(this);
  }
}
