import {Deserializable} from "../Deserializable";

export class Question implements Deserializable {

  constructor(public id?: number,
              public question?: String,
              public answer?: String) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Question {
    return new Question().deserialize(this);
  }
}
