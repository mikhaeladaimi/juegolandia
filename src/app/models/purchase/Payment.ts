import {Deserializable} from "../Deserializable";

export class Payment implements Deserializable {

  constructor(public operationNumber?: string,
              public totalPrice?: number) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Payment {
    return new Payment().deserialize(this);
  }
}
