import {Deserializable} from "../Deserializable";

export class PurchaseRequest implements Deserializable {

  constructor(public id?: number,
              public returnUrl?: string) {
  }

  deserialize(input) {
    this.id = input.id;
    this.returnUrl = input.returnUrl;
    return this;
  }
}
