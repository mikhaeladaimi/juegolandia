import {Deserializable} from "../Deserializable";
import {ImageApp} from "../image/ImageApp";

export class Category implements Deserializable {

  constructor(public id?: number,
              public name?: string,
              public slug?: string,
              public description?: string,
              public imageUrl?: ImageApp,
              public isSelected?: boolean) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): Category {
    return new Category().deserialize(this);
  }
}
