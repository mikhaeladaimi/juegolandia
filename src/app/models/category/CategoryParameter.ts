import {Deserializable} from "../Deserializable";

export class CategoryParameter implements Deserializable {

  constructor(public name?: string,
              public description?: string) {
  }

  deserialize(input) {
    Object.assign(this, input);
    return this;
  }

  clone(): CategoryParameter {
    return new CategoryParameter().deserialize(this);
  }
}
