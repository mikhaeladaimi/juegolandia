import {Component, OnDestroy, OnInit} from '@angular/core';

import {NavigationEnd, Router} from '@angular/router';
import {Subscription} from "rxjs/Rx";
import {NgcCookieConsentService, NgcInitializeEvent, NgcNoCookieLawEvent, NgcStatusChangeEvent} from "ngx-cookieconsent";
import {ScrollHelper} from "./shared/helpers/scroll.helper";
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/pairwise';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private popupOpenSubscription: Subscription;
  private popupCloseSubscription: Subscription;
  private initializeSubscription: Subscription;
  private statusChangeSubscription: Subscription;
  private revokeChoiceSubscription: Subscription;
  private noCookieLawSubscription: Subscription;

  constructor(private router: Router, private cookieConsentService: NgcCookieConsentService, private scrollHelper: ScrollHelper
  ) {
  }

  ngOnInit() {
    this.router.events.filter(e => e instanceof NavigationEnd)
      .pairwise()
      .subscribe((e: any[]) => {
        if (!AppComponent.getRouteWithoutQueryParams(e[1].url).includes('legal') &&
          (AppComponent.getRouteWithoutQueryParams(e[0].url) != AppComponent.getRouteWithoutQueryParams(e[1].url)))
          this.scrollHelper.scroll(0, 0);
      });

    this.popupOpenSubscription = this.cookieConsentService.popupOpen$.subscribe(
      () => {
      });

    this.popupCloseSubscription = this.cookieConsentService.popupClose$.subscribe(
      () => {
      });

    this.initializeSubscription = this.cookieConsentService.initialize$.subscribe(
      (event: NgcInitializeEvent) => {
      });

    this.statusChangeSubscription = this.cookieConsentService.statusChange$.subscribe(
      (event: NgcStatusChangeEvent) => {
      });

    this.revokeChoiceSubscription = this.cookieConsentService.revokeChoice$.subscribe(
      () => {
      });

    this.noCookieLawSubscription = this.cookieConsentService.noCookieLaw$.subscribe(
      (event: NgcNoCookieLawEvent) => {
      });

  }

  ngOnDestroy() {
    this.popupOpenSubscription.unsubscribe();
    this.popupCloseSubscription.unsubscribe();
    this.initializeSubscription.unsubscribe();
    this.statusChangeSubscription.unsubscribe();
    this.revokeChoiceSubscription.unsubscribe();
    this.noCookieLawSubscription.unsubscribe();
  }

  private static getRouteWithoutQueryParams(url: string): string {
    const fragment = url.split("?");
    return fragment[0];
  }
}
