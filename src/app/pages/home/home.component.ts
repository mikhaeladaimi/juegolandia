import {Component, OnInit} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {InfoObject} from "../../models/info-object/InfoObject";
import {InfoObjectsService} from "../../shared/view/info.objects.service";
import {Grid} from "../../models/grid/Grid";
import {GridService} from "../../shared/view/grid.service";
import {Slide} from "../../models/slide/Slide";
import {Question} from "../../models/question/Question";
import {QuestionsService} from "../../shared/services/questions.service";
import {THEME_CONFIG} from "../../@theme/theme-config";
import {GeneralLink} from "../../models/link/GeneralLink";
import {ImageUtils} from "../../shared/utils/image.utils";
import {SliderService} from "../../shared/view/slider.service";
import {DummyUtils} from "../../shared/utils/dummy.utils";
import {HorizontalAlignmentType} from "../../models/enum/HorizontalAlignmentType";
import {ObjectPositionType} from "../../models/enum/ObjectPositionType";

@Component({
  selector: 'app-main',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  slideObjects: Slide[] = [];
  questions: Question[] = [];
  height: string = "60vh";
  infoObjects: InfoObject[] = [];
  objectPosition = ObjectPositionType;
  horizontalAlignment = HorizontalAlignmentType;
  gridList: Grid[];
  displayPrice: boolean;
  items = [];
  button: GeneralLink;

  bannerButton = DummyUtils.randomBannerButton();
  bannerInfo = DummyUtils.randomBannerInfo(this.bannerButton);
  bannerTextAlignment = ObjectPositionType.CENTER;
  default = ImageUtils.DEFAULT_PRODUCT_IMAGE;

  constructor(private logger: NGXLogger, private gridService: GridService,
              private questionService: QuestionsService, private sliderService: SliderService, private infoObjectService: InfoObjectsService) {
    this.getQuestions();
    this.getSlides();
    this.getInfoObjects();
    this.getGridList();
  }

  ngOnInit(): void {
    this.displayPrice = THEME_CONFIG.shopConfig.displayPrice;
    this.items = Array.from(Array(5).keys()).map(() => DummyUtils.randomCarouselItems());
    this.button = DummyUtils.randomButton();

  }

  private getGridList() {
    this.gridService.getGridList()
      .then(response => this.gridList = response)
      .catch(error => this.logger.debug("Se ha producido un error", error));
  }

  private getQuestions() {
    this.questionService.getQuestions()
      .then(response => this.questions = response)
      .catch(error => this.logger.debug("Se ha producido un error", error));
  }

  private getSlides() {
    this.sliderService.getSlideObjectsDummy()
      .then(response => this.slideObjects = response)
      .catch(error => this.logger.debug("Se ha producido un error", error));
  }

  private getInfoObjects() {
    this.infoObjectService.getInfoObjects()
      .then(response => this.infoObjects = response)
      .catch(error => this.logger.debug("Se ha producido un error", error));
  }

}
