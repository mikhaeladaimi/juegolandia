import {NgModule} from '@angular/core';

import {ThemeModule} from '../../@theme/theme.module';
import {GridListModule} from "../../components/grid-list/grid-list.module";
import {FaqsModule} from "../../components/faqs/faqs.module";
import {SlideModule} from "../../components/slide/slide.module";
import {CarouselListModule} from "../../components/carousel-list/carousel-list.module";
import {CallToActionModule} from "../../components/call-to-action/call-to-action.module";
import {InfoGridModule} from "../../components/info-grid/info-grid.module";
import {HomeRoutingModule, routedComponents} from './home.routing.module';
import {BlogModule} from "../../components/blog/blog.module";

@NgModule({
  imports: [
    ThemeModule,
    FaqsModule,
    SlideModule,
    CarouselListModule,
    SlideModule,
    CallToActionModule,
    GridListModule,
    InfoGridModule,
    HomeRoutingModule,
    BlogModule
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class HomeModule {
}

