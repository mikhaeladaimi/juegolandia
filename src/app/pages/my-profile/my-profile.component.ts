import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalUtils} from "../../shared/utils/modal.utils";
import {User} from "../../models/user/User";
import {UserOrdersService} from "../../shared/services/user-orders.service";
import {NGXLogger} from "ngx-logger";
import {Order} from "../../models/order/Order";
import {UserService} from "../../shared/services/user.service";
import {AuthService} from "../../shared/services/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SyncUtils} from "../../shared/utils/sync-utils.service";
import {MyProfileTabsType} from "../../models/enum/MyProfileTabsType";
import {SlugsUtils} from "../../shared/utils/slugs.utils";

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit, OnDestroy {

  @ViewChild("access") access;
  loading: boolean = true;
  user: User;
  credentials: any = {};
  orderList: Order[];
  userSubscription;
  myProfileTabs = MyProfileTabsType;
  selectedTab;

  constructor(private modalUtils: ModalUtils, private ordersService: UserOrdersService, private logger: NGXLogger,
              private userService: UserService, private authService: AuthService, private router: Router,
              private route: ActivatedRoute) {
    this.route.queryParams.subscribe(queryParams => {
      this.selectedTab = (queryParams && queryParams.tab) ? queryParams.tab : this.myProfileTabs.MY_DATA;
    });
  }

  ngOnInit(): void {
    if (!this.authService.isUserLogged()) {
      this.modalUtils.openErrorModal("No hemos conseguido recuperar tus datos.")
        .then(() => this.router.navigate([SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME)]))
        .catch(() => this.router.navigate([SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME)]));
    }

    if (!this.userSubscription)
      this.userSubscription = this.userService.getObservableUser().subscribe(
        () => {
          this.getData();
        },
        error => {
          this.logger.debug("Error en userObservable", error);
          this.modalUtils.openErrorModal("No hemos conseguido recuperar tus datos.")
            .then(() => this.router.navigate([SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME)]))
            .catch(() => this.router.navigate([SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME)]));
        },
        () => this.getData()
      );

  }

  ngOnDestroy(): void {
    SyncUtils.unsubscribe(this.userSubscription);
  }

  saveUserData(userData) {
    this.modalUtils.openCancelableModal("¿Estás seguro?", "Vas a modificar tus datos.", "Guardar", "Cancelar")
      .then(() => {
        this.loading = true;
        this.userService.updateUser(userData, this.authService.getFirebaseUid())
          .then(response => {
            this.user = new User().deserialize(userData);
            this.userService.updateUserOnCache(this.user);
            this.modalUtils.openModal("¡Logrado!", "Los cambios han sido guardados", "Vale");
          })
          .catch(error => this.onError("No se pudo actualizar la información del usuario", error))
          .then(() => this.loading = false);
      })
      .catch(() => {
      });
  }

  getData() {
    this.userService.getUser(this.authService.getFirebaseUid())
      .then(user => {
        this.user = new User().deserialize(user);
        this.loading = false;
        this.ordersService.getUserOrders()
          .then(response => this.orderList = response)
          .catch(error => this.onError("Ocurrió un error al intentar recuperar los pedidos del usuario.", error));
      });
  }

  saveUserPassword() {
    this.modalUtils.openCancelableModal("¿Estás seguro?", "Esta acción cambiará tu contraseña.")
      .then(() => {
        this.loading = true;
        this.authService.changePassword(this.user.email, this.credentials.oldPassword, this.credentials.newPassword)
          .then(() => {
            this.modalUtils.openModal("¡Logrado!", "La contraseña ha sido modificada");
            this.credentials = {};
          })
          .catch(error => this.onError("No se pudo actualizar la contraseña del usuario", error))
          .then(() => this.loading = false);
      })
      .catch(() => {
      });
  }

  logOutModal() {
    this.modalUtils.openCancelableModal("Cerrar sesión", "¿Estás seguro de que deseas cerrar sesión?")
      .then(() => {
        this.logOut();
      })
      .catch(() => this.logger.debug("No se cerrará sesión"));
  }

  private logOut() {
    this.loading = true;
    this.authService.logout()
      .then(() => {
        this.loading = false;
        this.router.navigate([SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME)]);
      })
      .catch(error => this.onError("No se pudo cerrar la sesión", error));
  }

  private onError(body, error?) {
    this.logger.debug(error);
    this.modalUtils.openErrorModal(body);
  }
}
