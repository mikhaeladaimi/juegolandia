import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-user-access',
  templateUrl: './user-access.component.html',
  styleUrls: ['./user-access.component.scss'],
})
export class UserAccessComponent {

  @ViewChild("form") form;
  @Input() email: string;
  @Input() credentials: any;
  @Output() onSaveUserPassword: EventEmitter<any> = new EventEmitter();
  submitted: boolean;

  constructor() {
  }

  saveUserPassword() {
    this.submitted = true;
    if (this.form.valid) {
      this.onSaveUserPassword.emit({oldPassword: this.credentials.oldPassword, newPassword: this.credentials.newPassword});
    }
  }
}
