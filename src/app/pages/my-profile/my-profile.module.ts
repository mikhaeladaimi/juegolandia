import {LOCALE_ID, NgModule} from '@angular/core';
import localeEs from '@angular/common/locales/es';

import {MyProfileComponent} from './my-profile.component';
import {ThemeModule} from '../../@theme/theme.module';
import {UserDataComponent} from "./user-data/user-data.component";
import {UserOrdersComponent} from "./user-orders/user-orders.component";
import {registerLocaleData} from "@angular/common";
import {UserAccessComponent} from "./user-access/user-access.component";
import {MyProfileRoutingModule} from "./my-profile.routing.module";
import {MatExpansionModule} from "@angular/material";

registerLocaleData(localeEs);

@NgModule({
  imports: [ThemeModule, MyProfileRoutingModule, MatExpansionModule],

  declarations: [
    MyProfileComponent, UserDataComponent, UserOrdersComponent, UserAccessComponent
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "es-ES" }
  ]
})
export class MyProfileModule {
}

