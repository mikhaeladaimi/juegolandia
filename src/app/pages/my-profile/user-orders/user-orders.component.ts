import {Component, Input} from '@angular/core';
import {Order} from "../../../models/order/Order";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-user-orders',
  templateUrl: './user-orders.component.html',
  styleUrls: ['./user-orders.component.scss'],
})
export class UserOrdersComponent {

  @Input() orderList: Order[];
  products: number = 0;

  // region routes
  orderRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.ORDER);
  shopRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.SHOP);

  // endregion

  constructor() {
  }

}
