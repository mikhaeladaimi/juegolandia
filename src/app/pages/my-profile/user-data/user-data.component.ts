import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {User} from "../../../models/user/User";

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss'],
})
export class UserDataComponent {

  loading: boolean = false;
  @ViewChild("form") form;
  @Input() user: User;
  @Output() onSaveUserData: EventEmitter<User> = new EventEmitter();
  @Output() onDeleteUser: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  saveUserData() {
    if (this.form.valid) {
      this.onSaveUserData.emit(this.user);
    }
  }

  deleteUser() {
    this.onDeleteUser.emit();
  }
}
