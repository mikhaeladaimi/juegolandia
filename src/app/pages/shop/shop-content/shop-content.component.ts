import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Product} from "../../../models/product/Product";
import {Paged} from "../../../models/pagination/Paged";
import {THEME_CONFIG} from '../../../@theme/theme-config';
import {CartCacheService} from "../../../shared/cache/cart-cache.service";
import {ModalUtils} from "../../../shared/utils/modal.utils";

@Component({
  selector: 'app-shop-content',
  templateUrl: './shop-content.component.html',
  styleUrls: ['./shop-content.component.scss']
})
export class ShopContentComponent {

  @Input() loading = true;
  @Input() ordinationValues: string[];
  @Input() paged: Paged<Product> = new Paged<Product>();

  @Output() onSortingOptionSelected: EventEmitter<any> = new EventEmitter();
  @Output() onNewProductsNeeded: EventEmitter<any> = new EventEmitter();

  shopConfig = THEME_CONFIG.shopConfig;

  constructor(private cartUtils: CartCacheService, private modalUtils: ModalUtils) {
  }

  addProductToCart(slug: string) {
    const product = this.paged.content.filter(product => product.slug == slug)[0];
    this.cartUtils.addProductToCart(product, 1);
    this.modalUtils.openModal("Catálogo - Carrito", "El producto se añadió correctamente al carrito.");
  }
}
