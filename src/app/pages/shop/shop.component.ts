import {Component, OnInit} from '@angular/core';
import {THEME_CONFIG} from "../../@theme/theme-config";
import {ProductsService} from "../../shared/services/products.service";
import {NGXLogger} from "ngx-logger";
import {Product} from "../../models/product/Product";
import {ActivatedRoute, Router} from "@angular/router";
import {Paged} from "../../models/pagination/Paged";
import {CategoriesService} from "../../shared/services/categories.service";
import {OrdinationService} from "../../shared/services/ordination.service";
import {PriceFilter} from "../../models/filter/PriceFilter";
import {ListFilter} from "../../models/filter/ListFilter";
import {Observable} from "rxjs/Rx";
import {FilterUtils} from "../../shared/utils/filter.utils";
import {Category} from "../../models/category/Category";
import {ProductSearchRequest} from "../../models/product/ProductSearchRequest";
import {ProductSearch} from "../../models/product/ProductSearch";
import {FeaturedProductList} from "../../models/product/FeaturedProductList";
import {HorizontalAlignmentType} from "../../models/enum/HorizontalAlignmentType";
import {SlugsUtils} from "../../shared/utils/slugs.utils";

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  loading = true;
  loaded = false;
  alignment = HorizontalAlignmentType;
  shopConfig = THEME_CONFIG.shopConfig;

  categorySelected: Category;
  productSearch = new ProductSearchRequest();
  ordinationValues: string[];

  paged: Paged<Product> = new Paged();

  // Sidebar
  priceFilter: PriceFilter;
  categoryFilter: ListFilter;
  attributesFilter: ListFilter;
  productFeatured: FeaturedProductList;

  constructor(private route: ActivatedRoute, private router: Router, private logger: NGXLogger,
              private productsService: ProductsService,
              private categoriesService: CategoriesService,
              private ordinationService: OrdinationService) {

    this.generateSidebarElements();

    const navigation = Observable
      .combineLatest(this.route.params, this.route.queryParams, (params, queryParams) => ({params, queryParams}));

    navigation.subscribe((paramsGroup) => {
      this.getFiltersFromParams(paramsGroup);
    });
  }

  ngOnInit(): void {
    this.getOrdinationValues();
    this.getPagedProducts();
  }

  sortProducts(ordinationValue: string) {
    this.productSearch.ordination = ordinationValue;
    this.filterProducts();
  }

  showProducts(size: number) {
    this.productSearch.pageSize = size;
    this.filterProducts();
  }

  onPageChange(paged) {
    paged.nextPageable.pageNumber = paged.pageNumber + 2;
    paged.previousPageable.pageNumber = paged.pageNumber;
    this.productSearch.deserialize(paged);
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {page: this.productSearch.currentPage},
        queryParamsHandling: "merge",
      });
    this.getPagedProducts();
  }

  clearFilters() {
    this.loaded = false;
    this.productSearch = new ProductSearchRequest();
    this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.SHOP));
    this.getPagedProducts();
  }

  searchProduct(name) {
    // TODO search
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {nombre: name},
        queryParamsHandling: "merge",
      });
    this.productSearch.name = name;
    this.filterProducts();
  }

  private getFiltersFromParams(paramsGroup) {
    if (paramsGroup) {
      if (paramsGroup.params)
        this.productSearch.categorySlugs = (paramsGroup.params.categories) ? FilterUtils.getParamsCategories(paramsGroup.params.categories) : [];
      if (paramsGroup.queryParams) {
        this.productSearch.name = paramsGroup.queryParams.nombre ? paramsGroup.queryParams.nombre : "";
        if (paramsGroup.queryParams.page <= 0 || isNaN(paramsGroup.queryParams.page)) {
          this.router.navigate([], {relativeTo: this.route, queryParams: {page: 1}, queryParamsHandling: "merge", replaceUrl: true});
        } else {
          this.productSearch.currentPage = (paramsGroup.queryParams.page) ? paramsGroup.queryParams.page : "";
          this.productSearch.offset = (paramsGroup.queryParams.page - 1) * this.productSearch.pageSize;
          this.productSearch.pageNumber = paramsGroup.queryParams.page - 1;
        }
      }
      this.filterProducts();
    }
  }


  private filterProducts() {
    this.getPagedProducts();
  }

  private getPagedProducts() {
    this.loading = true;
    this.productsService.getProducts(this.productSearch).then(products => {
      this.paged.content = products;
      this.saveCategoryData();
      this.loading = false;
    });
  }

  private generateSidebarElements() {
    Observable.forkJoin([
      this.getCategories(),
      this.getMaxProductPrice(),
      this.getFeaturedProducts()
    ])
      .subscribe(t => {
        if (!this.categoryFilter) this.saveCategoryData(new ListFilter("CATEGORIAS", t[0]));
        // this.priceFilter = <PriceFilter>t[2];
        this.productFeatured = new FeaturedProductList("Productos destacados", t[3].content, 4);
        this.loaded = true;
        this.loading = false;
      });
  }


  // Services calls
  private getFeaturedProducts() {
    return this.productsService.getFeaturedProducts(new ProductSearch().deserialize({
      offset: 0,
      pageNumber: 0,
      pageSize: 4,
    }));
  }

  private getMaxProductPrice() {
    return new Promise(solve => {
      if (!this.shopConfig.sidebarConfig.displayPriceFilter) solve(null);
      // TODO
      // this.productsService.getMinMaxPrices().then(minMaxPrice => {
      //   solve(new PriceFilter(0, minMaxPrice.max / 100, null, 1));
      // });
    });
  }

  private getCategories(): Promise<any> {
    if (!this.shopConfig.sidebarConfig.displayCategoriesFilter) return Promise.resolve(null);
    return this.categoriesService.getCategories();
  }

  private saveCategoryData(listFilter?: ListFilter) {
    if (this.productSearch && this.productSearch.categorySlugs && this.productSearch.categorySlugs.length > 0
      && this.categoryFilter && this.categoryFilter.items) {
      this.categoryFilter.selectedItems = this.productSearch.categorySlugs;
      const results = this.categoryFilter.items.filter(category => category.slug == this.productSearch.categorySlugs[0]);
      if (results.length > 0) this.categorySelected = results[0];
    }
    if (listFilter) this.categoryFilter = listFilter;
  }

  private getOrdinationValues() {
    this.ordinationService.getOrdinationValues()
      .then(response => this.ordinationValues = response)
      .catch(reason => this.logger.debug(reason));
  }
}
