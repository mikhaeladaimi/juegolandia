import {NgModule} from '@angular/core';

import {ThemeModule} from '../../@theme/theme.module';
import {ProductListModule} from "../../components/product-list/product-list.module";
import {SearchProductModule} from "../../components/search-product/search-product.module";
import {RangeSliderModule} from "../../components/range-slider/range-slider.module";
import {routedComponents, ShopRoutingModule} from './shop.routing.module';
import {FilterListModule} from "../../components/filter-list/filter-list.module";
import {FilterSelectModule} from "../../components/filter-select/filter-select.module";
import {SortingSelectModule} from "../../components/sorting-select/sorting-select.module";
import {MatFormFieldModule, MatSelectModule} from "@angular/material";
import {SidebarModule} from "../../components/sidebar/sidebar.module";
import {ProductDetailViewModule} from "../../components/product-detail-view/product-detail-view.module";
import {TabsModule} from "../../components/tabs/tabs.module";
import {GalleryModule} from "@ngx-gallery/core";
import {ShopSidebarComponent} from './sidebar/shop-sidebar.component';
import {ShopContentComponent} from './shop-content/shop-content.component';

@NgModule({
  imports: [
    ThemeModule,
    ProductListModule,
    RangeSliderModule,
    ShopRoutingModule,
    FilterListModule,
    FilterSelectModule,
    SortingSelectModule,
    ProductDetailViewModule,
    TabsModule,
    GalleryModule,
    SidebarModule,
    ProductListModule,
    SortingSelectModule,
    SearchProductModule,
    GalleryModule,
    MatFormFieldModule,
    MatSelectModule
  ],

  declarations: [
    ...routedComponents, ShopSidebarComponent, ShopContentComponent
  ],
})
export class ShopModule {
}

