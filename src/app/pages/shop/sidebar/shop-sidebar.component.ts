import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {THEME_CONFIG} from "../../../@theme/theme-config";
import {ActivatedRoute, Router} from "@angular/router";
import {PriceFilter} from "../../../models/filter/PriceFilter";
import {ListFilter} from "../../../models/filter/ListFilter";
import {FilterUtils} from "../../../shared/utils/filter.utils";
import {AttributeValue} from "../../../models/attribute/AttributeValue";
import {ProductSearchRequest} from "../../../models/product/ProductSearchRequest";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";
import {FeaturedProductList} from "../../../models/product/FeaturedProductList";

@Component({
  selector: 'app-shop-sidebar',
  templateUrl: './shop-sidebar.component.html',
  styleUrls: ['./shop-sidebar.component.scss']
})
export class ShopSidebarComponent implements OnInit, OnChanges {

  @Input() productSearch = new ProductSearchRequest();
  @Input() priceFilter: PriceFilter;
  @Input() categoryFilter: ListFilter;
  @Input() attributesFilter: ListFilter;
  @Input() productFeatured: FeaturedProductList;

  @Output() onFilterUpdated: EventEmitter<ProductSearchRequest> = new EventEmitter();
  @Output() onClearFilters: EventEmitter<any> = new EventEmitter();
  @Output() onSearchProduct: EventEmitter<string> = new EventEmitter();

  shopConfig = THEME_CONFIG.shopConfig;

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    if (this.attributesFilter) this.attributesFilter.selectedItems = this.productSearch.attributes;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.productSearch) {
      if (this.categoryFilter) this.categoryFilter.selectedItems = this.productSearch.categorySlugs;
      if (this.attributesFilter) this.attributesFilter.selectedItems = this.productSearch.attributes;
    }
  }

  filterPriceProducts(event) {
    this.productSearch.minPrice = event.minSelected * 100;
    this.productSearch.maxPrice = event.maxSelected * 100;
    this.onFilterUpdated.emit(this.productSearch);
  }

  filterCategoryProducts(event) {
    this.productSearch.categorySlugs = event;
    const categories = this.productSearch.categorySlugs[0] ? this.productSearch.categorySlugs[0] : "";
    this.router.navigate([SlugsUtils.getAbsoluteRoute(SlugsUtils.SHOP_FILTER), categories], {queryParamsHandling: "merge"});
    this.onFilterUpdated.emit(this.productSearch);
  }

  filterAttributeProducts(attributes: AttributeValue[]) {
    this.productSearch.attributes = attributes;
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {attributes: FilterUtils.getStringArrayFromAttributes(attributes)},
        queryParamsHandling: "merge",
      });
    this.onFilterUpdated.emit(this.productSearch);
  }

  clearFilters() {
    this.priceFilter.minSelected = this.priceFilter.minValue;
    this.priceFilter.maxSelected = this.priceFilter.maxValue;
    this.onClearFilters.emit();
  }

  searchProduct(name) {
    this.onSearchProduct.emit(name);
  }

}
