import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ShopComponent} from './shop.component';
import {ProductDetailComponent} from "./product-detail/product-detail.component";

const routes: Routes = [
  {
    path: '',
    component: ShopComponent
  },
  {
    path: 'tienda/:categories',
    component: ShopComponent
  },
  {
    path: 'detalle/:slug',
    component: ProductDetailComponent
  },
  {path: 'detalle', redirectTo: '', pathMatch: 'full'},
  {path: 'tienda', redirectTo: '', pathMatch: 'full'},

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }

export const routedComponents = [
    ShopComponent, ProductDetailComponent
  ];
