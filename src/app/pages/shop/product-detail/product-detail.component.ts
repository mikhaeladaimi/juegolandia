import {Component, OnInit} from '@angular/core';
import {THEME_CONFIG} from "../../../@theme/theme-config";
import {ProductsService} from "../../../shared/services/products.service";
import {Product} from "../../../models/product/Product";
import {ActivatedRoute, Router} from "@angular/router";
import {ModalUtils} from "../../../shared/utils/modal.utils";
import {ErrorResponseUtils} from "../../../shared/utils/errorResponse.utils";
import {OrderItem} from "../../../models/order/OrderItem";
import {CartCacheService} from "../../../shared/cache/cart-cache.service";
import {ProductUtils} from "../../../shared/utils/product.utils";
import {ScrollHelper} from "../../../shared/helpers/scroll.helper";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";
import {TabsData} from "../../../models/tabs-data/TabsData";

@Component({
  selector: 'app-shop',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  loading = false;
  product: Product;
  productFile;
  shopConfig;
  tabsData: TabsData[] = [];
  productSlug: string;


  constructor(private route: ActivatedRoute,
              private productsService: ProductsService,
              private router: Router,
              private modalUtils: ModalUtils,
              private cartUtils: CartCacheService,
              private scrollHelper: ScrollHelper) {
    this.route.params.subscribe(params => {
      if (params.slug) {
        this.productSlug = params.slug;
        this.getProduct(params.slug);
      }
    });
  }

  ngOnInit(): void {
    this.scrollHelper.scroll(0, 0);
    this.shopConfig = THEME_CONFIG.shopConfig;
  }

  getProduct(slug) {
    this.productFile = null;
    this.loading = true;
    this.productsService.getProduct(slug)
      .then(product => {
        this.product = product;
        this.tabsData = ProductUtils.getTabData(this.product);
        this.loading = false;
      })
      .catch(error => this.onError(error));
  }

  addProductToCart(orderItem: OrderItem) {
    if (!this.loading) {
      this.cartUtils.addProductToCart(orderItem.product.clone(), orderItem.quantity);
      this.modalUtils.openModal("Catálogo - Carrito", "El producto se añadió correctamente al carrito.");
    }
  }

  private onError(error) {
    this.loading = false;
    this.modalUtils.openErrorModal(ErrorResponseUtils.getStringError(ErrorResponseUtils.PRODUCT_NOT_FOUND)).then(
      () => this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.SHOP))
    ).catch(() => this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.SHOP)));
  }
}
