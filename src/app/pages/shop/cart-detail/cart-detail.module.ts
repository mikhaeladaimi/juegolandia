import {NgModule} from '@angular/core';
import {ThemeModule} from '../../../@theme/theme.module';
import {CartDetailRoutingModule, routedComponents} from "./cart-detail.routing.module";
import {AutocompleteModule} from "../../../components/autocomplete/autocomplete.module";
import {MatProgressSpinnerModule} from "@angular/material";

@NgModule({
  imports: [
    ThemeModule,
    CartDetailRoutingModule,
    AutocompleteModule,
    MatProgressSpinnerModule
  ],

  declarations: [
    ...routedComponents
  ],
})
export class CartDetailModule {
}

