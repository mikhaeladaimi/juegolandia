import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CartDetailComponent} from "./cart-detail.component";

const routes: Routes = [
  {
    path: '',
    component: CartDetailComponent
  },
  {path: '*', redirectTo: '', pathMatch: 'full'},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartDetailRoutingModule {
}

export const routedComponents = [
  CartDetailComponent
];
