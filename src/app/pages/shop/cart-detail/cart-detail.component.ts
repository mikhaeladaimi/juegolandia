import {Component, OnInit} from '@angular/core';
import {THEME_CONFIG} from "../../../@theme/theme-config";
import {Product} from "../../../models/product/Product";
import {Router} from "@angular/router";
import {CartCacheService} from "../../../shared/cache/cart-cache.service";
import {Subscription} from "rxjs/Rx";
import {OrderItem} from "../../../models/order/OrderItem";
import {ModalUtils} from "../../../shared/utils/modal.utils";
import {DeliveryPrice} from "../../../models/price/DeliveryPrice";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";
import {AuthService} from "../../../shared/services/auth.service";
import {EndpointsUtils} from "../../../shared/endpoint.utils";

@Component({
  selector: 'app-shop',
  templateUrl: './cart-detail.component.html',
  styleUrls: ['./cart-detail.component.scss']
})
export class CartDetailComponent implements OnInit {

  loading = false;
  product: Product;
  shopConfig;
  subscription: Subscription;
  orderItems: OrderItem[];
  totalPrice: number;
  hasDeliveryCost;

  deliveryPrices: DeliveryPrice = new DeliveryPrice();
  submitted: boolean = false;

  // region routes
  productDetailRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.PRODUCT_DETAIL);
  shopRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.SHOP);

  // endregion

  constructor(private router: Router, public cartUtils: CartCacheService,
              private authService: AuthService,
              private modalUtils: ModalUtils) {
  }

  ngOnInit(): void {
    if (this.cartUtils.countItems() > 0) {
      if (!this.subscription) {
        this.subscription = this.cartUtils.cartObservable.subscribe(() => {
          this.retrieveCartInfo();
        });
      }
    }

    this.shopConfig = THEME_CONFIG.shopConfig;
  }

  retrieveCartInfo() {
    this.orderItems = this.cartUtils.getCartData();
    this.totalPrice = this.cartUtils.getTotal();
    this.hasDeliveryCost = this.deliveryPrices.price > 0 && this.deliveryPrices.freeDeliveryThreshold > this.totalPrice;
  }

  removeItem(item: OrderItem) {
    this.modalUtils.openCancelableModal("Vas a eliminar el producto del carrito", "¿Estás seguro?", "Sí", "Cancelar")
      .then(() => this.cartUtils.deleteItemFromCart(item))
      .catch(() => {
      });
  }

  checkRange(quantity, item, inputQuantity) {
    if (quantity && quantity >= 1) {
      item.quantity = quantity;
    } else {
      inputQuantity.value = "";
      item.quantity = 1;
    }
    this.updateCart();
  }

  updateCart() {
    this.cartUtils.saveCartData(this.orderItems);
  }

  next(event) {
    this.submitted = true;
    event.preventDefault();
    this.loading = true;
    this.checkOrder();
  }

  navigateToCheckout() {
    this.router.navigate([SlugsUtils.getAbsoluteRoute(SlugsUtils.CHECKOUT)]);
  }

  private checkOrder() {
    if (!this.authService.isUserLogged()) {
      this.loading = false;
      this.modalUtils.openModal("¡Espera un momento!",
        "Necesitas registrarte o iniciar sesión para comprar en nuestra tienda")
        .then(() => {
        })
        .catch(() => {
        })
        .then(() => {
          this.router.navigateByUrl(EndpointsUtils.getAccess());
        });
    } else {
      this.orderItems.forEach((item, index) => item.position = index);
      this.navigateToCheckout();
    }
  }

}
