import {Component, OnInit} from '@angular/core';
import {THEME_CONFIG} from "../../@theme/theme-config";
import {HorizontalAlignmentType} from "../../models/enum/HorizontalAlignmentType";
import {VerticalAlignmentType} from "../../models/enum/VerticalAlignmentType";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactConfig;
  horizontalAlignment = HorizontalAlignmentType;
  mapWidth: string = "100%";
  mapHeight: string = "500px";
  isMapBottom = THEME_CONFIG.contactConfig.map.mapAlign && THEME_CONFIG.contactConfig.map.mapAlign == VerticalAlignmentType.BOTTOM;
  isSidebarRight = THEME_CONFIG.contactConfig.sidebar && THEME_CONFIG.contactConfig.sidebar != HorizontalAlignmentType.RIGHT;

  imgSrc: string;
  showZoomImage: boolean;

  constructor() {

  }

  ngOnInit(): void {
    this.contactConfig = THEME_CONFIG.contactConfig;
  }

  onImageClick(imgSrc) {
    if (imgSrc) {
      this.imgSrc = imgSrc;
      this.showZoomImage = true;
    }
  }

  onZoomImageClick() {
    this.showZoomImage = false;
    this.imgSrc = "";
  }
}
