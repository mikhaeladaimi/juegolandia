import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { MapModule } from "../../components/map/map.module";
import { ContactFormModule } from "../../components/contact-form/contact-form.module";
import { ContactRoutingModule, routedComponents } from './contact.routing.module';

@NgModule({
  imports: [ThemeModule, MapModule, ContactFormModule, ContactRoutingModule],

  declarations: [
    ...routedComponents
  ],
})
export class ContactModule { }

