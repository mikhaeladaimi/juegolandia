import {NgModule} from '@angular/core';

import {ThemeModule} from "../../@theme/theme.module";
import { LegalRoutingModule, routedComponents } from './legal.routing.module';

@NgModule({
  imports: [
    ThemeModule, LegalRoutingModule
  ],

  declarations: [
    ...routedComponents
  ],
})
export class LegalModule {
}

