import {Component, OnInit} from '@angular/core';
import {LegalService} from "../../shared/view/legal.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {LegalSection} from "./LegalSection";

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss']
})

export class LegalComponent implements OnInit {

  data: any = {displayPageTitle: true};

  constructor(private router: Router, private route: ActivatedRoute, private legalService: LegalService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const section: LegalSection = params['section'] as LegalSection;
      let promise;
      switch (section) {
        case LegalSection.COOKIES:
          promise = this.legalService.getCookiesData();
          break;
        case LegalSection.TERMS:
          promise = this.legalService.getTermsData();
          break;
        case LegalSection.DISCLAIMER:
          promise = this.legalService.getDisclaimerData();
          break;
        default:
          const newUrl = "/legal/" + LegalSection.DISCLAIMER;
          this.router.navigate([newUrl], { replaceUrl: true });
          return;
      }
      promise.then(response => this.data = response);
    });
  }

}
