import {NgModule} from '@angular/core';

import {ThemeModule} from '../../@theme/theme.module';
import {Error404PageRoutingModule, routedComponents} from './error-404-page.routing.module';

@NgModule({
  imports: [
    ThemeModule,
    Error404PageRoutingModule
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class Error404PageModule {
}

