import {Component} from '@angular/core';
import {CategoriesService} from "../../shared/services/categories.service";
import {ModalUtils} from '../../shared/utils/modal.utils';
import {Category} from "../../models/category/Category";
import {Media} from "../../models/media/Media";
import {Video} from "../../models/video/Video";
import {DataSection} from "../../models/data-section/DataSection";
import {GeneralLink} from "../../models/link/GeneralLink";
import {HorizontalAlignmentType} from "../../models/enum/HorizontalAlignmentType";

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent {

  result = {};
  categories: Category[];
  error = null;
  errorBody = {};
  background: string;

  dataInfo: DataSection;
  dataButton: GeneralLink;

  mediaInfo: Media;
  videoInfo: Video;

  constructor(private categoriesService: CategoriesService, private modalUtils: ModalUtils) {

    this.dataButton = new GeneralLink("", "", "/", "button", "_blank");
    this.dataInfo = new DataSection("titulo", "Lorem Ipsum tal tal tal Lorem Ipsum tal tal tal Lorem Ipsum tal tal tal Lorem Ipsum tal tal tal", this.dataButton,
      HorizontalAlignmentType.CENTER);
    this.videoInfo = new Video("title video", "https://www.youtube.com/embed/tgbNymZ7vqY", "video description");
    this.mediaInfo = new Media("https://e00-marca.uecdn.es/assets/multimedia/imagenes/2019/03/18/15528979070822.jpg", this.videoInfo, HorizontalAlignmentType.NONE);
  }

  getCategories() {
    this.categoriesService.getCategories().then(categories => this.categories = categories);
  }

  showModal() {
    this.modalUtils.openModal("BENVENUTI", "Esto es esparta");
  }

  displayCreditCardModal() {
    this.modalUtils.openCreditCardModal(32);
  }

}
