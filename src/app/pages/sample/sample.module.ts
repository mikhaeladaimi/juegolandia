import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { InfoSectionModule } from "../../components/info-section/info-section.module";
import { SampleRoutingModule, routedComponents } from './sample.routing.module';
import { StripeCardModule } from "../../components/modal-stripe-card/stripe-card.module";
import { BankTransferModule } from '../../components/bank-transfer/bank-transfer.module';

@NgModule({
  imports: [
    ThemeModule,
    SampleRoutingModule,
    InfoSectionModule,
    StripeCardModule,
    BankTransferModule
  ],
  declarations: [
    ...routedComponents
  ],
})
export class SampleModule { }

