import {Component, OnDestroy, OnInit} from '@angular/core';
import {OrderItem} from "../../models/order/OrderItem";
import {ViewUtils} from "../../shared/utils/view.utils";
import {CartCacheService} from "../../shared/cache/cart-cache.service";
import {OrderService} from "../../shared/services/order.service";
import {Router} from "@angular/router";
import {ModalUtils} from "../../shared/utils/modal.utils";
import {UserService} from "../../shared/services/user.service";
import {NGXLogger} from "ngx-logger";
import {PaymentType} from "../../models/enum/PaymentType";
import {Address} from "../../models/address/Address";
import {THEME_CONFIG} from "../../@theme/theme-config";
import {AuthService} from "../../shared/services/auth.service";
import {SyncUtils} from "../../shared/utils/sync-utils.service";
import {User} from "../../models/user/User";
import {OrderCheck} from "../../models/order/OrderCheck";
import {SlugsUtils} from "../../shared/utils/slugs.utils";
import {Order} from "../../models/order/Order";
import {OrderLine} from "../../models/order/OrderLine";
import {DeliveryPrice} from "../../models/price/DeliveryPrice";
import {OrderStatusType} from "../../models/enum/OrderStatusType";
import {PriceLine} from "../../models/price/PriceLine";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit, OnDestroy {

  private userSubscription;
  totalPrice: string;
  orderItems: OrderItem[];
  userData: User = new User();
  checkOrder: OrderCheck = new OrderCheck();
  loading: boolean;
  isLogged: boolean;
  billingAddress: Address = new Address();

  displayStripe = THEME_CONFIG.shopConfig.displayStripe;

  constructor(private cartUtils: CartCacheService, private orderService: OrderService, private router: Router,
              private modalUtils: ModalUtils, private userService: UserService, private logger: NGXLogger,
              private authService: AuthService) {
  }

  ngOnInit() {
    if (!this.authService.isUserLogged()) this.navigateToCart();
    this.setUser();
  }

  ngOnDestroy(): void {
    SyncUtils.unsubscribe(this.userSubscription);
  }

  retrieveCartInfo() {
    this.orderItems = this.cartUtils.getCartData();
    this.totalPrice = ViewUtils.getPrice(this.cartUtils.getTotal());
    if (!this.orderItems || this.orderItems.length == 0) {
      this.navigateToCart();
    }
  }

  setUser() {
    if (!this.userService.user) this.navigateToCart();
    this.userData = new User().deserialize(this.userService.user);
    this.isLogged = true;
    this.retrieveCartInfo();
  }

  submit(event) {
    if (event.valid) {
      if (!event.paymentType) {
        this.modalUtils.openErrorModal("Debes seleccionar un método de pago.");
      } else if (!event.privacyPolicy) {
        this.modalUtils.openErrorModal("Debes aceptar la política de privacidad.");
      } else {
        const isCreditCard = event.paymentType == PaymentType.STRIPE;
        const order = this.generateOrderObject(
          isCreditCard, this.getBillingAddress(event.differentAddress), event.observations,
          this.userData.address, "stripeToken");

        if (isCreditCard) {
          this.loading = false;
          this.modalUtils.openCreditCardModal(order.totalPrice, this.userData)
            .then(solve => {
              order.stripeToken = solve;
              this.loading = true;
              this.doPurchase(order);
            }).catch(cancel => cancel());
        } else {
          this.loading = true;
          this.doPurchase(order);
        }


      }
    } else {
      this.onError("Los campos del formulario no son correctos.");
    }
  }

  doPurchase(order: Order) {
    this.loading = true;
    this.orderService.purchase(order)
      .then(orderId => {
        this.purchaseSuccessful(orderId);
        this.loading = false;
      }).catch(error => this.onError("No se pudo realizar el pedido.", error));
  }

  navigateToCart() {
    this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.CART_DETAIL));
  }

  generateOrderObject(isCreditCard, billingAddress, observations, deliveryAddress, stripeToken) {
    const orderLines: OrderLine[] = [];
    for (const line of this.orderItems) {
      orderLines.push(new OrderLine().deserialize(line));
    }
    const deliveryPrices = new DeliveryPrice();
    const hasDeliveryPrice = deliveryPrices.freeDeliveryThreshold > this.cartUtils.getTotal();
    const total = this.cartUtils.getTotal() + (hasDeliveryPrice ? deliveryPrices.price : 0);

    return new Order(null, total, OrderStatusType.ACCEPTED, isCreditCard, this.userData.email,
      observations, this.userData.name, billingAddress, deliveryAddress, null, new Date().toISOString(),
      [new PriceLine(hasDeliveryPrice ? deliveryPrices.price : 0, "Gastos de envío")],
      orderLines, this.userData.phone, null, null, stripeToken);
  }

  purchaseSuccessful(purchaseId) {
    this.modalUtils.openModal("Pedido realizado", "Tu pedido se realizó correctamente.");
    this.cartUtils.clearCache();
    this.router.navigate([SlugsUtils.getAbsoluteRoute(SlugsUtils.ORDER) + '/' + purchaseId]);
  }

  private getBillingAddress(differentAddress) {
    return (differentAddress && this.billingAddress && Object.entries(this.billingAddress).length > 0) ? this.billingAddress : this.userData.address;
  }

  private onError(body, error?) {
    this.loading = false;
    this.modalUtils.openErrorModal(body);
    error && this.logger.debug(error);
  }
}
