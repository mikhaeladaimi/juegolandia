import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {OrderSummaryComponent} from "./order-summary/order-summary.component";
import {CheckoutRoutingModule, routedComponents} from "./checkout.routing.module";
import {OrderUserDataComponent} from "./order-user-data/order-user-data.component";

@NgModule({
  imports: [
    ThemeModule,
    CheckoutRoutingModule
  ],

  declarations: [
    ...routedComponents, OrderUserDataComponent, OrderSummaryComponent
  ],
})
export class CheckoutModule {
}

