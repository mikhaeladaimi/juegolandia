import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {OrderItem} from "../../../models/order/OrderItem";
import {User} from "../../../models/user/User";
import {OrderCheck} from "../../../models/order/OrderCheck";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-user-data-component',
  templateUrl: './order-user-data.component.html',
  styleUrls: ['./order-user-data.component.scss']
})
export class OrderUserDataComponent {

  FIELDS: any[] = [
    {id: "name", label: "Nombre*", required: true, type: "text", maxlength: 50, pattern: ""},
    {
      id: "email", label: "E-mail*", required: true, type: "email", maxlength: 50,
      pattern: "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
    },
    {parent: "address", id: "dni", label: "DNI/CIF*", required: true, type: "text", maxlength: 50, minlength: 6},
    {parent: "address", id: "country", label: "País*", required: true, type: "text", maxlength: 50, disabled: true},
    {parent: "address", id: "address", label: "Dirección*", required: true, type: "text", maxlength: 50, pattern: ""},
    {parent: "address", id: "postalCode", label: "Código postal*", required: true, type: "text", maxlength: 5, minlength: 5, pattern: "^[0-9]{5}$", disabled: false},
    {parent: "address", id: "locality", label: "Localidad*", required: true, type: "text", maxlength: 50, pattern: "", disabled: false},
    {parent: "address", id: "province", label: "Provincia*", required: true, type: "text", maxlength: 50, pattern: "", disabled: false},
    {id: "phone", label: "Teléfono*", required: true, type: "tel", maxlength: 20, pattern: "^(\\+34|0034|34)?[6|7|9][0-9]{8}$"}
  ];

  @ViewChild('form') form;
  @ViewChild('formDelivery') formDelivery;
  @Input() userData: User;
  @Input() displayStripe: boolean;
  @Input() checkOrder: OrderCheck;
  @Input() isLogged: boolean;
  @Input() orderItems: OrderItem[];
  @Input() totalPrice: string;
  @Input() delivery: any;
  @Output() onSubmit: EventEmitter<any> = new EventEmitter();
  submitted: boolean = false;
  differentAddress: boolean = false;
  observations: string;

  // region routes
  accessRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.ACCESS);

  // endregion

  submit(orderSummaryData) {
    this.submitted = true;
    this.onSubmit.emit({
      valid: ((!this.differentAddress && this.form.valid) || this.form.valid && this.formDelivery.valid),
      paymentType: orderSummaryData.paymentType,
      observations: this.observations,
      differentAddress: this.differentAddress,
      privacyPolicy: this.isLogged || orderSummaryData.privacyPolicy
    });
  }
}
