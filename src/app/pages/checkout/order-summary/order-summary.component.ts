import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OrderItem} from "../../../models/order/OrderItem";
import {PaymentType} from "../../../models/enum/PaymentType";
import {OrderCheck} from "../../../models/order/OrderCheck";
import {CartCacheService} from "../../../shared/cache/cart-cache.service";
import {DeliveryPrice} from "../../../models/price/DeliveryPrice";

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})
export class OrderSummaryComponent implements OnInit {

  @Input() orderItems: OrderItem[];
  @Input() displayStripe: boolean;
  @Input() checkOrder: OrderCheck;
  @Input() isLogged: boolean;
  paymentType: PaymentType = PaymentType.STRIPE;
  @Output() onSubmit: EventEmitter<any> = new EventEmitter<any>();

  deliveryPrices: DeliveryPrice = new DeliveryPrice();
  paymentTypes = [];
  privacyPolicy: boolean = false;
  totalPrice;
  hasDeliveryCost = false;

  constructor(private cartUtils: CartCacheService) {
  }

  ngOnInit(): void {
    this.setPaymentTypes();
    this.totalPrice = this.cartUtils.getTotal();
    this.hasDeliveryCost = this.deliveryPrices.price > 0 && this.deliveryPrices.freeDeliveryThreshold > this.totalPrice;
  }

  submit() {
    this.onSubmit.emit({paymentType: this.paymentType, privacyPolicy: this.privacyPolicy});
  }



  private setPaymentTypes() {
    this.paymentType = PaymentType.STRIPE;
    this.paymentTypes.push(PaymentType.STRIPE);
    this.paymentTypes.push(PaymentType.TRANSFER);
  }

}
