import {Component, OnInit} from '@angular/core';
import {Order} from "../../models/order/Order";
import {ActivatedRoute, Params} from "@angular/router";
import {NGXLogger} from "ngx-logger";
import {UserOrdersService} from "../../shared/services/user-orders.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  order: Order;
  loading: boolean;

  constructor(private route?: ActivatedRoute, private userOrderService?: UserOrdersService, private logger?: NGXLogger) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      if (params && params.id && !isNaN(+params.id)) {
        this.loading = true;
        const id = +params.id;
        this.getOrder(id);
      }
    });
  }

  print() {
    window.print();
  }

  private getOrder(id: number) {
    this.userOrderService.getUserOrder(id)
      .then(order => {
        this.order = order;
      })
      .catch(error => this.logger.debug(error))
      .then(() => this.loading = false);
  }
}
