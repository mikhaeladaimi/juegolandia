import {NgModule} from '@angular/core';

import {ThemeModule} from '../../@theme/theme.module';
import {OrderRoutingModule, routedComponents} from './order.routing.module';
import {OrderDetailModule} from "../../components/order-detail/order-detail.module";
import {MatProgressSpinnerModule} from "@angular/material";

@NgModule({
  imports: [ThemeModule, OrderDetailModule, MatProgressSpinnerModule, OrderRoutingModule],

  declarations: [
    ...routedComponents
  ],
})
export class OrderModule {
}

