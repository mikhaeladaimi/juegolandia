import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  @ViewChild("form") form;
  @Output() onSubmit = new EventEmitter<{}>();
  @Input() loading: boolean = false;
  submitted: boolean = false;

  register = {email: '', password: '', confirmedPassword: '', isLegalConfirmed: '', newsletter: false};

  // region routes
  privacyRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.PRIVACY);
  termsRoute = SlugsUtils.getAbsoluteRoute(SlugsUtils.NOTICE);

  // endregion

  constructor() {
  }


  public createUser() {
    if (!this.form.invalid && (this.register.password == this.register.confirmedPassword)) {
      this.submitted = false;
      this.onSubmit.emit(this.register);
    } else {
      this.submitted = true;
    }
  }

}
