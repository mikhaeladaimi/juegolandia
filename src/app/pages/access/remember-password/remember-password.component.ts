import { Component, ViewChild } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Router } from "@angular/router";
import { ErrorResponseUtils } from "../../../shared/utils/errorResponse.utils";
import { ModalUtils } from "../../../shared/utils/modal.utils";
import { AuthService } from "../../../shared/services/auth.service";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-remember-password',
  templateUrl: './remember-password.component.html',
  styleUrls: ['./remember-password.component.scss']
})
export class RememberPasswordComponent {

  @ViewChild("form") form;
  emailFound = "";
  loading: boolean = false;
  submitted: boolean = false;

  constructor(private authService: AuthService,
    private modalUtils: ModalUtils, private logger: NGXLogger, private router: Router) {
  }

  rememberPasswordService() {
    this.submitted = true;
    if (!this.form.invalid) {
      this.loading = true;

      const email = this.emailFound;
      this.authService.resetPassword(email).then(result => {
        this.loading = false;

        this.openModalMsgSent();

      }).catch(error => {
        this.loading = false;
        this.logger.debug("(RememberPassword) error en rememberPassword: ", error);

        if (ErrorResponseUtils.getCode(error) == ErrorResponseUtils.USER_NOT_FOUND) {
          this.modalUtils.openErrorModal("El usuario no está registrado");
        } else {
          this.modalUtils.openErrorModal("No hemos conseguido enviar el e-mail de recuperación. Por favor, inténtalo más tarde");
        }
      });
    }
  }

  private openModalMsgSent() {
    this.modalUtils.openModal("Mensaje enviado", "Hemos enviado un e-mail a tu correo para recuperar tu cuenta").then(request => {
      this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.ACCESS));
    }).catch(exit => {
      this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.ACCESS));
    });
  }


}
