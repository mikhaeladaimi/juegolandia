import { ErrorResponseUtils } from '../../../shared/utils/errorResponse.utils';
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import {AuthService} from "../../../shared/services/auth.service";
import {ModalUtils} from "../../../shared/utils/modal.utils";
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {

  @ViewChild("form") form;

  password: string = "";
  passwordConfirmed: string = "";

  mode: string = "";
  code: string = "";

  submitted: Boolean = false;
  loading: boolean = false;

  constructor(private authService: AuthService, private modalUtils: ModalUtils, private route: ActivatedRoute,
    private router: Router, private logger: NGXLogger) {
    if (this.route != null) {
      this.route.queryParams.subscribe(params => {
        params["mode"] != null ? this.mode = params["mode"] : this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.ACCESS));
        params["oobCode"] != null ? this.code = params["oobCode"] : this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.ACCESS));
      });
    }
  }

  updatePassword() {
    if (!this.form.invalid && (this.password == this.passwordConfirmed)) {
      this.loading = true;
      this.submitted = false;

      this.authService.updatePassword(this.mode, this.code, this.password)
        .then(resolve => {
          this.modalUtils.openModal("¡Exito!", "La contraseña ha sido actualizada correctamente").then(() => {
            this.submitted = false;
            this.loading = false;
            this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.ACCESS));
          });
        }).catch(error => {
          this.submitted = false;
          this.loading = false;

          this.logger.debug('(Reset-password) Error en updatePassword: ', error);

          if (error == "invalidPass") {
            this.modalUtils.openErrorModal("La contraseña introducida no es válida");
          } else {
            this.modalUtils.openErrorModal(ErrorResponseUtils.getStringError(error)).then(() => {
              this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.ACCESS));
            });
          }
        });
    } else {
      this.submitted = true;
    }
  }
}
