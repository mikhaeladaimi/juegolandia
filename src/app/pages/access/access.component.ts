import {Component, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from "../../shared/services/auth.service";
import {ErrorResponseUtils} from "../../shared/utils/errorResponse.utils";
import {ModalUtils} from "../../shared/utils/modal.utils";
import {Subscription} from "rxjs";
import {UserService} from "../../shared/services/user.service";
import {User} from "../../models/user/User";
import {SlugsUtils} from "../../shared/utils/slugs.utils";

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss']
})
export class AccessComponent implements OnDestroy {

  loading: boolean = false;
  userSubscription: Subscription;

  constructor(private authService: AuthService, private router: Router, private modalUtils: ModalUtils,
              private userService: UserService) {
  }

  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }

  loginUser(loginData) {
    this.loading = true;
    this.authService.login(loginData['email'], loginData['password'])
      .then(() => {
        this.subscribeToUser();
      }).catch(error => {
      this.loading = false;
      let message = "Se ha producido un error";
      const code = ErrorResponseUtils.getCode(error);
      switch (code ? code.toString() : false) {
        case ErrorResponseUtils.FB_USER_NOT_FOUND:
        case ErrorResponseUtils.FB_WRONG_PASS:
          message = "El e-mail o la contraseña son erróneos";
          break;
      }
      this.authService.logout();
      this.modalUtils.openErrorModal(message);
    });
  }


  createUser(registerData) {
    this.loading = true;
    this.authService.createUser(registerData['email'], registerData['password'], registerData['newsletter'])
      .then(() => {
        this.userService.createUser(registerData['email'], this.authService.getFirebaseUid());
        this.loginUser({email: registerData['email'], password: registerData['password']});
      }).catch(error => {
      this.loading = false;
      if (error.code == ErrorResponseUtils.FB_USER_REGISTER) {
        this.modalUtils.openDangerCancelableModal("Vaya!", "El usuario ya existe. Si no recuerdas tu contraseña podemos " +
          " enviarte un correo para recuperarla.").then(() => {
          this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.REMEMBER_ACCESS));
        }).catch(() => {
        });
      } else {
        this.modalUtils.openErrorModal(ErrorResponseUtils.getStringError(error));
      }
    });
  }

  private subscribeToUser() {
    if (this.userSubscription) this.userSubscription.unsubscribe();
    this.userSubscription = this.userService.getObservableUser().subscribe(solve => {
      if (solve instanceof User) this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME));
    });
  }

}
