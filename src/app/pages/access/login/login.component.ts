import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {SlugsUtils} from "../../../shared/utils/slugs.utils";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  @ViewChild("form") form;
  @Output() onSubmit = new EventEmitter<{}>();
  @Input() loading = false;

  submitted: boolean = false;
  login = {email: '', password: ''};

  // region routes
  rememberAccess = SlugsUtils.getAbsoluteRoute(SlugsUtils.REMEMBER_ACCESS);

  // endregion

  constructor() {
  }


  loginUser() {
    if (!this.form.invalid) {
      this.submitted = false;
      this.onSubmit.emit(this.login);

    } else {
      this.submitted = true;
    }

  }

}
