import {ThemeModule} from "../../@theme/theme.module";
import {MatCardModule, MatTabsModule} from '@angular/material';
import {NgModule} from '@angular/core';
import { AccessRoutingModule, routedComponents } from "./access.routing.module";


@NgModule({
  imports: [
    ThemeModule, MatCardModule, AccessRoutingModule, MatTabsModule
  ],

  declarations: [
    ...routedComponents
  ],
})
export class AccessModule {
}

