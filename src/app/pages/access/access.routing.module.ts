import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessComponent } from './access.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { RememberPasswordComponent } from './remember-password/remember-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const routes: Routes = [
  {
    path: '',
    component: AccessComponent
  },
  {
    path: 'nuevo',
    component: ResetPasswordComponent
  },
  {
    path: 'recordar',
    component: RememberPasswordComponent
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessRoutingModule { }

export const routedComponents = [
  AccessComponent, RegisterComponent, LoginComponent, RememberPasswordComponent, ResetPasswordComponent,
];
