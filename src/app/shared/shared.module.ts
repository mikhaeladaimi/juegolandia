import {NgModule} from '@angular/core';
import {ViewUtils} from './utils/view.utils';
import {DateUtils} from './utils/date.utils';
import {ErrorResponseUtils} from './utils/errorResponse.utils';
import {ModalUtils} from './utils/modal.utils';
import {CategoriesService} from './services/categories.service';
import {ProductsService} from './services/products.service';
import {SliderService} from './view/slider.service';
import {QuestionsService} from './services/questions.service';
import {InfoObjectsService} from './view/info.objects.service';
import {LegalService} from './view/legal.service';
import {GridService} from './view/grid.service';
import {AuthService} from './services/auth.service';
import {ContactService} from './services/contact.service';
import {UserService} from './services/user.service';
import {ImageUtils} from "./utils/image.utils";
import {MatPaginatorIntlEsp} from "./utils/mat.paginator.int.esp";
import {ProductUtils} from "./utils/product.utils";
import {AttributesService} from "./services/attributes.service";
import {OrdinationService} from "./services/ordination.service";
import {DummyUtils} from "./utils/dummy.utils";
import {FilterUtils} from "./utils/filter.utils";
import {OrderService} from "./services/order.service";
import {UserOrdersService} from "./services/user-orders.service";
import {CartCacheService} from "./cache/cart-cache.service";
import {FeedService} from "./services/feed.service";
import {CompanyService} from "./view/company.service";
import {InitService} from "./services/init.service";
import {AddressService} from "./services/address.service";
import {DeliveryZonesService} from "./services/delivery.zones.service";
import {CacheService} from "./cache/cache.service";
import {ScrollHelper} from "./helpers/scroll.helper";
import {SyncUtils} from "./utils/sync-utils.service";
import {CaptchaService} from "./services/captcha.service";
import {LocationHelper} from "./helpers/location.helper";
import {PrintHelper} from "./helpers/print.helper";
import {EncryptionHelpers} from "./helpers/encryption.helpers";
import {SeoUtils} from "./utils/seo.utils";
import {ProductFilesService} from './services/product-files.service';
import {UserCacheService} from "./cache/user-cache.service";
import {OrderCacheService} from "./cache/order-cache.service";

@NgModule({
  providers: [
    DateUtils,
    ErrorResponseUtils,
    ImageUtils,
    MatPaginatorIntlEsp,
    ModalUtils,
    ProductUtils,
    ViewUtils,
    DummyUtils,
    FilterUtils,
    SyncUtils,
    SeoUtils,

    EncryptionHelpers,
    ScrollHelper,
    LocationHelper,
    PrintHelper,

    CacheService,
    CartCacheService,
    UserCacheService,
    OrderCacheService,

    GridService,
    InfoObjectsService,
    SliderService,
    OrderService,
    UserOrdersService,

    AttributesService,
    AuthService,
    CategoriesService,
    ContactService,
    LegalService,
    OrdinationService,
    ProductsService,
    ProductFilesService,
    QuestionsService,
    UserService,
    FeedService,
    CompanyService,
    InitService,
    AddressService,
    DeliveryZonesService,
    OrderService,
    CaptchaService
  ]
})
export class SharedModule {
}
