import {Injectable} from "@angular/core";

@Injectable()
export class ImageUtils {

  public static DEFAULT_PRODUCT_IMAGE = "assets/logo-trans.png";
  public static DEFAULT_BLOG_IMAGE = "assets/logo-trans.png";
  public static DEFAULT_IMAGE = "assets/logo-trans.png";

  public static getImageUrl(image) {
    return (image && image.url) ? image.url : ImageUtils.DEFAULT_PRODUCT_IMAGE;
  }

  public static getImageByUrl(url) {
    return url ? url : ImageUtils.DEFAULT_PRODUCT_IMAGE;
  }

  public static getFormattedUrl(image) {
    return (image && image.url) ? image.url + "?" + image.signature : ImageUtils.DEFAULT_PRODUCT_IMAGE;

  }

}
