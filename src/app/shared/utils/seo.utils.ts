import {Injectable} from '@angular/core';
import {filter, map, mergeMap} from "rxjs/operators";
import {Meta, Title} from "@angular/platform-browser";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";

@Injectable()
export class SeoUtils {

  constructor(private title: Title, private meta: Meta) { }

  updateTitle(title: string) {
    this.title.setTitle(title);
  }

  updateOgUrl(url: string) {
    this.meta.updateTag({ name: 'og:url', content: url });
  }

  updateDescription(desc: string) {
    this.meta.updateTag({ name: 'description', content: desc });
  }

  updateRoute(router: Router, activatedRoute: ActivatedRoute) {
    router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => activatedRoute),
      map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data)
    )
      .subscribe((event) => {
        this.updateTitle(event['title']);
        this.updateOgUrl(event['ogUrl']);
        this.updateDescription(event['title'] + event['description']);
      });
  }
}
