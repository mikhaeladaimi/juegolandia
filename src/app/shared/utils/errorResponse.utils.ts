import {Injectable} from "@angular/core";
import {isNumber} from "util";

@Injectable()
export class ErrorResponseUtils {

  // Errors from our API
  public static USER_NOT_FOUND = "1";
  public static USER_DELETED = "2";
  public static USER_PENDING = "3";
  public static USER_DENIED = "4";
  public static ADMIN_NOT_FOUND = "5";
  public static ADMIN_DELETED = "6";
  public static ADMIN_PENDING = "7";
  public static ADMIN_DENIED = "8";
  public static USER_ACCESS_NOT_DELETED = "9";

  public static PRODUCT_NOT_FOUND = "10";
  public static VARIANT_NOT_FOUND = "11";
  public static VARIABLE_PRODUCT_NOT_FOUND = "12";
  public static INVALID_VARIANT_ATTRIBUTES = "13";
  public static SIMPLE_PRODUCT_NOT_FOUND = "14";
  public static IMAGE_POSITIONS_NOT_FOUND = "15";
  public static INVALID_VARIANT_ATTRIBUTE = "16";
  public static DUPLICATE_VARIANT_ATTRIBUTES = "18";
  public static CANNOT_EXTRACT_VARIANTS = "19";

  public static CATEGORY_NOT_FOUND = "20";
  public static CATEGORIES_NOT_FOUND = "21";
  public static CANNOT_DELETE_CATEGORY = "22";
  public static CATEGORY_UPDATE_FAILED = "23";
  public static ATTRIBUTE_NOT_FOUND = "25";

  public static PRODUCT_FILE_NOT_FOUND = "30";
  public static PRODUCT_FILE_NOT_FOUND_ON_SERVER = "31";

  public static PRODUCTS_NOT_FOUND = "100";
  public static PRODUCTS_WITHOUT_STOCK = "101";
  public static PRICE_BELOW_MINIMUM = "102";
  public static INVALID_PRODUCT_ATTRIBUTES = "103";

  public static PREORDER_NOT_FOUND = "110";
  public static ERROR_UPDATING_PREORDER = "111";
  public static PREORDER_IN_PROGRESS = "112";
  public static PREORDER_BEING_UPDATED = "113";
  public static PREORDER_ALREADY_DONE = "114";
  public static PREORDER_TIMED_OUT = "115";
  public static DISTINCT_PREORDER_USER = "116";
  public static DISTINCT_PREORDER_PRICE = "117";
  public static DISTINCT_PREORDER_PRODUCTS = "118";
  public static ERROR_UPDATING_STOCK = "119";
  public static ORDER_NOT_FOUND = "130";
  public static CANNOT_UPDATE_ORDER = "131";
  public static INVALID_TARGET_STATUS = "132";
  public static ORDER_NOT_PAID = "133";

  public static DELIVERY_ZONE_NOT_FOUND = "140";
  public static COMMUNITY_NOT_FOUND = "141";
  public static COUNTRY_NOT_FOUND = "142";
  public static LOCALITY_NOT_FOUND = "143";
  public static POSTAL_CODE_NOT_FOUND = "144";
  public static PROVINCE_NOT_FOUND = "145";

  public static COUPON_NOT_FOUND = "150";
  public static COUPON_LIMIT_USAGE_EXCEEDED = "152";
  public static ORDER_PRICE_BELOW_COUPON_MINIMUM = "153";

  public static ERROR_SENDING_EMAIL = "200";
  public static ERROR_UPDATING_PRODUCT = "210";

  public static FEATURE_NOT_FOUND = "350";

  public static IMAGE_NOT_FOUND = "380";
  public static IMAGES_NOT_FOUND = "381";
  public static IMAGE_NOT_FOUND_INFO = "382";
  public static CANNOT_DELETE_IMAGE_WITH_USAGES = "383";

  public static SQL_EXCEPTION = "390";
  public static INVALID_INPUT_DATA = "400";
  public static ENTITY_NOT_FOUND = "401";
  public static INVALID_FIREBASE_TOKEN = "402";
  public static UPDATE_FAILED = "403";
  public static INVALID_TOKEN = "404";
  public static PERMISSION_ERROR = "406";
  public static DUPLICATE_AUTHENTICATION = "407";
  public static EXPIRED_TOKEN = "412";
  public static EXPIRED_FIREBASE_TOKEN = "413";
  public static FIREBASE_NOT_CREATED = "415";
  public static DATABASE_CONNECTION_ERROR = "415";
  public static SECURITY_EXCEPTION = "415";
  public static UNHANDLED_EXCEPTION = "415";

  public static SLIDER_NOT_FOUND = "450";
  public static SLIDER_ITEM_NOT_FOUND = "451";

  public static GENERIC_ERROR: string = "500";

  public static PAYMENT_NOT_FOUND = "790";

  public static PAYMENT_EXCEPTION = "800";
  public static PAYMENT_NETWORK_EXCEPTION = "801";
  public static PAYMENT_REQUEST_EXCEPTION = "802";
  public static PAYMENT_CARD_EXCEPTION = "803";
  public static PAYMENT_EXPIRED_CARD_EXCEPTION = "804";
  public static PAYMENT_INCORRECT_CVC_CARD_EXCEPTION = "805";
  public static PAYMENT_DECLINED_CARD_EXCEPTION = "806";
  public static PAYMENT_PROCESING_ERROR_CARD_EXCEPTION = "807";
  public static PAYMENT_AUTH_EXCEPTION = "808";
  public static PAYMENT_DATABASE_EXCEPTION = "809";
  public static PAYMENT_ALREADY_PAID_EXCEPTION = "810";
  public static PAYMENT_EXPIRED_EXCEPTION = "811";
  public static PAYMENT_REQUIRES_ACTION = "812";

  public static BLOCKED_USER = "900";

  // Firebaes Errores
  public static FB_USER_REGISTER: string = "auth/email-already-in-use";
  public static FB_WRONG_PASS: string = "auth/wrong-password";
  public static FB_INVALID_PASS: string = "auth/weak-password";
  public static FB_USER_NOT_FOUND: string = "auth/user-not-found";

  public static getStringError(errorResponse): String {
    const code = this.getCode(errorResponse);
    switch (code.toString()) {
      case this.FB_USER_REGISTER:
        return "El usuario ya está registrado. Prueba con otro e-mail" + this.getCodes(errorResponse);
      case this.FB_INVALID_PASS:
        return "La contraseña es demasiado débil" + this.getCodes(errorResponse);
      case this.INVALID_FIREBASE_TOKEN:
        return "Firebase token inválido" + this.getCodes(errorResponse);
      case this.INVALID_TOKEN:
        return "No conseguimos encontrar el usuario" + this.getCodes(errorResponse);
      case this.FB_USER_NOT_FOUND:
      case this.FB_WRONG_PASS:
        return "El e-mail o la contraseña son erróneos" + this.getCodes(errorResponse);
      case this.USER_PENDING :
        return "Tu usuario está pendiende de confirmación. Por favor, espera a que un adminsitrador confirme tu acceso al sistema" + this.getCodes(errorResponse);
      case this.USER_DENIED:
        return "Tu usuario ha sido bloeado. Por favor, habla con el administrador del sistema" + this.getCodes(errorResponse);
      case this.PRODUCT_NOT_FOUND:
        return "No se ha podido encontrar el producto" + this.getCodes(errorResponse);
      case this.VARIANT_NOT_FOUND:
        return "No se ha podido encontrar el producto" + this.getCodes(errorResponse);
      case this.DELIVERY_ZONE_NOT_FOUND:
        return "Lo sentimos, no tramitamos pedidos online para tu dirección. Escríbenos a este correo madaimi@uoc.edu para realizar el pedido manualmente"
          + this.getCodes(errorResponse);
      case this.PRODUCT_FILE_NOT_FOUND:
        return "No se ha encontrado el archivo" + this.getCodes(errorResponse);
      case this.PRODUCT_FILE_NOT_FOUND_ON_SERVER:
        return "No se ha encontrado el archivo en el servidor" + this.getCodes(errorResponse);
      case this.GENERIC_ERROR:
      default:
        return "Ha ocurrido un error inesperado" + this.getCodes(errorResponse);
    }
  }

  public static getCodes(errorResponse): string {
    const code = this.getCode(errorResponse);

    if (!isNumber(code)) {
      return " (Código de error: " + code + ")";
    } else {
      return "";
    }
  }

  public static getWarnings(error) {
    if (error != null && error.error != null && error.error.warning != null)
      return error.error.warning;
  }

  public static getCode(errorResponse): string {
    if (errorResponse != null && errorResponse.code != null) return errorResponse.code;
    if (errorResponse != null && errorResponse.error != null && errorResponse.error.error != null && errorResponse.error.error.code != null) {
      return '' + errorResponse.error.error.code;
    } else {
      return null;
    }
  }

  public static getSeverity(errorResponse): string {
    if (errorResponse != null && errorResponse.severity != null) return errorResponse.severity;
    if (errorResponse != null && errorResponse.error != null && errorResponse.error.error != null && errorResponse.error.error.severity != null) {
      return errorResponse.error.error.severity;
    } else {
      return null;
    }
  }


  /**
   * Check if error matches any errors on a code list.
   * @param error from API
   * @param codes will be checked
   */
  public static isEquals(error, codes: string[]) {
    for (const code of codes) {
      if (this.getCode(error) == code) return true;
    }
    return false;
  }
}
