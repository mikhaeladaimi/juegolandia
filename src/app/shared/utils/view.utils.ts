import {Injectable} from "@angular/core";

@Injectable()
export class ViewUtils {

  public getLocalityProvince(locality: String, province: String): String {
    if (locality != null && province != null) {
      return locality + ", " + province;
    } else if (locality == null && province != null) {
      return province;
    } else if (locality != null && province == null) {
      return locality;
    } else {
      return "";
    }
  }

  public static getPrice(cents): string {
    let result = 0;
    if (cents > 1) {
      result = cents / 100;
    }
    return result.toFixed(2) + " €";
  }

  public getTime(time) {
    const date: Date = new Date(time);
    return date.getHours() + ":" + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
  }

  public getDate(time) {
    const date: Date = new Date(time);
    const dia = date.getDate().toString();
    const mes = (date.getMonth() + 1).toString();
    const anio = date.getFullYear().toString();
    return dia + "/" + mes + "/" + anio;
  }

  public compareDateWithCurrent(firstDate) {
    const dateFirstDate: Date = new Date(firstDate);
    const currentDay: Date = new Date();
    return dateFirstDate > currentDay;
  }

  public static getFirstWord(value: string): string {
    if (!value) {
      return '';
    }
    return value.trim().split(' ')[0];
  }


}
