import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
import { ModalComponent } from '../../components/modal/modal.component';
import {StripeCardComponent} from "../../components/modal-stripe-card/stripe-card.component";

@Injectable()
export class ModalUtils {

  DEF_ACCEPT_TEXT = "aceptar";
  DEF_CANCEL_TEXT = "cancelar";

  constructor(private modalService: NgbModal) {
  }

  /**
   * This method create a modal in the screen that isn't cancelable
   * @param title Modal title
   * @param content Modal HTML Content
   * @param acceptText Optional parameter setting the accept button text
   * @param size Optional parameter setting the size of the modal
   */
  openModal(title, content, acceptText?: string, size?: 'sm' | 'lg' | null) {
    let modalOptions: NgbModalOptions = {};
    switch (size) {
      case 'lg':
        modalOptions = { size: 'lg', centered: true  };
        break;
      case 'sm':
        modalOptions = { size: 'sm', centered: true  };
        break;
      default:
        modalOptions = { size: 'sm', centered: true  };
        break;
    }
    const activeModal = this.modalService.open(ModalComponent, modalOptions);
    activeModal.componentInstance.modalHeader = title;
    activeModal.componentInstance.modalContent = content;
    activeModal.componentInstance.acceptText = acceptText != null && acceptText.length > 0 ? acceptText : this.DEF_ACCEPT_TEXT;
    activeModal.componentInstance.cancel = false;
    return activeModal.result;

  }


  /**
  * This method create a modal in the screen that is cancelable
  * @param title Modal title
  * @param content Modal HTML Content
  * @param acceptText Optional parameter setting the accept button text
  * @param cancelText Optional parameter setting the cancel button text
  * @param size Optional parameter setting the size of the modal
  */
  openCancelableModal(title, content, acceptText?: string, cancelText?: string, size?: 'sm' | 'lg' | null) {
    let modalOptions: NgbModalOptions = {};
    switch (size) {
      case 'lg':
        modalOptions = { size: 'lg', centered: true  };
        break;
      case 'sm':
        modalOptions = { size: 'sm', centered: true  };
        break;
      default:
        modalOptions = { size: 'sm', centered: true  };
        break;
    }
    const activeModal = this.modalService.open(ModalComponent, modalOptions);
    activeModal.componentInstance.modalHeader = title;
    activeModal.componentInstance.modalContent = content;
    activeModal.componentInstance.acceptText = acceptText != null && acceptText.length > 0 ? acceptText : this.DEF_ACCEPT_TEXT;
    activeModal.componentInstance.cancel = true;
    activeModal.componentInstance.cancelText = cancelText != null && cancelText.length > 0 ? cancelText : this.DEF_CANCEL_TEXT;
    return activeModal.result;
  }

  /**
  * This method create a modal in the screen that is cancelable and danger
  * @param title Modal title
  * @param content Modal HTML Content
  * @param acceptText Optional parameter setting the accept button text
  * @param cancelText Optional parameter setting the cancel button text
  * @param size Optional parameter setting the size of the modal
  */
  openDangerCancelableModal(title, content, acceptText?: string, cancelText?: string, size?: 'sm' | 'lg' | null) {
    let modalOptions: NgbModalOptions = {};
    switch (size) {
      case 'lg':
        modalOptions = { size: 'lg', centered: true  };
        break;
      case 'sm':
        modalOptions = { size: 'sm', centered: true  };
        break;
      default:
        modalOptions = { size: 'sm', centered: true  };
        break;
    }
    const activeModal = this.modalService.open(ModalComponent, modalOptions);
    activeModal.componentInstance.modalHeader = title;
    activeModal.componentInstance.modalContent = content;
    activeModal.componentInstance.classNg = "btn btn-md btn-danger";
    activeModal.componentInstance.acceptText = acceptText != null && acceptText.length > 0 ? acceptText : this.DEF_ACCEPT_TEXT;
    activeModal.componentInstance.cancel = true;
    activeModal.componentInstance.swipe = true;
    activeModal.componentInstance.cancelText = cancelText != null && cancelText.length > 0 ? cancelText : this.DEF_CANCEL_TEXT;
    return activeModal.result;
  }

  openDangerCancelableModalRed(title, content, acceptText?: string, cancelText?: string, size?: 'sm' | 'lg' | null) {
    let modalOptions: NgbModalOptions = {};
    switch (size) {
      case 'lg':
        modalOptions = { size: 'lg', centered: true };
        break;
      case 'sm':
        modalOptions = { size: 'sm', centered: true  };
        break;
      default:
        modalOptions = { size: 'sm', centered: true  };
        break;
    }
    const activeModal = this.modalService.open(ModalComponent, modalOptions);
    activeModal.componentInstance.modalHeader = title;
    activeModal.componentInstance.modalContent = content;
    activeModal.componentInstance.classNg = "btn btn-md btn-danger";
    activeModal.componentInstance.acceptText = acceptText != null && acceptText.length > 0 ? acceptText : this.DEF_ACCEPT_TEXT;
    activeModal.componentInstance.cancel = true;
    activeModal.componentInstance.cancelText = cancelText != null && cancelText.length > 0 ? cancelText : this.DEF_CANCEL_TEXT;
    return activeModal.result;
  }

  openLoadingModal() {
    const activeModal = this.modalService.open(ModalComponent, { size: 'lg', backdrop: 'static', centered: true });
    activeModal.componentInstance.modalHeader = "<span class='load-title'>Estamos trabajando en ello </span>";
    activeModal.componentInstance.loading = true;
    activeModal.componentInstance.acceptText = "Cancelar";
    activeModal.componentInstance.cancel = false;
    return activeModal.result;
  }

  openErrorModal(body?) {
    const activeModal = this.modalService.open(ModalComponent, { size: 'sm', backdrop: 'static', centered: true });
    activeModal.componentInstance.modalHeader = "<b'>Error encontrado </b>";
    activeModal.componentInstance.modalContent = "Hemos encontrado un error y no podemos atenderte ahora mismo. Intentaremos solucionarlo pronto.";
    activeModal.componentInstance.classNg = "btn btn-md btn-danger";
    activeModal.componentInstance.acceptText = "Aceptar";
    activeModal.componentInstance.cancel = false;

    if (body) {
      activeModal.componentInstance.modalContent = body;
    }

    return activeModal.result;
  }

  openCreditCardModal(price, userData?) {
    const activeModal = this.modalService.open(StripeCardComponent, {backdrop: 'static', centered: true, keyboard: false });
    activeModal.componentInstance.price = price;
    activeModal.componentInstance.user = userData;
    return activeModal.result;
  }
}
