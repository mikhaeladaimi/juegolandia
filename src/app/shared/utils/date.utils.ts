import { Injectable } from "@angular/core";

@Injectable()
export class DateUtils {

    public getTime(time) {
        const date: Date = new Date(time);
        return date.getHours() + ":" + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    }

    public getDate(time) {
        const date: Date = new Date(time);
        const dia = date.getDate().toString();
        const mes = (date.getMonth() + 1).toString();
        const anio = date.getFullYear().toString();
        return dia + "/" + mes + "/" + anio;
    }

    public compareDateWithCurrent(firstDate) {
        const dateFirstDate: Date = new Date(firstDate);
        const currentDay: Date = new Date();
        return dateFirstDate > currentDay;
    }

}
