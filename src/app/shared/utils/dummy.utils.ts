import {Injectable} from "@angular/core";
import {Product} from "../../models/product/Product";
import {FeaturedProductList} from "../../models/product/FeaturedProductList";
import {ImageApp} from "../../models/image/ImageApp";
import {CarouselItem} from "../../models/carousel/CarouselItem";
import {GeneralLink} from "../../models/link/GeneralLink";
import {InfoObject} from "../../models/info-object/InfoObject";

@Injectable()
export class DummyUtils {

  public static generateProdutFeatured(originalProducts: Product[]) {
    const array = [1, 2, 3];
    const productList = [];
    array.forEach((value, i) => {
      if (originalProducts[i]) productList.push(originalProducts[i]);
    });
    return new FeaturedProductList("Productos destacados", productList, 4);
  }

  public getDate(time) {
    const date: Date = new Date(time);
    const dia = date.getDate().toString();
    const mes = (date.getMonth() + 1).toString();
    const anio = date.getFullYear().toString();
    return dia + "/" + mes + "/" + anio;
  }

  public compareDateWithCurrent(firstDate) {
    const dateFirstDate: Date = new Date(firstDate);
    const currentDay: Date = new Date();
    return dateFirstDate > currentDay;
  }

  static randomCarouselItems(): CarouselItem {
    const carouselItem = new CarouselItem();
    const titles = [
      "Excursión de navidad",
      "Póster tamaño A4",
      "Póster Tamaño A5",
      "Cuadro al óleo",
      "Camiseta personalizada",
      "Alfombrilla de ratón"];
    carouselItem.title = titles[Math.floor(Math.random() * titles.length)];
    carouselItem.price = Math.random() * 901 + 100;
    carouselItem.isNew = Math.random() > 0.5;
    carouselItem.image = new ImageApp(carouselItem.title, new Date().toDateString(), "https://picsum.photos/1080/1080?" + String.fromCharCode(Math.floor(Math.random() * 25 + 65)));
    return carouselItem;
  }

  static randomButton(): GeneralLink {
    return new GeneralLink().deserialize({
      title: "Añadir",
      icon: "fa-shopping-cart",
      link: "#",
      target: "_blank"
    });
  }

  static randomBannerButton(): GeneralLink {
    return new GeneralLink("fa fa-envelope", "", "//contacto", "Contactar", "_blank");
  }

  static randomBannerInfo(bannerButton): InfoObject {
    return new InfoObject("¿Estás buscando algún juego descatalogado?", "", "Escríbemos y te ayudaremos a encontrarlo", bannerButton);
  }

}
