import {Injectable} from "@angular/core";

/**
 * All SLUGS Should be indicated in this class as readonly static variables
 * All methods related to slug are here to reuse code
 */

@Injectable()
export class SlugsUtils {

  public static readonly HOME = '';
  public static readonly ACCESS = "acceso";
  public static readonly REMEMBER_ACCESS = "acceso/recordar";
  public static readonly CONTACT = "contacto";
  public static readonly COMPANY = "empresa";
  public static readonly SHOP = "catalogo";
  public static readonly SHOP_FILTER = SlugsUtils.SHOP + '/tienda';
  public static readonly PRODUCT_DETAIL = SlugsUtils.SHOP + "/detalle";
  public static readonly CART_DETAIL = "carrito";
  public static readonly CHECKOUT = "checkout";
  public static readonly MY_PROFILE = "mi-cuenta";
  public static readonly ORDER = "pedido";
  public static readonly LEGAL = "legal";
  public static readonly COOKIES = SlugsUtils.LEGAL + "/cookies";
  public static readonly PRIVACY = SlugsUtils.LEGAL + "/privacidad";
  public static readonly NOTICE = SlugsUtils.LEGAL + "/aviso";
  public static readonly ERROR = "error";
  public static readonly ERROR_404 = "error-404";

  public static getAbsoluteRoute(route) {
    return '/' + route;
  }

}
