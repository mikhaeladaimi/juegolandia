import {MatPaginatorIntl} from "@angular/material";

export class MatPaginatorIntlEsp extends MatPaginatorIntl {
  nextPageLabel = 'siguiente';
  previousPageLabel = 'anterior';

  getRangeLabel = function (page, pageSize, length) {
    if (length === 0 || pageSize === 0) {
      return '0 - ' + length;
    }
    const totalPages = Math.ceil(length / pageSize);
    return (page + 1) + ' / ' + totalPages;
  };
}
