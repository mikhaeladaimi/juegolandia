import {Injectable} from "@angular/core";
import {Subscription} from "rxjs";

@Injectable()
export class SyncUtils {

  public static unsubscribe(subscription: Subscription) {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
