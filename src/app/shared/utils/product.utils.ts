import {Injectable} from "@angular/core";
import {Product} from "../../models/product/Product";
import {TabsData} from "../../models/tabs-data/TabsData";

@Injectable()
export class ProductUtils {

  public static getTabData(product: Product): TabsData[] {
    const array: TabsData[] = [];
    array.push(new TabsData("Descripción", product.longDescription, "Sin descripción"));
    array.push(new TabsData("Información adicional", product.additionalInformation, "Sin información"));
    return array;
  }

}
