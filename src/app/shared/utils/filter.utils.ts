import {Injectable} from "@angular/core";
import {AttributeValue} from "../../models/attribute/AttributeValue";

@Injectable()
export class FilterUtils {

  public static getSelectedElements(items: any[]) {
    const filteredElements = [];
    for (const item of items) {
      if (item.isSelected) {
        filteredElements.push(item.slug);
      }
      if (item.children) {
        for (const child of item.children) {
          if (child.isSelected)
            filteredElements.push(child.slug);
        }
      }
    }
    return filteredElements;
  }

  public static mergeArray(items: any [], itemsSelected: any[]) {
    if (!itemsSelected) return items;
    for (const itemSelected of itemsSelected) {
      items.filter(item => {
        if (item.slug == itemSelected) item.isSelected = true;
        if (item.children) {
          for (const child of item.children) {
            if (child.slug == itemSelected) child.isSelected = true;
          }
        }
      });
    }
    return items;
  }

  public static mergeArrayList(items: any [], itemsSelected: any[]) {
    for (const itemSelected of itemsSelected) {
      items.filter(item => {
        if (item.id == itemSelected.id) {
          if (item.values) {
            for (const value of item.values) {
              if (value.value == itemSelected.value) value.isSelected = true;
            }
          }
        }
      });
    }
    return items;
  }

  public static getAttributeFromString(text: string) {
    const attr = new AttributeValue();
    const textSplitted = text.split(",");
    attr.id = +textSplitted[0];
    attr.value = textSplitted[1];
    return attr;
  }

  public static getStringFromAttribute(attr: AttributeValue) {
    return attr.id + "," + attr.value;
  }

  public static getStringArrayFromAttributes(attrs: AttributeValue[]) {
    const attrStrings = [];
    for (const attr of attrs) {
      attrStrings.push(FilterUtils.getStringFromAttribute(attr));
    }
    return attrStrings;
  }

  public static getParamsCategories(catParam) {
    const list = [];
    if (catParam instanceof Array) return catParam;
    list.push(catParam);
    return list;
  }

  public static getParamsAttributes(attrParams) {
    const attrList = [];
    if (attrParams instanceof Array) {
      for (const param of attrParams) {
        attrList.push(FilterUtils.getAttributeFromString(param));
      }
    } else {
      attrList.push(FilterUtils.getAttributeFromString(attrParams));
    }
    return attrList;
  }
}
