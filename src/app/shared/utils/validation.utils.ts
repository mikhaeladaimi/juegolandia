import {Injectable} from "@angular/core";
import {ValidationErrors} from "@angular/forms";

@Injectable()
export class ValidationUtils {

  /**
   * Validation errors of forms
   */

  // ERRORS
  public static REQUIRED: string = 'required';
  public static PATTERN: string = 'pattern';
  public static MINLENGTH: string = 'minlength';
  public static MAXLENGTH: string = 'maxlength';

  // VALUES
  public static REQUIRED_LENGTH: string = 'requiredLength';

  isDefined(errors: ValidationErrors, error: string): boolean {
    return errors[error] != undefined;
  }

  getErrorValue(errors: ValidationErrors, error: string, value: string): any {
    return errors[error][value];
  }

  getErrorString(errors: ValidationErrors): string {
    let error: string = "";
    if (this.isDefined(errors, ValidationUtils.REQUIRED)) {
      error += "Este campo es obligatorio.";
    }
    if (this.isDefined(errors, ValidationUtils.PATTERN)) {
      error += "Este campo no es del tipo solicitado.";
    }
    if (this.isDefined(errors, ValidationUtils.MINLENGTH)) {
      error += "Este campo no contiene la longitud mínima necesaria (" +
        this.getErrorValue(errors, ValidationUtils.MINLENGTH, ValidationUtils.REQUIRED_LENGTH) + ").";
    }
    if (this.isDefined(errors, ValidationUtils.MAXLENGTH)) {
      error += "Este campo excede la longitud máxima permitida (" +
        this.getErrorValue(errors, ValidationUtils.MAXLENGTH, ValidationUtils.REQUIRED_LENGTH) + ").";
    }
    return error;
  }


}
