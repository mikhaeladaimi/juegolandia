import {Injectable} from "@angular/core";
import {Attribute} from "../../models/attribute/Attribute";
import {AttributeValue} from "../../models/attribute/AttributeValue";

@Injectable()
export class AttributesUtils {

  /**This method transform an attribute list to an attributeView list
   *
   * @param attributes Attributes list
   *
   * @return attributeView list
   */
  public static transformToAttributesView(attributes: Attribute[]): Attribute[] {
    const attributesView: Attribute[] = [];
    attributes.filter(attr => {
      attributesView.push(new Attribute().deserialize(attr));
    });

    return attributesView;
  }

  /**This method set selected the attributes in attributesView list
   *
   * @param attributes AttributeView list
   * @param productAttributes Attributes list
   *
   * @return attributeView list
   */
  public static getSelectedAttributesView(attributes: Attribute[], productAttributes: Attribute[]): Attribute[] {
    attributes.filter(attr => {
      let selected = false;
      attr.values.filter(attrValue => {
        productAttributes.filter(productAttr => {
          productAttr.values.filter(productAttrValues => {
            if (!attrValue.isSelected && !selected) {
              attrValue.isSelected = (attrValue.id == productAttrValues.id && attrValue.attributeId == productAttrValues.attributeId);
              selected = true;
            }
          });
        });

      });
    });

    return attributes;
  }


  /**This method transform an attribute list to an attributesId list
   *
   * @param attributes Attributes list
   *
   * @return attributesIds list
   */
  public static transformToAttributeIds(attributes): number[] {
    if (!attributes) return null;
    if (!AttributesUtils.isAttributesArray(attributes)) return attributes;
    const attributeValueIds: number[] = [];
    attributes.map(attr => {
      if (attr.values && attr.values.length > 0) attributeValueIds.push(attr.values[0].id);
    });
    return attributeValueIds;
  }


  static compareAttributes(values1: AttributeValue[], values2: AttributeValue[]): boolean {
    let equals = false;
    if (values1.length == values2.length) {
      equals = true;
      for (let i = 0; i < values1.length && equals; i++) {
        equals = values1[i].id == values2[i].id;
      }
    }
    return equals;
  }

  private static isAttributesArray(array): boolean {
    if (!array && array.length < 1) return false;
    return array[0].values != null;

  }

}
