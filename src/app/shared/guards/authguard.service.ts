import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserService} from "../services/user.service";
import {AuthService} from "../services/auth.service";
import {UserCacheService} from "../cache/user-cache.service";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private userCacheService: UserCacheService,
              private userService: UserService,
              private authService: AuthService) {
  }

  canActivate(): boolean {
    const user = this.userCacheService.getUserFromCache(this.userService.user);
    if (!user && !this.authService.isUserLogged()) {
      this.router.navigateByUrl(EndpointsUtils.getAccess());
      return false;
    }
    return true;
  }
}

