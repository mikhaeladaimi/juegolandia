import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserService} from "../services/user.service";
import {AuthService} from "../services/auth.service";
import {UserCacheService} from "../cache/user-cache.service";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class AccessGuard implements CanActivate {
  constructor(private router: Router, private userService: UserService,
              private authService: AuthService, private userCacheService: UserCacheService) {
  }

  canActivate(): boolean {
    const user = this.userCacheService.getUserFromCache(this.userService.user);
    if (user || this.authService.isUserLogged()) {
     this.router.navigateByUrl(EndpointsUtils.getMyAccount());
      return false;
    }
    return true;
  }
}
