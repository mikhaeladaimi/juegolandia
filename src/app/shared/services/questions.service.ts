import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {Question} from "../../models/question/Question";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class QuestionsService {

  constructor(private httpClient: HttpClient) {
  }

  getQuestions(): Promise<Question[]> {
    return this.httpClient.get(EndpointsUtils.getQuestions())
      .pipe(map(response => response['response']))
      .toPromise();
  }
}
