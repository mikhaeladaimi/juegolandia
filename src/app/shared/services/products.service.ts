import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { NGXLogger } from 'ngx-logger';
import { map } from 'rxjs/operators';

import { Product } from '../../models/product/Product';
import { ProductSearchRequest } from '../../models/product/ProductSearchRequest';
import { MainService } from './main.service';

@Injectable()
export class ProductsService extends MainService {

  constructor(logger: NGXLogger, private firestore: AngularFirestore) {
    super(logger);
  }

  /**This method return a product list
   * @param productSearch ProductSearch objectList
   * @return a Product list
   */
  getProducts(productSearch: ProductSearchRequest): Promise<Product[]> {
    return this.firestore.collection('products').get()
      .pipe(map(response => {
        const products = [];
        response.forEach((productData: any) => {
          products.push(new Product().deserialize(productData.data()));
        });
        return products;
      })).toPromise();
  }

  /**This method return a featured product list
   * @param forceRefresh boolean to get data from service(true) or from cache(undefined || null)
   * @param productSearch ProductSearch objectList
   * @return a featured Product list
   */
  getFeaturedProducts(productSearch: ProductSearchRequest, forceRefresh?: boolean): Promise<Product[]> {
    return this.firestore.collection('products',
        ref => ref.where("featured", "==", true)).get()
      .pipe(map(response => {
        const products = [];
        response.forEach((productData: any) => {
          products.push(new Product().deserialize(productData.data()));
        });
        return products;
      })).toPromise();
  }

  /**This method return a product
   * @param slug Product slug
   * @return a Product
   */
  getProduct(slug): Promise<Product> {
    return this.firestore.collection('products', ref => ref.where("slug", "==", slug))
      .get()
      .pipe(map(response => {
        if (!response || response.size < 1) return null;
        return new Product().deserialize(response.docs[0].data());

      })).toPromise();
  }

}
