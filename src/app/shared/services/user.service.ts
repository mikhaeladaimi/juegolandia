import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import {Injectable} from '@angular/core';
import {NGXLogger} from 'ngx-logger';

import {MainService} from './main.service';
import {User} from "../../models/user/User";
import {ReplaySubject} from "rxjs";
import {UserCacheService} from "../cache/user-cache.service";
import {map} from "rxjs/operators";
import {AngularFirestore} from "@angular/fire/firestore";

@Injectable()
export class UserService extends MainService {
  user: User;
  private userObservable: ReplaySubject<User> = new ReplaySubject<User>();

  constructor(logger: NGXLogger, private firestore: AngularFirestore, private userCacheService: UserCacheService) {
    super(logger);
    if (!this.user) userCacheService.getUserFromCache();
  }

  /**
   * return userObservable. userObservable is complete when user value is requested to API
   */
  getObservableUser() {
    return this.userObservable;
  }

  /**
   * This method returns current user
   * @param forceRefresh If is true force to call the service
   * @return current user inside a promise
   */
  getUser(token): Promise<User> {
    this.logger.debug('User request', token);

    return this.firestore.collection('users').doc(token)
      .get()
      .pipe(map(response => {
        this.user = new User().deserialize(response.data());
        this.userCacheService.saveUserOnCache(this.user);
        this.userObservable.next(this.user);
        this.userObservable.complete();
        return this.user;
      })).toPromise();

  }

  clearUser() {
    this.user = null;
    this.userCacheService.removeItem("ngx-user");
    this.userObservable.unsubscribe();
    this.userObservable = new ReplaySubject();
  }

  updateUser(user: User, token) {
    return this.firestore.collection('users').doc(token)
      .set({
        name: user.name
      }, {merge: true});
  }

  updateUserOnCache(user) {
    this.user.deserialize(user);
    this.userCacheService.saveUserOnCache(this.user);
    this.userObservable.next(this.user);
  }

  createUser(email, token) {
    return this.firestore.collection('users').doc(token)
      .set({
        email: email
      });
  }
}
