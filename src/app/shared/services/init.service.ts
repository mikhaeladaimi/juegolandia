import {Injectable} from '@angular/core';
import {MainService} from "./main.service";
import {NGXLogger} from "ngx-logger";
import 'rxjs/add/observable/forkJoin';
import {AuthService} from "./auth.service";

@Injectable()
export class InitService extends MainService {

  constructor(
    logger: NGXLogger,
    private authService: AuthService) {
    super(logger);
  }

  startUp() {
    return new Promise((solve) => {
      this.authService.getInitialState().subscribe(() => solve());
    });
  }


}
