import {Injectable} from '@angular/core';
import {NGXLogger} from "ngx-logger";

@Injectable()
export class MainService {

  constructor(public logger: NGXLogger) {
  }

  /** GETTERS && SETTERS */
  getLogger(): NGXLogger {
    return this.logger;
  }
}
