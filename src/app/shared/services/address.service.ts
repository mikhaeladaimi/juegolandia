import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MainService} from "./main.service";
import {NGXLogger} from "ngx-logger";
import {Country} from "../../models/country/Country";
import {map} from "rxjs/operators";
import {Province} from "../../models/address/province/Province";
import {Locality} from "../../models/address/locality/Locality";
import {PostalCode} from "../../models/address/postal-code/PostalCode";
import {NameRequest} from "../../models/name/NameRequest";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class AddressService extends MainService {

  constructor(logger: NGXLogger, private httpClient: HttpClient) {
    super(logger);
  }

  getCountries(): Promise<Country[]> {
    return this.httpClient.get(EndpointsUtils.getCoutries())
      .pipe(map(response => response["response"]))
      .toPromise();
  }

  getProvinces(countryId: number): Promise<Province[]> {
    return this.httpClient.get(EndpointsUtils.getProvinces(countryId))
      .pipe(map(response => response["response"]))
      .toPromise();
  }

  getLocalitiesByName(provinceId: number, name: string): Promise<Locality[]> {
    return this.httpClient.post(EndpointsUtils.getLocalities(provinceId), new NameRequest().deserialize({name: name}))
      .pipe(map(response => response["response"]))
      .toPromise();
  }

  getLocalities(provinceId: number): Promise<Locality[]> {
    return this.httpClient.get(EndpointsUtils.getLocalities(provinceId))
      .pipe(map(response => response["response"]))
      .toPromise();
  }

  getPostalCodes(localityId: number): Promise<PostalCode[]> {
    return this.httpClient.get(EndpointsUtils.getPostalCodes(localityId))
      .pipe(map(response => response["response"]))
      .toPromise();
  }
}
