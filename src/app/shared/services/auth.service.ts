import {NGXLogger} from 'ngx-logger';
import {Subscription} from 'rxjs/Subscription';
import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import 'rxjs/add/operator/retryWhen';
import {from} from 'rxjs';
import {map} from 'rxjs/operators';
import {MainService} from './main.service';
import {UserService} from './user.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {CartCacheService} from "../cache/cart-cache.service";


@Injectable()
export class AuthService extends MainService {

  private subscription: Subscription;

  constructor(logger: NGXLogger, private fireAuth: AngularFireAuth,
              private userService: UserService, private cartUtils: CartCacheService) {
    super(logger);
    this.subscribeToAuth();
  }

  /**
   * Login user on firebase, and then, get the token
   * @param user's user
   * @param password's user
   * @return empty promise
   */
  login(user: string, password: string): Promise<any> {
    this.subscribeToAuth();
    return this.fireAuth.auth.signInWithEmailAndPassword(user, password);
  }


  /**
   * Create user on firebase and then, register it on our api
   * @param email mail
   * @param password pass
   * @param newsletter
   * @return empty promise
   */
  createUser(email, password, newsletter): Promise<any> {
    this.userService.clearUser();
    return this.fireAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  /**
   * Generate a subscription to firebase state to check if user is logged
   * @return user logged if there is one
   */
  subscribeToAuth() {
    if (this.subscription == null) {
      this.subscription = this.fireAuth.authState.subscribe(user => {
        if (user) {
          this.logger.debug('User logged on FB', user.email);
          this.userService.getUser(user.uid).catch(error => {
            this.logger.error(error);
            this.logout();
          });
        } else {
          this.userService.clearUser();
          this.logger.debug('NOT logged in FB');
        }
      });
    }
  }

  getInitialState() {
    return this.fireAuth.authState;
  }

  getFirebaseUid() {
    return this.fireAuth.auth.currentUser.uid;
  }


  /**
   * Sync method return user logged not anonymous
   * @return user logged if there is one
   */
  isUserLogged() {
    return this.fireAuth.auth && this.fireAuth.auth.currentUser && !this.fireAuth.auth.currentUser.isAnonymous;
  }

  /**
   * Request to reset an user's password
   * @param email where instruction will be sent
   */
  resetPassword(email: string): Promise<void> {
    return this.fireAuth.auth.sendPasswordResetEmail(email);
  }

  /**
   * Logout user from firebase and clean all data
   */
  logout(): Promise<any> {
    if (this.subscription) this.subscription.unsubscribe();
    this.subscription = null;
    this.userService.clearUser();
    this.cartUtils.clearCache();
    this.logger.debug("LOGOUT");
    return from(this.fireAuth.auth.signOut()).pipe(
      map(() => this.subscribeToAuth())
    ).toPromise();
  }

  /**
   * @param mode The mode that is sent in the email
   * @param actionCode Code Generated from google to validate the password reset
   * @param newPassword new Password to set
   *
   */
  updatePassword(mode: string, actionCode: string, newPassword: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (mode == "resetPassword") {
        // This checks the code is valid
        this.fireAuth.auth.verifyPasswordResetCode(actionCode).then(email => {
          this.fireAuth.auth.confirmPasswordReset(actionCode, newPassword).then(() => {
            this.fireAuth.auth.signInWithEmailAndPassword(email, newPassword);
            resolve();
          }).catch(() => reject("invalidPass"));
        }).catch(() => reject("invalidCode"));
      } else {
        reject("invalidMode");
      }
    });

  }

  /**
   * Update password on firebase
   * @param email used
   * @param passwordOld old pass
   * @param passwordNew new pass
   */
  changePassword(email, passwordOld, passwordNew): Promise<any> {
    return new Promise((resolve, reject) => {
      this.login(email, passwordOld).then(() => {
        this.fireAuth.auth.currentUser.updatePassword(passwordNew)
          .then(() => resolve())
          .catch(error => reject(error));
      }).catch(error => reject(error));
    });
  }

}



