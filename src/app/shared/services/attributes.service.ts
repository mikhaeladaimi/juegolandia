import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {MainService} from "./main.service";
import {NGXLogger} from "ngx-logger";
import {Attribute} from "../../models/attribute/Attribute";
import {EndpointsUtils} from "../endpoint.utils";
import {AttributeRequest} from "../../models/attribute/AttributeRequest";

@Injectable()
export class AttributesService extends MainService {

  constructor(logger: NGXLogger, private httpClient: HttpClient) {
    super(logger);
  }

  getAttributes(): Promise<Attribute[]> {
    return this.httpClient.get(EndpointsUtils.getAttributes())
      .pipe(map(response => {
        const attributes = [];
        for (const attr of response["response"]) {
          attributes.push(new Attribute().deserialize(attr));
        }
        return attributes;
      }))
      .toPromise();
  }

  getVariableProductAttributesWithSelected(id: number, attributes: Attribute[]): Promise<Attribute[]> {
    return this.httpClient.post(EndpointsUtils.getVariableProductAttributes(),
      new AttributeRequest().deserialize({attributes: attributes, id: id}))
      .pipe(map(response => {
        const attributes = [];
        for (const attr of response['response']['attributes']) {
          attributes.push(new Attribute().deserialize(attr));
        }
        return attributes;
      }))
      .toPromise();
  }
}
