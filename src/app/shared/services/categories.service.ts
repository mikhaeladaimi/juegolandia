import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { NGXLogger } from 'ngx-logger';
import { map } from 'rxjs/operators';

import { Category } from '../../models/category/Category';
import { Product } from '../../models/product/Product';
import { MainService } from './main.service';

@Injectable()
export class CategoriesService extends MainService {

  constructor(logger: NGXLogger, private firestore: AngularFirestore) {
    super(logger);
  }

  getCategories(): Promise<Category[]> {
    return this.firestore.collection('categories')
      .get()
      .pipe(map(response => {
        const categories = [];
        response.forEach((categoryData: any) => {
          categories.push(new Category().deserialize(categoryData.data()));
        });
        return categories;
      })).toPromise();
  }

  /** This method return a category
   * @param slug Product slug
   * @return a Product
   */
  getCategory(ref): Promise<Product> {
    return this.firestore.collection('categories').doc(ref)
      .get()
      .pipe(map(response => {
        return new Category().deserialize(response.data());

      })).toPromise();
  }
}
