import {Injectable} from '@angular/core';
import {MainService} from "./main.service";
import {NGXLogger} from "ngx-logger";
import {Payment} from "../../models/purchase/Payment";
import {AngularFirestore} from "@angular/fire/firestore";
import {AuthService} from "./auth.service";
import {UserOrdersService} from "./user-orders.service";

@Injectable()
export class OrderService extends MainService {

  constructor(logger: NGXLogger, private firestore: AngularFirestore, private authService: AuthService, private orderService: UserOrdersService) {
    super(logger);
  }

  purchase(order): Promise<Payment | any> {

    return new Promise((solve, failure) => {
      this.orderService.getUserOrders().then(orders => {
        order.id = 1;
        if (orders.length > 0) {
          order.id = orders.length + 1;
        }
        this.createOrder(order)
          .then(() => {
            solve(order.id);
          }).catch(error => failure(error));
      }).catch(error => failure(error));
    });

  }

  createOrder(order): Promise<any> {
    return this.firestore.collection('orders').doc(this.authService.getFirebaseUid())
      .collection("userOrders").doc(order.id.toString())
      .set(JSON.parse(JSON.stringify(order)));
  }

}
