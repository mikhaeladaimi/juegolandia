import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {MainService} from "./main.service";
import {NGXLogger} from "ngx-logger";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class OrdinationService extends MainService {

  constructor(logger: NGXLogger, private httpClient: HttpClient) {
    super(logger);
  }

  getOrdinationValues(): Promise<any> {
    return this.httpClient.get(EndpointsUtils.getOrdinationValues())
      .pipe(map(response => response['response']))
      .toPromise();
  }
}
