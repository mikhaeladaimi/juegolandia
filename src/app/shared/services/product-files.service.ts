import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {MainService} from "./main.service";
import {NGXLogger} from "ngx-logger";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class ProductFilesService extends MainService {

  constructor(logger: NGXLogger, private httpClient: HttpClient) {
    super(logger);
  }

  /**This method return a product file
   * @return a product file
   */
  downloadProductFile(id): Promise<any> {
    return this.httpClient.get(EndpointsUtils.downloadProductFile(id), {responseType: 'arraybuffer'})
      .pipe(map(response => {
        const blobPart: any = <any>response;
        return new Blob([blobPart], {type: 'application/pdf'});
      }))
      .toPromise();
  }

  /** Return the file assigned to that product
   * @param id Product id
   * @return product file info
   */
  getProductFile(id): Promise<any> {
    return this.httpClient.get(EndpointsUtils.getProductFile(id))
      .pipe(map(data => data['response']))
      .toPromise();
  }
}
