import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {NGXLogger} from "ngx-logger";
import {map} from "rxjs/operators";
import {THEME_CONFIG} from "../../@theme/theme-config";
import {Feed} from "../../models/feed/Feed";
import {FeedItem} from "../../models/feed/FeedItem";

@Injectable()
export class FeedService extends MainService {

  public static BLOG_URL = THEME_CONFIG.blog.blogUrl;

  feedContent: any;

  constructor(private httpClient: HttpClient, logger: NGXLogger) {
    super(logger);
  }

  getFeedContent(url: string, forceRefresh?: boolean): Promise<Feed> {
    if (forceRefresh || !this.feedContent) {
      return this.httpClient.get(url, {observe: 'response'})
        .pipe(map((response) => {
          const posts = (<any[]>response.body);
          const feed = new Feed();
          feed.maxItems = +response.headers.get('X-WP-Total');
          feed.maxPage = +response.headers.get('X-WP-TotalPages');

          for (const post of posts) {
            const item = new FeedItem().deserialize(post);
            feed.posts.push(item);
          }
          return feed;
        })).toPromise();
    } else
      return Promise.resolve(this.feedContent);
  }
}
