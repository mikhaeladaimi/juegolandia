import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {MainService} from "./main.service";
import {NGXLogger} from "ngx-logger";
import {DeliveryPrice} from "../../models/price/DeliveryPrice";
import {Address} from "../../models/address/Address";
import {EndpointsUtils} from "../endpoint.utils";
import {AddressRequest} from "../../models/address/AddressRequest";

@Injectable()
export class DeliveryZonesService extends MainService {

  deliveryZone: Address;

  constructor(logger: NGXLogger, private httpClient: HttpClient) {
    super(logger);
  }

  getDeliveryPrice(address): Promise<DeliveryPrice> {
    this.deliveryZone = address;

    return this.httpClient.post(EndpointsUtils.getDeliveryPrice(), new AddressRequest().deserialize(this.deliveryZone))
      .pipe(map(response => response["response"]))
      .toPromise();
  }

  getDeliveryZone() {
    return new Address().deserialize(this.deliveryZone);
  }
}
