import {Injectable} from '@angular/core';
import {MainService} from "./main.service";
import {NGXLogger} from "ngx-logger";
import {map} from "rxjs/operators";
import {AngularFirestore} from "@angular/fire/firestore";
import {AuthService} from "./auth.service";
import {Order} from "../../models/order/Order";

@Injectable()
export class UserOrdersService extends MainService {

  constructor(logger: NGXLogger, private firestore: AngularFirestore, private authService: AuthService) {
    super(logger);
  }

  getUserOrders(): Promise<any> {
    return this.firestore.collection('orders').doc(this.authService.getFirebaseUid()).collection("userOrders")
      .get()
      .pipe(map(response => {
        const userOrders = [];
        response.forEach((orderData: any) => {
          userOrders.push(new Order().deserialize(orderData.data()));
        });
        return userOrders;
      })).toPromise();

  }

  getUserOrder(id): Promise<any> {
    return this.firestore.collection('orders').doc(this.authService.getFirebaseUid())
      .collection("userOrders", ref => ref.where("id", "==", id))
      .get()
      .pipe(map(response => {
        if (!response || response.size < 1) return null;
        return new Order().deserialize(response.docs[0].data());
      })).toPromise();
  }
}
