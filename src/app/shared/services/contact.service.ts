import {Injectable} from '@angular/core';
import {MainService} from "./main.service";
import {NGXLogger} from "ngx-logger";
import {Contact} from "../../models/contact/Contact";

@Injectable()
export class ContactService extends MainService {

  constructor(logger: NGXLogger) {
    super(logger);
  }

  postContactForm(contact: Contact): Promise<any> {
    return new Promise(solve => solve());
  }
}
