import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ErrorResponseUtils} from '../utils/errorResponse.utils';
import {NGXLogger} from 'ngx-logger';
import {AuthService} from '../services/auth.service';
import {ModalUtils} from '../utils/modal.utils';
import {Router} from "@angular/router";
import {SlugsUtils} from "../utils/slugs.utils";


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private logger: NGXLogger,
              private modalService: ModalUtils, private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(error => {
      if (error.status === 400) {
        this.doLogout();
      } else if (error.status == 500) {
        this.displayModal(error);
      }
      return throwError(error);
    }));
  }

  private doLogout() {
    this.logger.debug("Not authorized. Logging out");
    this.authService.logout();
    this.router.navigateByUrl(SlugsUtils.getAbsoluteRoute(SlugsUtils.HOME));
  }

  /**
   * Display modal for not controled errors
   * @param error to handle
   */
  private displayModal(error: any) {
    this.modalService.openErrorModal(ErrorResponseUtils.getStringError(error));
  }

}
