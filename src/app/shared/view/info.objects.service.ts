import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

import { InfoObject } from '../../models/info-object/InfoObject';

@Injectable()
export class InfoObjectsService {


  constructor(private firestore: AngularFirestore) {
  }

  getInfoObjects(): Promise<InfoObject[]> {
    return this.firestore.collection('categories').get()
      .pipe(map(response => {
        const infoObjectList = [];
        response.forEach((categoryData: any) => {
          infoObjectList.push(new InfoObject().deserialize(categoryData.data()));
        });
        return infoObjectList;
      }))
      .toPromise();
  }
}
