import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class CompanyService {

  constructor(private httpClient: HttpClient) {
  }

  getCompanyIcons(): Promise<any> {
    return this.httpClient.get(EndpointsUtils.getCompanyIcons())
      .pipe(map(answer => answer))
      .toPromise();
  }
}
