import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class LegalService {

  constructor(private httpClient: HttpClient) {
  }

  getCookiesData(): Promise<any> {
    return this.httpClient.get(EndpointsUtils.getCookies())
      .pipe(map(answer => answer))
      .toPromise();
  }

  getDisclaimerData(): Promise<any> {
    return this.httpClient.get(EndpointsUtils.getDisclaimer())
      .pipe(map(answer => answer))
      .toPromise();
  }

  getTermsData(): Promise<any> {
    return this.httpClient.get(EndpointsUtils.getTerms())
      .pipe(map(answer => answer))
      .toPromise();
  }
}
