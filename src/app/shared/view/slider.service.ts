import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {Slide} from "../../models/slide/Slide";
import {MainService} from "../services/main.service";
import {NGXLogger} from "ngx-logger";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class SliderService extends MainService {

  constructor(logger: NGXLogger, private httpClient: HttpClient) {
    super(logger);
  }

  getSlideObjectsDummy(): Promise<Slide[]> {
    return this.httpClient.get(EndpointsUtils.getSlides())
      .pipe(map(answer => (answer as Array<any>).map(slide => new Slide().deserialize(slide))))
      .toPromise();
  }
}
