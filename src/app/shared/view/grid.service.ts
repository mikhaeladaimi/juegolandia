import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from "rxjs/operators";
import {Grid} from "../../models/grid/Grid";
import {EndpointsUtils} from "../endpoint.utils";

@Injectable()
export class GridService {

  constructor(private httpClient: HttpClient) {
  }

  getGridList(): Promise<Grid[]> {
    return this.httpClient.get(EndpointsUtils.getGridItems())
      .pipe(map(response => {
        const gridList: Grid[] = [];
        for (const grid of response['response']) {
          gridList.push(new Grid().deserialize(grid));
        }
        return gridList;
      }))
      .toPromise();
  }
}
