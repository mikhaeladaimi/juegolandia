import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {isPlatformBrowser} from "@angular/common";

@Injectable()
export class CacheService {

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  }

  setItem(itemIndex, item) {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem(itemIndex, item);
    }
  }

  getItem(key) {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem(key);
    }
    return null;
  }

  clear() {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.clear();
    }
  }

  removeItem(key) {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.removeItem(key);
    }
  }

}


