import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {User} from "../../models/user/User";
import {CacheService} from "./cache.service";

@Injectable()
export class UserCacheService extends CacheService {

  constructor(@Inject(PLATFORM_ID) platformId: Object) {
    super(platformId);
  }

  getUserFromCache(user?: User) {
    let userStorage = JSON.parse(this.getItem("ngx-user"));
    if (!userStorage) {
      if (user) {
        this.saveUserOnCache(user);
        userStorage = user;
      }
    }
    return userStorage ? new User().deserialize(userStorage) : null;
  }

  saveUserOnCache(user: User) {
    this.setItem("ngx-user", JSON.stringify(user));
  }
}


