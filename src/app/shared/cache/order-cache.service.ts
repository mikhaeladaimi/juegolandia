import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {CacheService} from "./cache.service";
import {EncryptionHelpers} from "../helpers/encryption.helpers";
import {Order} from "../../models/order/Order";

@Injectable()
export class OrderCacheService extends CacheService {

  orderRequest: Order;
  orderObservable: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(@Inject(PLATFORM_ID) platformId: Object, private encryptionHelper: EncryptionHelpers) {
    super(platformId);
  }

  getOrderRequest(): Order {
    if (!this.orderRequest) {
      const cartData = this.getItem("orderRequest");
      if (cartData) {
        this.orderRequest = <Order>JSON.parse((this.encryptionHelper.atob(cartData)));
      } else {
        this.orderRequest = null;
      }
    }
    return this.orderRequest;
  }

  saveOrderRequest() {
    this.setItem("orderRequest", this.encryptionHelper.btoa(this.orderRequest));
    this.orderObservable.next(this.orderRequest);
  }

  removeOrderRequest() {
    this.removeItem('orderRequest');
  }

  removePreorder() {
    this.orderRequest.preorderId = undefined;
    this.saveOrderRequest();
  }

  clearOrderRequest() {
    this.orderRequest = null;
  }

  setOrderRequest(orderRequest) {
    this.orderRequest = orderRequest;
    this.saveOrderRequest();
  }

  setOrderRequestAttribute(attribute: string, value: any) {
    this.getOrderRequest()[attribute] = value;
    this.saveOrderRequest();
  }

  removeStripeToken() {
    this.getOrderRequest().stripeToken = undefined;
    this.saveOrderRequest();
  }
}

