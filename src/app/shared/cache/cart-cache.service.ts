import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {Product} from "../../models/product/Product";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {OrderItem} from "../../models/order/OrderItem";
import {CacheService} from "./cache.service";
import {OrderCacheService} from "./order-cache.service";

@Injectable()
export class CartCacheService extends CacheService {

  orderItems: OrderItem[];
  cartObservable: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(@Inject(PLATFORM_ID) platformId: Object, private orderCacheService: OrderCacheService) {
    super(platformId);
    this.orderCacheService.orderObservable.subscribe(answer => this.cartObservable.next(answer));
  }

  getCartData() {
    if (!this.orderItems) {
      const cartData = this.getItem("cart");
      if (cartData) {
        this.orderItems = <OrderItem[]>JSON.parse(cartData);
      } else {
        this.orderItems = [];
      }
    }
    return this.orderItems;
  }

  getTotal() {
    let totalPrice = 0;
    if (this.orderItems) {
      this.orderItems.forEach(item => {
        if (item.product != null) totalPrice += item.product.price * item.quantity;
      });
    }
    return totalPrice;
  }

  countItems() {
    let total = 0;
    if (this.getCartData()) {
      this.orderItems.forEach(item => {
        total += item.quantity;
      });
    }
    return total;
  }

  saveCartData(orderItems?: OrderItem[]) {
    orderItems && (this.orderItems = orderItems);
    this.setItem("cart", JSON.stringify(this.orderItems));
    this.orderCacheService.clearOrderRequest();
    this.orderCacheService.removeOrderRequest();
    this.cartObservable.next(this.orderItems);
  }

  addProductToCart(product: Product, quantity: number) {
    if (quantity) {
      if (!this.orderItems || this.orderItems.length == 0) {
        this.orderItems = [new OrderItem().deserialize({ product: product, quantity: quantity })];
      } else {
        const items = this.orderItems.filter(item => item.product.slug == product.slug);
        if (items && items.length == 1) {
          items[0].quantity += quantity;
        } else {
          this.orderItems.push(new OrderItem().deserialize({ product: product.clone(), quantity: quantity }));
        }
      }
      this.saveCartData();
    }
  }


  deleteItemFromCart(orderItem: OrderItem): any {
    this.orderItems = this.orderItems.filter(item => !(item.product.slug == orderItem.product.slug));
    this.saveCartData();
  }

  clearCache() {
    this.clear();
    this.orderItems = [];
    this.cartObservable.next(this.orderItems);
  }
}

