import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {isPlatformBrowser} from "@angular/common";

@Injectable()
export class ScrollHelper {

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  }

  scroll(left, top) {
    if (isPlatformBrowser(this.platformId)) {
      window.scroll({left: left, top: top});
    }
  }

  scrollTo(left, top, behavior?) {
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo({left: left, top: top, behavior: behavior ? behavior : 'smooth'});
    }
  }

}
