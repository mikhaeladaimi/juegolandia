import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {isPlatformBrowser} from "@angular/common";

@Injectable()
export class EncryptionHelpers {

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  }

  atob(value) {
    if (isPlatformBrowser(this.platformId)) {
      return atob(value);
    }
  }

  btoa(value) {
    if (isPlatformBrowser(this.platformId)) {
      return btoa(JSON.stringify(value));
    }
  }

}


