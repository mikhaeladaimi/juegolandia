import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {isPlatformBrowser} from "@angular/common";

@Injectable()
export class LocationHelper {

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  }

  includes(text) {
    if (isPlatformBrowser(this.platformId)) {
      return window.location.origin.includes(text);
    } else {
      return false;
    }
  }

  getOrigin() {
    if (isPlatformBrowser(this.platformId)) {
      return window.location.origin;
    }
  }

  open(url, target) {
    if (isPlatformBrowser(this.platformId)) {
      window.open(url, target);
    }

  }

}
