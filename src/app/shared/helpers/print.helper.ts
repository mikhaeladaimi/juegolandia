import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {isPlatformBrowser} from "@angular/common";

@Injectable()
export class PrintHelper {

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  }

  print() {
    if (isPlatformBrowser(this.platformId)) {
      window.print();
    }
  }

}
