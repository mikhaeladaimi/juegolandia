/**
 * API endpoints
 */
import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";

@Injectable()
export class EndpointsUtils {

  private static readonly urlApp: string = environment.api.app;
  private static readonly urlPublic: string = environment.api.public;
  private static readonly urlAuth: string = environment.api.auth;

  constructor() {
  }

  /** Address **/
  private static readonly ADDRESSES = "addresses/";
  private static readonly COUNTRIES = "countries/";
  private static readonly PROVINCES = "provinces/";
  private static readonly LOCALITIES = "localities/";
  private static readonly POSTAL_CODES = "postal-codes/";

  static readonly ADDRESSES_ENDPOINT = EndpointsUtils.urlApp + EndpointsUtils.ADDRESSES;

  static getCoutries = () => EndpointsUtils.ADDRESSES_ENDPOINT + EndpointsUtils.COUNTRIES;
  static getProvinces = (id) => EndpointsUtils.ADDRESSES_ENDPOINT + EndpointsUtils.PROVINCES + id;
  static getLocalities = (id) => EndpointsUtils.ADDRESSES_ENDPOINT + EndpointsUtils.LOCALITIES + id;
  static getPostalCodes = (id) => EndpointsUtils.ADDRESSES_ENDPOINT + EndpointsUtils.POSTAL_CODES + id;

  /** Attributes **/
  private static readonly VARIABLE_ATTRIBUTES = "variable-attributes/";
  private static readonly VARIABLE_PRODUCT_V2 = "variable-product-v2/";

  static getAttributes = () => EndpointsUtils.urlApp + EndpointsUtils.VARIABLE_ATTRIBUTES;
  static getVariableProductAttributes = () => EndpointsUtils.getAttributes() + EndpointsUtils.VARIABLE_PRODUCT_V2;

  /** Auth **/
  private static readonly LOGIN = "login/";
  private static readonly AUTH = "auth/";
  private static readonly REGISTER = "register/";

  static getApiToken = () => EndpointsUtils.urlAuth + EndpointsUtils.LOGIN;
  static getPublicApiToken = () => EndpointsUtils.urlPublic + EndpointsUtils.AUTH;
  static getRegister = () => EndpointsUtils.urlAuth + EndpointsUtils.REGISTER;

  /** Categories **/
  private static readonly CATEGORIES = "categories/";

  static getCategories = () => EndpointsUtils.urlApp + EndpointsUtils.CATEGORIES;

  /** Contact **/
  private static readonly CONTACT = "contact/";

  static postContact = () => EndpointsUtils.urlPublic + EndpointsUtils.CONTACT;

  /** Orders **/
  private static readonly ORDERS = "orders/";
  private static readonly CHECK = "check/";
  private static readonly CHECK_COUPON = "check-coupon/";
  private static readonly PREORDER = "preorder/";
  private static readonly PURCHASE = "purchase/";
  private static readonly CONFIRM = "confirm/";

  static getUserOrders = () => EndpointsUtils.urlApp + EndpointsUtils.ORDERS;
  static getOrder = (id) => EndpointsUtils.getUserOrders() + id;
  static getAnonymousOrder = (id) => EndpointsUtils.getUserOrders() + id;
  static checkCoupon = () => EndpointsUtils.getUserOrders() + EndpointsUtils.CHECK_COUPON;
  static checkOrderItems = () => EndpointsUtils.getUserOrders() + EndpointsUtils.CHECK;
  static preorder = () => EndpointsUtils.getUserOrders() + EndpointsUtils.PREORDER;
  static purchase = () => EndpointsUtils.getUserOrders() + EndpointsUtils.PURCHASE;
  static confirmPurchase = () => EndpointsUtils.getUserOrders() + EndpointsUtils.CONFIRM;

  /** Delivery zones **/
  private static readonly DELIVERY_ZONES = "delivery-zones/";

  static getDeliveryPrice = () => EndpointsUtils.urlApp + EndpointsUtils.DELIVERY_ZONES + EndpointsUtils.CHECK;

  /** Products **/
  private static readonly PRODUCTS = "products/";
  // private static readonly SEARCH = "search/";
  private static readonly FEATURED = "featured/";
  private static readonly SLUG = "slug/";
  private static readonly VARIANT = "variant/";
  private static readonly PRICES = "prices/";
  private static readonly PRODUCT_FILES = "product-files/";
  private static readonly DOWNLOAD = "download/";
  private static readonly ANY = "any/";
  private static readonly ID = "id/";
  private static readonly PRODUCT_ATTRIBUTES = "attributes.json";

  private static readonly PRODUCTS_ENDPOINT = EndpointsUtils.urlApp + EndpointsUtils.PRODUCTS;

//  static getPagedProducts = () => EndpointsUtils.PRODUCTS_ENDPOINT + EndpointsUtils.SEARCH;
  private static readonly PRODUCTSMOCK = "/assets/data/products.json";
  static getPagedProducts = () => EndpointsUtils.PRODUCTSMOCK;
  // FIN CAMBIOS MAH
  static getFeaturedProducts = () => EndpointsUtils.PRODUCTS_ENDPOINT + EndpointsUtils.FEATURED;
  static getProduct = (slug) => EndpointsUtils.PRODUCTS_ENDPOINT + EndpointsUtils.SLUG + slug;
  static getAnyProduct = slug => EndpointsUtils.PRODUCTS_ENDPOINT + EndpointsUtils.SLUG + EndpointsUtils.ANY + slug;
  static getProductById = id => EndpointsUtils.PRODUCTS_ENDPOINT + EndpointsUtils.ID + EndpointsUtils.ANY + id;
  static getProductVariant = () => EndpointsUtils.PRODUCTS_ENDPOINT + EndpointsUtils.VARIANT;
  static getMinMaxPrices = () => EndpointsUtils.PRODUCTS_ENDPOINT + EndpointsUtils.PRICES;
  static downloadProductFile = (id) => EndpointsUtils.urlApp + EndpointsUtils.PRODUCT_FILES + EndpointsUtils.DOWNLOAD + id;
  static getProductFile = (id) => EndpointsUtils.urlApp + EndpointsUtils.PRODUCT_FILES + id;
  static getProductVariantFromAttributeValues = () => EndpointsUtils.PRODUCTS_ENDPOINT + EndpointsUtils.VARIANT;
  static getProductAttributes = () => EndpointsUtils.PRODUCT_ATTRIBUTES;

  /** Ordination **/
  private static readonly ORDINATIONS = "ordinations/";

  static getOrdinationValues = () => EndpointsUtils.PRODUCTS_ENDPOINT + EndpointsUtils.ORDINATIONS;

  /** Slider **/
  private static readonly SLIDER = "slider/";
  private static readonly HOME_DEV = "home_dev/";

  static getHomeSlides = () => EndpointsUtils.urlPublic + EndpointsUtils.SLIDER + EndpointsUtils.HOME_DEV;

  /** User **/
  private static readonly USERS = "users/";
  private static readonly ME = "me/";
  private static readonly UPDATE = "update/";
  private static readonly DELETE = "delete/";

  static readonly USERS_ENDPOINT = EndpointsUtils.urlApp + EndpointsUtils.USERS;

  static getUser = () => EndpointsUtils.USERS_ENDPOINT + EndpointsUtils.ME;
  static updateUser = () => EndpointsUtils.USERS_ENDPOINT + EndpointsUtils.UPDATE;
  static deleteUser = () => EndpointsUtils.USERS_ENDPOINT + EndpointsUtils.DELETE;

  /** Captcha **/
  private static readonly CAPTCHA = "captcha/";

  static validateCaptcha = () => EndpointsUtils.urlPublic + EndpointsUtils.CAPTCHA;

  /** Questions **/
  private static readonly QUESTIONS = "/assets/data/questions.json";

  static getQuestions = () => EndpointsUtils.QUESTIONS;

  /** Info Objects **/
  private static readonly INFO_OBJECTS_DATA = "/assets/data/infoObjectsData.json";

  static getInfoObjectsData = () => EndpointsUtils.INFO_OBJECTS_DATA;

  /** Grid **/
  private static readonly GRID_ITEMS = "/assets/data/gridItems.json";

  static getGridItems = () => EndpointsUtils.GRID_ITEMS;

  /** Legal **/
  private static readonly COOKIES = "/assets/data/legal/cookies.json";
  private static readonly DISCLAIMER = "/assets/data/legal/disclaimer.json";
  private static readonly TERMS = "/assets/data/legal/terms.json";

  static getCookies = () => EndpointsUtils.COOKIES;
  static getDisclaimer = () => EndpointsUtils.DISCLAIMER;
  static getTerms = () => EndpointsUtils.TERMS;

  /** Company **/
  private static readonly COMPANY_ICONS = "/assets/data/company-icons.json";

  static getCompanyIcons = () => EndpointsUtils.COMPANY_ICONS;

  /** Slider **/
  private static readonly SLIDES = "/assets/data/slides.json";

  static getSlides = () => EndpointsUtils.SLIDES;

  /** Feed **/
  private static readonly FEED_CONTENT = "https://rss2json.com/api.json?rss_url=";

  static getFeedContent = url => EndpointsUtils.FEED_CONTENT + url;

  /** Access guard **/
  private static readonly MY_ACCOUNT = "/mi-cuenta";

  static getMyAccount = () => EndpointsUtils.MY_ACCOUNT;

  /** Auth guard **/
  private static readonly ACCESS = "/acceso";

  static getAccess = () => EndpointsUtils.ACCESS;
}
